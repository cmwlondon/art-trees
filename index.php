<?php
/*
Single post template
handles:
news post -> /source/scss/modules/news-article.scss
person bio -> /source/scss/modules/bio,scss
*/
if ( have_posts() ) {
	the_post();
	// populates variable $post
	// post id = $post->ID

	/* PAGE STRUCTURE START */

	switch( $post->post_type ) {
	    case 'post' : {
// --------------------------------------------------------------------
// standard wp post with additinal ACF Pro properties attached
// --------------------------------------------------------------------
			get_header( null, [ 'page-css' => 'single-news', 'hasgenericheader' => 'no', 'hasbreadcumbbar' => 'no' ] ); 

			$newsPostHandler = new NewsHandler();
			// echo "<pre>".print_r( $newsPostHandler->posts, true )."</pre>";

			// get this post data
			$thisPost = $newsPostHandler->getPostByID( $post->ID );

			$title = $thisPost['title'];
			$permalink = $thisPost['permalink'];
			$category = $thisPost['category'];
			$source = $thisPost['source'];
			$date = $thisPost['date'];

			$encodedTo = rawurlencode('no-one@gmail.com');
			$encodedSubject = rawurlencode($post->post_title);
			$encodedBody = rawurlencode($permalink);
			$uri = "mailto:$encodedTo?subject=$encodedSubject&body=$encodedBody";
			$encodedUri = htmlspecialchars($uri);

			$image = ( !empty( $thisPost['image'] ) ) ? $thisPost['image'] : false;
			$imageURL = ($image) ? $image[0] : false;
			$caption = ( !empty(get_field('image_caption')) ) ? get_field('image_caption') : '';

			$latestPosts = $newsPostHandler->getNLatestPosts( [ $post->ID ],4 );
			$pagingLinks = $newsPostHandler->getNextAndPreviousPermalinks( $post->ID );
	?>
	<!--
	<section class="module page-banner" id="page-banner-01">
		<div class="breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col">
						<ul>
							<li><a href="/" class="root">Home</a></li>
							<li><a href="/news/" class="parent">News</a></li>
							<li><span><?= $post->post_title; ?></span></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
-->
	<article class="news" data-post-id="<?= $post->ID ?>">
		<?php if ( !$pagingLinks['lastPost'] ) :?><a href="<?= $pagingLinks['prevPost'] ?>" class="sidelink prevlink">PREV</a><?php endif; ?>
		<?php if ( !$pagingLinks['firstPost'] ) :?><a href="<?= $pagingLinks['nextPost'] ?>"  class="sidelink nextlink">NEXT</a><?php endif; ?>
		<div class="container">
			<div class="row">
				<div class="col sidePadding">
					<header>
						<p class="category"><?= $category ?></p>
						<h1><?= $title ?></h1>
						<p class="sourcedate">Source By <?= $source ?> - <?= $date ?></p>
					</header>
				</div>
			</div>

			<?php if ( $image ) : ?>
			<div class="row">
				<div class="col">
					<figure class="mainImg">
						<div class="frame"><img src="<?= $imageURL ?>" alt="<?= $caption ?>"/></div>
						<?php if ( $caption !== '' ) : ?><figcaption><?= $caption ?></figcaption><?php endif ?>
					</figure>
				</div>
			</div>
			<?php endif ?>

			<div class="row">
				<div class="col sidePadding postContentBox">
					<aside>
						<h2>Share</h2>
						<div class="buttons">
							<a href="https://www.linkedin.com/sharing/share-offsite/?url=<?= $encodedBody ?>" class="btn2 linkedin" title="Share this dpcoument on linkedin" target="_blank"><span>linkedin</span></a>
							<a href="<?= $encodedUri ?>" title="Share this document via email" class="btn2 email"><span>email</span></a>
						</div>
					</aside>
					
					<div class="postContent">
					<?= $post->post_content; ?>
					</div>
				</div>
				
			</div>

			<hr>
			<?php
			if ( !empty( get_field( 'attachments', $post->ID ) ) && count(get_field( 'attachments', $post->ID )) > 0 )  :

			?>
			<div class="row">
				<div class="col attachments sub download">
					<h2>Attached files/documents</h2>
					<ul>
					<?php foreach( get_field( 'attachments', $post->ID ) AS $attachment ) :
					?>
						<?php switch ( $attachment['type'] ) {
							case "file" : {
								$thisFileHandler = new FileHandler();

								$thisFilePropeties = $thisFileHandler->getTypeAndSize( $attachment['file']['filename'], $attachment['file']['filesize'] );
						?>
						<li data-filename="<?= $attachment['file']['filename'] ?>" data-filesize="<?= $attachment['file']['filesize'] ?>">
							<div class="docdl <?= $thisFilePropeties['type'] ?> multiLanguage">
								<h3><a href="<?= $attachment['file']['url'] ?>" target="_blank" title="<?= $attachment['file']['title']." (".$attachment['file_language']." ".$thisFilePropeties['size']." ".$thisFilePropeties['type'].")" ?>"><?= $attachment['file']['title'] ?></a></h3>
								<ul>
									<li><a href="<?= $attachment['file']['url'] ?>" target="_blank" title="<?= $attachment['file']['title'] ?>"><?= $attachment['file_language']." (".$thisFilePropeties['size']." ".$thisFilePropeties['type'] ?>)</a></li>
								</ul>
								</p>
							</div>
						</li>
						<?php
							} break;
							case "document" : {
								$documentHandler = new DocumentHandler(false, false);
								$documentID = $attachment['document']->ID;
								$document = $documentHandler->getDocumentByID( $documentID );
								$documentTitle = $document['title'];
								$documentTranslations = $document['translations'];
						?>
							<li>
								<div class="docdl pdf multiLanguage">
									<h3><?= $documentTitle ?></h3>
									<ul>
										<?php foreach ( $documentTranslations AS $translation ) :
												// $caption = ucfirst( $translation['language'])." ({$translation['size']} {$translation['type']})";
												$caption = ucfirst( $translation['language']);
											?>
												<li class="translation"><a href="<?= $translation['url'] ?>" title="<?= $caption ?>" target="_blank"><?= $caption ?></a></li>
											<?php endforeach; ?>

									</ul>	
								</div>
							</li>

						<?php
							} break;
						} ?>

					<?php endforeach ?>
					</ul>
				</div>
			</div>
			<?php endif ?>
		</div>
	</article>

	<section class="module latest_news">
		<div class="container">
			<div classs="row">
				<div class="col header">
					<h2>Recent news</h2>
				</div>
			</div>
			<div class="row">
				<?php foreach ( $latestPosts AS $post ) :
					$iconClass = '';
					$hasExternalLink = false;
					$postlink = $post['permalink'];
					// attachment icon
					if ( $post['attachments'] !== 'none' ) {
						$iconClass = "attachment";
					}
					// external link ** icon has priority over attachment
					if ( $post['has_link'] === 'yes' && $post['external_link'] === 'yes' ) {
						$postlink = $post['link_url'];
						$iconClass = "externalLink";
						$hasExternalLink = true;
					}
					// thumbnail
					$thumbnail = ( !empty($post['thumbnail']) ) ? $post['thumbnail'] : false;
					$thumbnailPath = ( $thumbnail ) ? $thumbnail[0] : false;
					// 0 URL
					// 1 width
					// 2 height
					$tw = $thumbnail[1];
					$th = $thumbnail[2];
					$ta = $th / $tw;
					// $cAspect = 0.61;
					$cAspect = 1.0;
				?>
				<div class="col-lg-3 item">

					<a href="<?= $postlink ?>" class="overlay" <?php if ( $hasExternalLink ) { echo 'target="_blank" title="'.$post['title'].' (external link)"'; } else { echo 'title="'.$post['title'].'"'; } ?>><span><?= $post['title'] ?></span></a>
					<figure <?php if ( $ta > $cAspect ) { echo "class=\"scaleByHeight\""; } ?> >
						<img src="<?= $thumbnailPath ?>" alt="<?= $post['title'] ?>">
					</figure>
					<div class="info <?= $iconClass ?>">
						<p class="category"><?= $post['category'] ?></p>
						<h3><?= $post['title'] ?></h3>
					</div>
					<p class="extra"><?= $post['source'] ?>, <?= $post['date'] ?></p>
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</section>

	<?php
// --------------------------------------------------------------------
// news post END
// --------------------------------------------------------------------
	  	} break;
	    case 'person' : {
// --------------------------------------------------------------------
// BIO page
// --------------------------------------------------------------------
			get_header( null, [ 'page-css' => 'single-person', 'hasgenericheader' => 'no', 'hasbreadcumbbar' => 'yes' ] );

	    	// perosn ID  = $post->ID
	    	// get post properties, ACF properties and person details
	    	$thisPersonID = $post->ID;
	    	$personMeta = getPerson( $thisPersonID );
	    	$memberOf = $personMeta['membership'];

	    	// $group = getMembersOf( $memberOf['slug'] );
	    	// $groupMembers = $group['members'];

            $portraitID = ( !empty( $personMeta['portrait'] ) ) ? $personMeta['portrait'] : false;
            $portraitPath = ( $portraitID ) ? wp_get_attachment_image_src($portraitID, 'full')[0] : '';

            // get group name/content based on person membership field
            $thisGroup = getGroup( $memberOf['slug'] );
            // get mebers of group in arbitrary order
			$_groupMembers = getPeopleInOrder( $thisGroup['members'] );

			// set up pev and next links
			$groupIDs = [];
			foreach ( $_groupMembers AS $index => $groupMember ) {
				$groupIDs[] = $groupMember['id'];

				if ( (int) $thisPersonID === (int) $groupMember['id'] ) {
					$n = $index;
					$prevIndex = $n - 1;
					$nextIndex = $n + 1;
					if ( $n === 0) {
						$prevIndex = count($_groupMembers) - 1;
					}
					if ( $n === (count($_groupMembers) - 1) ) {
						$nextIndex = 0;
					}
					$prevLink = $_groupMembers[$prevIndex]['permalink'];
					$nextLink = $_groupMembers[$nextIndex]['permalink'];
				} 
			}			
	?>
	
	<section class="module page-banner">
		<div class="breadcrumb">
			<div class="container">
				<div class="row">
					<div class="col-lg-9">
						<ul>
							<li><a href="/" class="root">Home</a></li>
							<!-- <li><a href="/about/?=group=<?= $memberOf['slug'] ?>" class="parent"><?= $memberOf['name'] ?></a></li> -->

							<li><a href="/about/" class="parent">About us</a></li>
							<!-- <li><a href="/about/#governance" class="parent"><?= $thisGroup['name'] ?></a></li> -->
							<li><span><?= get_the_title() ?></span></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="module bio" data-subject="<?= $thisPersonID ?>" data-group="<?= implode( ',', $groupIDs) ?>">

		<!--  loop through current group -->
		<a href="<?= $prevLink ?>" class="sidelink prevlink" data-member="<?= $_groupMembers[$prevIndex]['id'] ?>">PREV</a>
		<a href="<?= $nextLink ?>"  class="sidelink nextlink" data-member="<?= $_groupMembers[$nextIndex]['id'] ?>">NEXT</a>

		<div class="bioInfo" data-current="<?= $post->ID ?>" data-group="">
			<div class="container">
				<div class="row bioBlock">
					<div class="col-lg-6 text">
						<h1 <?php if ( empty( $personMeta['org'] ) ) { echo "class=\"no-org\""; } ?> ><?= $personMeta['title'] ?></h1>
						<?php if ( $personMeta['org'] ) : ?><h3><?= $personMeta['org'] ?></h3><?php endif; ?>
						<div class="biotext"><?= $personMeta['bio'] ?></div>
					</div>
					<div class="col-lg-5 portrait">
						<figure>
							<img src="<?= $portraitPath ?>" alt="<?= $personMeta['title'] ?>">
						</figure>
						<?php if ( !empty($personMeta['linkedin']) || !empty($personMeta['email']) ) : ?>
						<div class="connect">
							<p>CONNECT: </p>
							<?php if ( !empty($personMeta['linkedin']) ) : ?><a href="<?= $personMeta['linkedin'] ?>" class="linkedin" target="_blank">Linkedin</a><?php endif; ?>
							<?php if ( !empty($personMeta['linkedin']) ) : ?><a href="mailto:<?= $personMeta['email'] ?>" class="email">Email</a><?php endif; ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php
		// get board members grid
		$args = [
			'defaultGroup' => $thisGroup['slug'],
			'module_identifier' => 'bio-board'
		];
		get_template_part( 'blocks/modules/governance', '', $args);

// --------------------------------------------------------------------
// bio END
// --------------------------------------------------------------------
	  	} break;
	}
	/* PAGE STRUCTURE END */

	get_footer( null, [] );
}
?>