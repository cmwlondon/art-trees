</main>

<?php

// set copy in wP-ADMIN Theme Settings page
// signup_form_intro_copy
// footer_address
$singupFormCopy = get_field('signup_form_intro_copy','option');
$address = get_field('footer_address','option');
?>
<footer class="pagefooter" id="signup-anchor">
	<div class=" greytrees">
		<div class="container">

			<?php if (true) : ?>
			<div class="row">
				<div class="col-lg-6 intro">
					<?= $singupFormCopy ?>

				</div>
				<div class="col-lg-6 form unsub">

					<!--  contact-7 form START -->
					<?php echo do_shortcode('[contact-form-7 id="2293" title="Contact form 1"]'); ?>
					<!--  contact-7 form END -->

					<?php if ( false ) : ?>
					<form id="signup-form" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" class="needs-validation" novalidate>
						<input type="hidden" name="action" value="signup">
						<!-- <input type="hidden" name="signup_nonce" value="">-->

						<div class="row f2">
							<div class="col-sm-6 f1">
								<label for="name">Name</label>
								<input type="text" id="name" name="name" placeholder="Your name" required>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
							<div class="col-sm-6 f1">
								<label for="email">Email address</label>
								<input type="email" id="email" name="email" placeholder="Your email address" required>
								<div class="invalid-feedback">Please enter your name.</div>
							</div>
						</div>
						<div class="row f3">
							<div class="col">
								<label for="organization">Organization</label>
								<input type="text" id="organization" name="organization" placeholder="Organization">
							</div>
						</div>
						<button class="btn transparent" type="submit">SIGNUP</button>
					</form>
					<?php endif; ?>

					<div class="response">
						<p>Thank you for your submission.</p>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="darkGrey">
		<div class="container">
			<div class="row">
				<div class="logo">
					<a href="/" title="<?= get_bloginfo( 'name' ) ?>"><img alt="<?= get_bloginfo( 'name' ) ?>" src="<?php echo get_template_directory_uri(); ?>/assets/svg/art-logo-white2.svg"></a>
				</div>

		        <?php
		          wp_nav_menu( array(
		            'theme_location' => 'footer',
		            'menu_class' => 'footer-nav',
		            'container' => false,
		            'items_wrap' => '<ul id="menu-footer" class="footer-nav">%3$s<li id="menu-about" class="menu-item">
					<a href="/about-us/" class="parent">About us</a>
						<h3>ART</h3>'.$address.'</li></ul>'
		          ) );
		        ?>


			</div>
			<div class="row">
				<div class="col"><p>&copy;ART 2021 <?php if (false) : ?>| <a href="/privacy/">Privacy policy</a> | <a href="/cookies/">Cookie Statement</a><?php endif; ?></p></div>
			</div>

		</div>
	</div>
</footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/library/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.3.1/gsap.min.js"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/ScrollTrigger.min.js"></script>

<?php wp_footer(); ?>
</body>
</html>
