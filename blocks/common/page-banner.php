<?php
/*
page-banner.php
/source/scss/modules/page-banner.scss
*/

/*
has_generic_header ('yes','no')
header_image (image ID)
header_title 
header_copy
has_breadcrumb ('no','yes')
has_info_blocks  ('no','yes')
blocks[
	[
		heading
		copy
		link_list[
			[
				url
				caption
				new_window  ('no','yes')
			],
		]
		block_link[
				url
				caption
				new_window  ('no','yes')
		]
	]
]

* not sure if the items in link_list will be external pages/downloads or if they might be ARTTrees Documents
modules[]
*/
$hasHeader = ( get_field('has_generic_header') === 'yes' );
$hasBreadcrumbs = ( get_field('has_breadcrumb') === 'yes' );
$hasInfoBlocks = ( get_field('has_info_blocks') === 'yes' );
$desktop = get_field('header_image');
$header_image = ( !empty($desktop) ) ? wp_get_attachment_image_src($desktop, 'full')[0] : '';

if ( $hasBreadcrumbs ) {
	// get page URL, us it to build bredcrumb elements
	$routes = [
		'/trees-documents/' => [
			'url' => '/trees/',
			'caption' => 'Trees'
		],
		'news' => [
			'url' => '/news/',
			'caption' => 'News'
		]
	];
}
?>
<style>
#page-banner-01{}	
#page-banner-01 .bg{
	background:
	linear-gradient(
      rgba(22,27,32,0.35), 
      rgba(22,27,32,0.35)
    ),
    center / cover no-repeat url('<?php echo $header_image; ?>');
</style>
<section class="module page-banner <?php if ( !empty(get_field('banner_css')) ) { echo get_field('extra_css'); } ?>" id="page-banner-01">
	<?php
	if ($hasHeader) {
	?>
	<div class="bg">
		<div class="heading">
			<div class="container">
				<div class="row">
					<div class="col-lg-9">
						<?php if ( !empty(get_field('header_title')) ) : ?><h1><?php echo get_field('header_title'); ?></h1><?php endif; ?>
						<?php if ( !empty(get_field('header_copy')) ) : ?><p  <?php if (get_field('header_copy_size') === 'large') : ?>class="large"<?php endif; ?>><?php echo get_field('header_copy'); ?></p><?php endif; ?>
					</div>
				</div>


			</div>
		</div>

	</div>

	<?php if ( $hasInfoBlocks ) :
		$blocks = get_field('blocks');
	?>
	<div class="extras">
		<div class="container">
			<div class="row">
			<?php
				foreach ($blocks as $key => $block) :
				$linkList = ( !empty($block['link_list']) ) ? $block['link_list'] : false;
				$blockLink = ( !empty($block['block_link']['url']) ) ? $block['block_link'] : false;
			?>
				<div class="block">
					<?php if ( !empty($block['heading']) ) : ?><h3><?php echo $block['heading']; ?></h3><?php endif; ?>
					<?php if ( !empty($block['copy']) ) : ?><p><?php echo $block['copy']; ?></p><?php endif; ?>
					<?php if ($linkList) : ?>
					<ul>
						<?php foreach($linkList AS $key => $link) :
						?>
						<li><a href="<?php echo $link['url']; ?>" class="arrow1" <?php if ( $link['new_window'] === 'yes' ) { echo "target=\"_blank\""; } ?>><?php echo $link['caption']; ?></a></li>
						<?php endforeach; ?>
					</ul>	
					<?php endif; ?>
					<?php if ($blockLink) : ?><a href="<?php echo $blockLink['url']; ?>" class="blocklink" <?php if ( $blockLink['new_window'] === 'yes' ) { echo "target=\"_blank\""; } ?>><?php echo $blockLink['caption']; ?></a><?php endif; ?>
				</div>
			<?php
				endforeach;
			?>
			</div>
		</div>			
	</div>
	<?php endif; ?>

	<?php
	}

	if ($hasBreadcrumbs) {
		// check for FAQ page, may need to handle breasdcrumbs on this page differently
		$ifFAQpage = ( get_permalink() === get_site_url().'/faqs/' );

		// home / FAQS -> faq page / current segment (updated with javascript at the same time as updating the address bar)
	?>
	<!-- <?= $ifFAQpage ?> / <?= $post->ID ?> / <?= get_permalink() ?> / <?= get_site_url().'/faqs/' ?>-->
	<div class="breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<ul>
						<li><a href="/" class="root">Home</a></li>
						<?php if ( $post->post_parent !== 0 ) : ?><li><a href="<?= get_permalink($post->post_parent) ?>" class="parent"><?= get_the_title($post->post_parent) ?></a></li><?php endif; ?>
						<li><span><?= get_the_title() ?></span></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<?php
	}
	?>
</section>
