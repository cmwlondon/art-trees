<?php
// people group grid

//group
// $args['slug'] // 'board-fof-directors','secretariat'

// $board = getGroup( 'board-of-directors' );
// $boardMembers = getPeopleInOrder( $board['members'] );

// $boardMembers -> $args['members'] // arbitrarily ordered array of people metadata
// title
// org
// portrait -> portrait image ID
// permalink

// $args['membership'] // arbitrarily ordered array of person IDS

//  $args['subject'] // id of selected person
?>

<div class="members container" data-group="<?= $args['group'] ?>" <?php if ( $args['membership'] ) : ?>data-members="<?= $args['membership'] ?>"<?php endif ?> <?php if ( $args['subject'] ) : ?>data-subject="<?= $args['subject'] ?>"<?php endif ?>>
	<ul class="row">
		<?php foreach ( $args['members'] AS $member ) :
        $portraitID = ( !empty( $member['portrait'] ) ) ? $member['portrait'] : false;
        $portraitPath = ( $portraitID ) ? wp_get_attachment_image_src($portraitID, 'full')[0] : '';
		?>
		<li class="col-lg-3 col-sm-6 item <?php if ( $member['id'] === $args['subject'] ) { echo 'greyedout'; } ?>">
			<a href="<?= $member['permalink'] ?>" class="overlay"><span>View bio for <?= $member['title'] ?></span></a>
			<figure>
				<img src="<?= $portraitPath ?>" alt="<?= $member['title'] ?>">
			</figure>
			<div class="info">
				<h3><?= $member['title'] ?></h3>
				<p><?= $member['org'] ?></p>
				<p class="bio">Bio+</p>
			</div>
		</li>
		<?php endforeach ?>
	</ul>
</div>
