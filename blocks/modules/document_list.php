<?php
/*
layout: document_list
source/scss/modules/document_list.scss
*/
$moduleIdentifier = $args['module_identifier'];
$filterLevel = get_sub_field('filter_levels');

/*
extra_menu_items[
	link_text
	link_url
]
*/
$extraMenuItems = ( !empty(get_sub_field('extra_menu_items')) ) ? get_sub_field('extra_menu_items') : [];

// $items_per_page = 4;
$items_per_page = 15;

$title = get_the_title($post->ID);
$showNocat = (get_field('show_nocat','option') === 'yes' );

// get main/sub document categpry from URL
// /trees/MAIN?pn=N
// /trees/MAIN?sub=SUB&pn=N
preg_match('/^.+\/([a-z\-\_]+)\/([a-z\-\_]+)\/(?:\?sub=([a-z0-9\-\_\.]*))*(?:(?:\&|\?)pn=([0-9]+))*$/', current_location(), $matches);
$section = ( array_key_exists( 1, $matches ) ) ? $matches[1] : '';
$main = ( array_key_exists( 2, $matches ) ) ? $matches[2] : '';
$sub = ( array_key_exists( 3, $matches ) ) ? $matches[3] : '';
$currentPage = ( array_key_exists( 4, $matches ) ) ? (int) $matches[4] : 1;

// sort categories into arbitrary order
// different set of fields for trees and verification
$mappings = [
	'trees' => [
		'field' => 'category_order',
		'category' => 'category-select',
		'subcategory' => 'subcategory-select',
		'documents' => 'documents',
		'document' => 'document-select'
	],
	'verification' => [
		'field' => 'category_order2',
		'category' => 'category-select2',
		'subcategory' => false,
		'documents' => 'documents2',
		'document' => 'document-select2'
	],
];

$thisPageDocumentsHandler = new DocumentHandler( $section, $mappings );
$mainList = $thisPageDocumentsHandler->mainList;
$subList = $thisPageDocumentsHandler->subList;
$heirarchy  = $thisPageDocumentsHandler->heirarchy;

$vx = [
	'main' => [],
	'sub' => []
];


$sortOrder = $thisPageDocumentsHandler->normalizeSortOrder( $section ); 
$menu = $thisPageDocumentsHandler->documentListMenu( $main, $sub, $sortOrder );

// ------------------------------------------------------------------------------------------

?>
<!-- <?= count($extraMenuItems) ?> -->
<!-- <?= print_r( $extraMenuItems, true ) ?>-->
<!-- <?= $filterLevel ?> -->
<!-- <?= $matches[1] ?> -->
<!-- <?= $main ?> -->
<!-- <?= $sub ?> -->
<!-- <?= $currentPage ?> --> 
<script>
const section = '<?= $section ?>';
const masterCategories = <?= json_encode( $vx ) ?>;
const siteURL = '<?= get_site_url() ?>';
</script>
<style>
#<?= $moduleIdentifier; ?>{}	
</style>
<section class="module document_list" id="<?= $moduleIdentifier; ?>" data-filter-level="<?= $filterLevel ?>">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1><?= $title ?></h1>
			</div>
		</div>
		<div class="row">

			<!-- category list START -->
			<div class="col-xl-3 col-lg-4 docfilter">
		
				<div class="filter">
					<div class="control">
						<h2>Filter by document type:</h2>
						<p class="currentFilter">Current filter: <?= $fmt ?></p>
					</div>

					<?php if ( $filterLevel === 'mainsub') : ?>
					<ul class="main mainsub">
						<?php foreach ( $menu AS $menuMain ) :
							$subMenu = $menuMain['sub'];

							// DO NOT display process-documentation category
							// link to /process-documentation/ instead
						if ( $menuMain['main']['slug'] !== 'process-documentation') :
						?>
						<li class="accordion <?php if ( $menuMain['current'] ) { echo "open current"; } ?>" data-main="<?= $menuMain['main']['slug'] ?>">
							<div class="title">
								<h2><?= $menuMain['main']['name'] ?></h2>
							</div>
							<div class="clipper">
								<div class="content">
									<ul class="sub">
										<?php foreach ( $subMenu AS $subMenuItem ) :
											$current = (array_key_exists( 'current', $subMenuItem )) ? $subMenuItem['current'] : false;
										?>
										<li <?php if ( $current ) { echo "class=\"current\""; } ?>>
											<a href="/trees/<?= $menuMain['main']['slug'] ?>/?sub=<?= $subMenuItem['slug'] ?>" data-action="<?= $menuMain['main']['slug'].','.$subMenuItem['slug'] ?>"><?= $subMenuItem['name'] ?></a>
										</li>
									<?php endforeach ?>
									</ul>
								</div>
							</div>
						</li>
						<?php
						endif;
						endforeach
						?>
						<?php if ( count($extraMenuItems) > 0 ) :
								foreach ( $extraMenuItems AS $extraMenuItem ) :
						?>
						<li class="static">
							<div class="title">
								<a href="<?= $extraMenuItem['link_url'] ?>"><?= $extraMenuItem['link_text'] ?></a>
							</div>
						</li>
						<?php
						endforeach;
						endif;
						?> 
					</ul>
					<?php else : ?>
					<ul class="main mainonly">
						<?php foreach ( $menu AS $menuMain ) : ?>
						<li class="static <?php if ( $menuMain['current'] ) { echo "current"; } ?>">
							<div class="title">
								<a href="/<?= $section ?>/<?= $menuMain['main']['slug'] ?>/"><?= $menuMain['main']['name'] ?></a>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?> 
				</div>

			</div>
			<!-- category list END -->

			<!-- document list START -->
			<div class="col-xl-9 col-lg-8 docList">

				<?php

				$thisPageDocuments = $thisPageDocumentsHandler->getOrderedDocumentsByFilterAndPage([ 'main' => $main, 'sub' => $sub ], $filterLevel, $currentPage, $items_per_page);
				$pages = $thisPageDocuments['pages'];

				?>

				<div class="row documentContainer" data-main="<?= $main ?>" data-sub="<?= $sub ?>" data-items-per-page="<?= $items_per_page ?>" data-pages="<?= $pages ?>" data-page="<?= $currentPage ?>">

					<div class="wt">
						<div class="docBox">
							<?php if ( $thisPageDocuments['mode'] === 'hierarchy' && $filterLevel === 'mainsub' ) : ?>

							<!-- subcategpory not defined, show all under main START -->
							<ul class="doctop">
								<?php foreach( $thisPageDocuments['documents'] AS $subcategory ) : ?>
								<li class="sub">
									<h2><?= $subcategory['name'] ?></h2>
									<ul>
										<?php foreach( $subcategory['docs'] AS $subdoc ) :
											$sts = $subdoc['translations'];
											$typeicon = ( count($sts) > 1 ) ? 'pdf' : $sts[0]['type'];
											// $typeicon = 'pdf';
										?>
										<li class="doc">
											<div class="item <?= $typeicon ?> multiLanguage">
												<h3><?= $subdoc['title'] ?></h3>
												<?php if ( count($sts) > 0 ) : ?>
												<ul>
												<?php if ( count($sts) > 1 ) : ?>
													<?php foreach ( $sts AS $st ) :
													// $caption = $st['language'];
													// $caption = ucfirst($st['language'])." ({$st['size']} {$st['type']})";
														$caption = ucfirst($st['language']);
													?>
														<li class="translation"><a href="<?= $st['url'] ?>" title="<?= "{$st['language']}" ?>" target="_blank"><?= $caption ?></a></li>
													<?php endforeach; ?>
												<?php else :
													$st = $sts[0];
													// $caption = ucfirst($st['language'])." ({$st['size']} {$st['type']})";
													$caption = ucfirst($st['language']);
												?>
													<li class="translation"><a href="<?= $st['url'] ?>" title="<?= "{$st['language']}" ?>" target="_blank"><?= $caption ?></a></li>
												<?php
												endif;
												?>
												</ul>	
												<?php if ( $subdoc['webinarcaption'] && $subdoc['webinarlink'] ) : ?><p class="webinar"><a href="<?= $subdoc['webinarlink'] ?>" target="_blank" title="<?= $subdoc['webinarcaption'] ?>"><?= $subdoc['webinarcaption'] ?></a></p><?php endif; ?>
												<?php
												endif;
												?>
											</div>
											
										</li>
										<?php
										endforeach;
										?>
									</ul>
								</li>
							<?php endforeach; ?>
							</ul>
							<!-- END -->

							<?php else :

								// $filterLevel === 'mainonly'
								// $thisPageDocuments['mode'] === 'hierarchy'
								// $thisPageDocuments['documents']['nocat']['docs']

								if ( $filterLevel === 'mainonly' ) {
									$_thisPageDocuments = [
										'documents' => $thisPageDocuments['documents']['nocat']['docs']
									];
								} else {
									$_thisPageDocuments = $thisPageDocuments;
								}
							?>
							
							<!-- subcategpory defined START -->
							<ul class="doctop">
								<?php foreach( $_thisPageDocuments['documents'] AS $doc ) :
									$sts = $doc['translations'];
									$typeicon = ( count($sts) > 1 ) ? 'pdf' : $sts[0]['type'];
								?>
								<li class="doc">
									<div class="item <?= $typeicon ?> multiLanguage">
										<h3><?= $doc['title'] ?></h3>
										<ul>
											<?php if ( count($sts) > 1 ) : ?>
												<?php foreach ( $sts AS $st ) :
													// $caption = ucfirst($st['language'])." ({$st['size']} {$st['type']})";
													$caption = $st['language'];
												?>
													<li class="translation"><a href="<?= $st['url'] ?>" title="<?= "{$st['language']}" ?>" target="_blank"><?= $caption ?></a></li>
												<?php endforeach; ?>
											<?php else :
												$st = $sts[0];
												$caption = ucfirst($st['language']);
											?>
												<li class="translation"><a href="<?= $st['url'] ?>" title="<?= "{$st['language']}" ?>" target="_blank"><?= $caption ?></a></li>
											<?php endif; ?>
										</ul>	
										<?php if ( $doc['webinarcaption'] && $doc['webinarlink'] ) : ?><p class="webinar"><a href="<?= $doc['webinarlink'] ?>" target="_blank" title="<?= $doc['webinarcaption'] ?>"><?= $doc['webinarcaption'] ?></a></p><?php endif; ?>

									</div>
								</li>
								<?php endforeach; ?>
							</ul>
							<!-- END -->

							<?php endif ?>
						</div>

						<div class="paging" data-min="1" data-max="<?= $pages ?>">
						<?php if ( $pages > 1 ) : ?>
							<!-- <a href="/<?= $section ?>/<?= $main ?>/?sub=<?= $sub ?>&amp;pn=1" class="btn2 transparent <?php if ( $currentPage === 1 ) : ?>current<?php endif; ?>" data-action="1">FIRST</a> -->
							<?php
							$n = 0;
							while ($n < $pages ) {
								$page = $n + 1;
							?>
								
								<a href="/<?= $section ?>/<?= $main ?>/?sub=<?= $sub ?>&amp;pn=<?= $page ?>" class="btn2 transparent page <?php if ( $currentPage === $page ) : ?>current<?php endif; ?>" data-action="<?= $page ?>"><?= $page ?></a>
							<?php
								$n++;
							}
							?>
							<!-- <a href="/<?= $section ?>/<?= $main ?>/?sub=<?= $sub ?>&amp;pn=<?= $pages ?>" class="btn2 transparent <?php if ( $currentPage === $pages ) : ?>current<?php endif; ?>" data-action="<?= $pages ?>">LAST</a> -->
						<?php endif; ?>
						</div>
					</div>
				</div>


			</div>
			<!-- document list END -->
		</div>
	</div>

	<ul class="documentListTemplates">
		<li class="sub">
			<h2>SUBCATEGORY</h2>
			<ul>
				<!-- docs -->
			</ul>
		</li>
		<li class="doc">
			<div class="item multiLanguage">
				<h3></h3>
				<ul>
					<!-- docitems -->
				</ul>	
			</div>
		</li>
		<li class="translation">
			<a href="URL" title="LANGUAGE (SIZE TYPE)" target="_blank">LANGUAGE</a>
		</li>
	</ul>
</section>