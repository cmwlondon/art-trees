<?php
/*
layout: two_column_text+image
source/scss/modules/two_column_text+image.scss
*/

/*
extra_css
layout ('left','right') first row: image on left / image on right
rows[
	image
	image_caption
	copy
	has_buttons ('no','yes')
	buttons[
		type  ('transpoarent','green')
		label
		link
		external_link  ('no','yes')
	]
]
*/

$moduleIdentifier = $args['module_identifier'];
$extraCSS = get_sub_field('extra_css');
// bg_colour
$bgColour = ( !empty(get_sub_field('bg_colour')) ) ? get_sub_field('bg_colour') : false;
$textColour = ( !empty(get_sub_field('text_colour')) ) ? get_sub_field('text_colour') : 'dark'; 'dark / light';

/*
$bgImage = ( !empty(get_sub_field('bg_image')) ) ? get_sub_field('bg_image') : false;
$textColour = ( !empty(get_sub_field('text_colour')) ) ? get_sub_field('text_colour') : 'dark';

// no-repeat,repeat-x,repeat-y,repeat
$bgRepeat = ( !empty(get_sub_field('bg_repeat')) ) ? get_sub_field('bg_repeat') : false;

// t,tr,r,br,b,bl,l,tl 
$bgPosition = ( !empty(get_sub_field('bg_position')) ) ? get_sub_field('bg_position') : false;
*/

$layout = get_sub_field('layout');
$rows = get_sub_field('rows');
?>
<style>
#<?php echo $moduleIdentifier; ?>{
	<?php if ( $bgColour ) : ?>
		background-color: <?= $bgColour ?>;
	<?php endif; ?>
}	
</style>
<section class="module two_column_text_image <?= $layout ?> <?php if ( !empty($extraCSS) ) { echo $extraCSS; } ?>" id="<?php echo $moduleIdentifier; ?>" >

	<?php
	if ( !empty($rows) ) :
		foreach ( $rows AS $key => $row ) :
			$rowbgColour = ( !empty( $row['row_bg_colour'] ) ) ? $row['row_bg_colour'] : false;
			$rowTextColour = ( !empty( $row['row_text_colour'] ) ) ? $row['row_text_colour'] : 'dark';
			$imagePath = ( !empty($row['image']) ) ? wp_get_attachment_image_src( $row['image'], 'full')[0] : '';
			$hasButtons = $row['has_buttons'] === 'yes';
	?>
	<div class="pair <?= $rowTextColour ?>" <?php if ( $rowbgColour ) : ?>style="background-color:<?= $rowbgColour ?>"<?php endif; ?>>
		<div class="one image">
			<figure>
				<div><img src="<?= $imagePath ?>"></div>
				<?php if ( !empty($row['image_caption']) ) : ?><figcaption><?= $row['image_caption'] ?></figcaption><?php endif; ?>
			</figure>
		</div>
		<div class="one text">
			<div class="copy">
				<?= $row['copy'] ?>
				<?php
				if ( $hasButtons ) :
					foreach ( $row['buttons'] AS $button ) :
				?>
				<a class="btn2 <?= $button['type'] ?>" href="<?= $button['link'] ?>" <?php if ( $button['external_link'] === 'yes' ) { echo "target=\"_blank\""; } ?>><?= $button['label'] ?></a>
				<?php
					endforeach;
				endif;
				?>
			</div>
			
		</div>
	</div>
	<?php
		endforeach;
	endif; 
	?>
</section>
