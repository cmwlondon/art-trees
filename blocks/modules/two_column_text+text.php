<?php
/*
layout: two_column_text+text
source/scss/modules/two_column_text_text.scss
*/

/*
left_column FLEXIBLE CONTENT
right_ccolumn  FLEXIBLE CONTENT
*/
$moduleIdentifier = $args['module_identifier'];

$extraCSS = get_sub_field('extra_css');
$jumpTarget = ( !empty(get_sub_field('jump_target'))) ? get_sub_field('jump_target') : false;
$bgColour = ( !empty(get_sub_field('bg_colour')) ) ? get_sub_field('bg_colour') : false;
$bgImage = ( !empty(get_sub_field('bg_image')) ) ? get_sub_field('bg_image') : false;
$textColour = ( !empty(get_sub_field('text_colour')) ) ? get_sub_field('text_colour') : 'dark';

// no-repeat,repeat-x,repeat-y,repeat
$bgRepeat = ( !empty(get_sub_field('bg_repeat')) ) ? get_sub_field('bg_repeat') : false;
// t,tr,r,br,b,bl,l,tl 
$bgPosition = ( !empty(get_sub_field('bg_position')) ) ? get_sub_field('bg_position') : false;
// normal, contain, cover 
$bgSize = ( !empty(get_sub_field('bg_size')) ) ? get_sub_field('bg_size') : 'normal';
$bgOpacity = ( !empty(get_sub_field('bg_opacity')) ) ? get_sub_field('bg_opacity') : 1.0;

// add background image repeat, background image position properties
$balance = get_sub_field('column_balance');

switch( $balance ) {
	case 'even' : {
		$leftCol = 'col-lg-6';
		$rightCol = 'col-lg-6';
	} break;
	case 'left' : {
		$leftCol = 'col-lg-8';
		$rightCol = 'col-lg-4';
	} break;
	case 'right' : {
		$leftCol = 'col-lg-4';
		$rightCol = 'col-lg-8';
	} break;
}
$leftColumn = get_sub_field('left_column');
$rightColumn = get_sub_field('right_column');

if ( $jumpTarget ) {
	$_moduleIdentifier = $jumpTarget;
} else {
	$_moduleIdentifier = $moduleIdentifier;
}

$hasBackground = ( $bgColour || $bgImage );
?>
<style>
#<?php echo $_moduleIdentifier; ?>{
	<?php if ($bgColour) : ?>
		background-color: <?= $bgColour ?>;
	<?php endif; ?>
}
#<?php echo $_moduleIdentifier; ?>::before {
	opacity: <?= $bgOpacity ?>;
	<?php if ($bgImage) :
		$bgImageURL = wp_get_attachment_image_src($bgImage, 'full')[0];
	?>
		background-image: url('<?= $bgImageURL ?>');
	<?php
		if ($bgRepeat) :
	?>
		background-repeat: <?= $bgRepeat ?>;
	<?php endif;
		if ( $bgSize !== 'normal') :
	?>
		background-size: <?= $bgSize ?>;
	<?php endif;
		if ($bgPosition) :
			switch($bgPosition) {
				case 't' : { $position = 'top'; } break;
				case 'tr' : { $position = 'right top'; } break;
				case 'r' : { $position = 'right'; } break;
				case 'br' : { $position = 'right bottom'; } break;
				case 'b' : { $position = 'bottom'; } break;
				case 'bl' : { $position = 'left bottom'; } break;
				case 'l' : { $position = 'left'; } break;
				case 'tl' : { $position = 'left top'; } break;
			}
	?>
		background-position: <?= $position ?>;
	<?php
	endif;
	endif;
	?>
}	
</style>
<section class="module two_column_text_text <?= $textColour ?> <?php if( !empty($extraCSS) ) { echo $extraCSS; } ?> <?php if( $hasBackground ) { echo 'bgSet'; } ?>" data-module="<?= $moduleIdentifier ?>" id="<?php echo $_moduleIdentifier; ?>">
	<div class="container">
		<div class="row">
			<div class="<?php echo $leftCol; ?> heading">
				<?php
				// get_template_part( 'blocks/modules/content-blocks/text','',[]);
				if ( have_rows('left_column') ) :
					while ( have_rows('left_column') ) :
						the_row();
						$moduleLayout = get_row_layout();
						switch ( $moduleLayout ) :
							case 'header_block' :{
								get_template_part( 'blocks/modules/content-blocks/header','',[ 'heading' => get_sub_field('heading') ]);
							} break;
							case 'copy_block' :{
								get_template_part( 'blocks/modules/content-blocks/text','',[ 'copy' => get_sub_field('copy') ]);
							} break;
							case 'mixed' :{
								get_template_part( 'blocks/modules/content-blocks/mixed','',[ 'heading' => get_sub_field('heading'), 'copy' => get_sub_field('copy') ]);
							} break;
							case 'buttons' :{
								get_template_part( 'blocks/modules/content-blocks/button','',[ 'buttons' => get_sub_field('button_group') ]);
							} break;
						endswitch;
					endwhile;
				endif;

				?>
			</div>
			<div class="<?php echo $rightCol; ?> bulk">
				<?php
				// get_template_part( 'blocks/modules/content-blocks/text','',[]);
if ( have_rows('right_column') ) :
	while ( have_rows('right_column') ) :
		the_row();

		$moduleLayout = get_row_layout();
		// echo "<p class=\"testex\">$moduleLayout</p>";

		switch ( $moduleLayout ) :

			case 'copy' :{
				get_template_part( 'blocks/modules/content-blocks/text','',[ 'copy' => get_sub_field('copy') ]);
			} break;

			case 'image' :{
				get_template_part( 'blocks/modules/content-blocks/image','',[ 'image' => get_sub_field('image'), 'caption' => get_sub_field('caption') ]);
			} break;

			case 'downloads' :{
				get_template_part( 'blocks/modules/content-blocks/download','',[ 'bottom-border' => get_sub_field('bottom_border'), 'downloads' => get_sub_field('documents') ]);
			} break;

			case 'email' :{
				get_template_part( 'blocks/modules/content-blocks/email','',[ 'email' => get_sub_field('email_address') ]);
			} break;

			case 'buttons' :{
				get_template_part( 'blocks/modules/content-blocks/button','',['buttons' => get_sub_field('button_group') ]);
			} break;

			case 'accordion' :{
				get_template_part( 'blocks/modules/content-blocks/accordion','',[ 'heading' => get_sub_field('heading'), 'content' => get_sub_field('content') ]);
			} break;

			case 'ordered_list' :{
				get_template_part( 'blocks/modules/content-blocks/ordered_list','',[ 'numbering_position' => get_sub_field('numbering_position'), 'start' => get_sub_field('start'), 'items' => get_sub_field('items') ]);
			} break;

			case 'highlight_box' :{
				get_template_part( 'blocks/modules/content-blocks/highlight_box','',[ 'copy' => get_sub_field('copy') ]);
			} break;
		endswitch;
	endwhile;
endif;
?>

			</div>
		</div>
	</div>
</section>