<?php
/*
layout: news_filter
source/scss/modules/news_filter.scss (shares some styles latest_news)
*/
$moduleIdentifier = $args['module_identifier'];
$extraCSS = get_sub_field('extra_css');
/*
$args['featured']

extra_css
items_per_page
*/

$featured = $args['featured'];
$filter = 'all';
$page = 1;
// $filter = 'document-update';
// $filter = 'press-release';
$items_per_page = get_sub_field('items_per_page');
$categoryFilters = getAllPostCategories(); // 'posts','categories'

// get post data with iamge/thumbnail IDs
$normalPosts = getNormalPosts( $featured, $filter, $items_per_page, $page, false ); // 'pages', 'page', 'ids', 'posts'

$currentPage = $normalPosts['page'];
$maxPages = $normalPosts['pages'];
?>

<style>
#<?= $moduleIdentifier; ?>{}	
</style>
<section class="module latest_news news_filter<?php if( !empty($extraCSS) ) { echo $extraCSS; } ?>" id="<?= $moduleIdentifier; ?>">
	<div class="bg"></div>
    <div class="container z2">
      <div class="row">
        <div class="col">
          <div class="filter" id="filter-control-<?php echo $moduleIdentifier; ?>">
            <p class="control"><span class="f1">FILTER BY </span><span class="category">CATEGORY</span><span class="icon"></span></p>
            <div class="clipper">
              <ul>
                <li <?php if ( $filter === 'all' ) : ?>class="active"<?php endif; ?> ><button data-action="all" data-parent="<?php echo $moduleIdentifier; ?>">All</button></li>

                <?php foreach ($categoryFilters['categories'] AS $filterItem) : ?>
                <li <?php if ( $filter === strtolower($filterItem['slug']) ) : ?>class="active"<?php endif; ?> ><button data-action="<?php echo strtolower($filterItem['slug']); ?>" data-parent="<?php echo $moduleIdentifier; ?>"><?php echo $filterItem['name']; ?></button></li>
                <?php endforeach; ?>

              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

	<div class="col newsItems" data-featured="<?= implode(',', $featured) ?>" data-items-per-page="<?= $items_per_page ?>" data-page="1" data-filter="all">
		<div class="wt">
			<div class="container">
				<div class="row newsItemTarget">
					<?php foreach( $normalPosts['posts'] AS $key => $post) {

						$iconClass = '';
						$hasExternalLink = false;
						$postlink = $post['permalink'];
						// attachment icon
						if ( $post['attachments'] !== 'none' ) {
							$iconClass = "attachment";
						}

						// thumbnail
						$thumbnailID = ( !empty($post['thumbnail']) ) ? $post['thumbnail'] : false;
						$thumbnail = ( $thumbnailID ) ? wp_get_attachment_image_src($thumbnailID, 'full') : false;
						// 0 URL
						// 1 width ZERO FOR SVG files
						// 2 height ZERO FOR SVG files
						$thumbnailPath = ( $thumbnail ) ? $thumbnail[0] : '';

						// need to check for svg images
						$isSVG = false;
						// /.*\.(svg|SVG)$/ test for svg fiel extension at end of path
						preg_match( '/.*\.(svg|SVG)$/', $thumbnailPath, $matches);
						if ( count($matches) > 0 ) {
							$isSVG = true;
						}

						$tw = $thumbnail[1];
						$th = $thumbnail[2];

						// check to see width is not zero, WP returns zero for width and height for SVG files, leading to a divide by zero error
						$ta = ( $tw > 0) ? $th / $tw : 9999.0; // SVG image, force 'scale by height'

						// $cAspect = 0.61;
						$cAspect = 1.0;

						$link = $post['permalink'];
						$isExternalLink = false;
						if ( $post['has_link'] === 'yes') {
			                $isExternalLink = ( $post['external_link'] === 'yes' );
			                $link = $post['link_url'];
							$iconClass = "externalLink";
						}

					?>
					<div class="col-lg-3 col-sm-6 item" id="post<?= $post['id'] ?>">
						<a href="<?= $link ?>" <?php if ( $isExternalLink ) { echo "target=\"_blank\""; } ?> class="overlay"><span></span></a>
						<figure <?php if ( $ta > $cAspect ) { echo "class=\"scaleByHeight\""; } ?>>
							<img src="<?= $thumbnailPath; ?>" alt="<?= $post['title'] ?>">
						</figure>
						<div class="info <?= $iconClass ?>">
							<p class="category"><?= $post['category'] ?></p>
							<h3><?= $post['title'] ?></h3>
						</div>
						<p class="extra"><?= $post['source'] ?>, <?= $post['date'] ?></p>
					</div>
					<?php
					}
					?>
				</div>

				<?php if ( $maxPages > 1 ) : ?>
				<div class="row">
					<div class="col">
						<div class="paging" data-min="1" data-max="<?= $maxPages ?>">
							<button data-action="1" class="btn transparent first <?php if ( $currentPage === 1 ) : ?>current<?php endif; ?>">First</button>
							<?php
							$n = 0;
							while ($n < $maxPages ) {
								$page = $n + 1;
							?>
								<button data-action="<?= $page ?>" class="btn transparent number <?php if ( $currentPage === $page ) : ?>current<?php endif; ?>"><?= $page ?></button>	
							<?php
								$n++;
							}
							?>
							<button data-action="<?= $page ?>" data-a="<?= $currentPage ?>" data-b="<?= $maxPages ?>" class="btn transparent last <?php if ( $currentPage == $maxPages ) : ?>current<?php endif; ?>">Last</button>	
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="col-lg-3 col-sm-6 item" id="itemTemplate">
		<a href="" class="overlay"><span></span></a>
		<figure>
			<img src="/wp-content/uploads/2021/11/news-thumbnail5.jpg" alt="TITLE">
		</figure>
		<div class="info">
			<p class="category">CATEGORY</p>
			<h3>DISPLAYED TITLE</h3>
		</div>
		<p class="extra">SOURCE, DATE</p>
	</div>

</section>