<?php
/*
layout: home_complex_image
source/scss/modules/home_complex_image.scss
*/

/*
extra_css
copy WISWYG
has_buttons  ('no','yes')
buttons[
	type ('transparent','green')
	label
	link
	is_external_link ('no','yes')
]
mode ('static','fixed','parallax')
foreground (image ID)
background (image ID)
*/
$moduleIdentifier = $args['module_identifier'];

$extraCSS = get_sub_field('extra_css');
$copy = get_sub_field('copy');
$mode = get_sub_field('mode');
$foregroundID = get_sub_field('foreground');
$foregroundPath = ( !empty($foregroundID) ) ? wp_get_attachment_image_src($foregroundID, 'full')[0] : '';
$backgroundID = get_sub_field('background');
$backgroundPath = ( !empty($backgroundID) ) ? wp_get_attachment_image_src($backgroundID, 'full')[0] : '';

$hasButtons = get_sub_field('has_buttons') ==='yes';

?>
<style>
#<?php echo $moduleIdentifier; ?>{}	
</style>
<section class="module home_complex_image <?php if ( !empty($extraCSS) ) { echo $extraCSS; } ?> <?= $mode ?>" id="<?php echo $moduleIdentifier; ?>">
	<div class="complex">
		<div class="heading">
			<?= $copy ?>
			<?php
			if ( $hasButtons ) :
				foreach ( get_sub_field('buttons') AS $button ) :
			?>
				<a href="<?= $button['link'] ?>" class="btn2 <?= $button['type'] ?>" <?php if ( $button['is_external_link'] === 'yes' ) { echo "target=\"_blank\""; } ?>><?= $button['label'] ?></a>
			<?php
				endforeach;
			endif;
			?>
		</div>
		<?php if(false) : ?>
		<div class="xbox">
			<?= $copy ?>
		</div>
		<!-- 
			two layers:
			foreground transsparent pattern PNG
			background: normal image either position fixed or parallax
		-->
		<?php endif; ?>
		<div class="graphic parallaxbg">
			<img alt="" src="<?= $foregroundPath ?>">			
		</div>

	</div>
</section>