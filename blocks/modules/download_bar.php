<?php
/*
layout: download_bar
source/scss/modules/download_bar.scss

label
caption
file_url
file_size
background
*/

/*
get size of file from attachment id
ilesize( get_attached_file( $attachment->ID ) );
*/

$moduleIdentifier = $args['module_identifier'];
$background = 'http://arttree.local/wp-content/uploads/2021/11/download-bar-background.jpg';

$buttonCaption1 = get_sub_field('label')." (filesize: ".get_sub_field('file_size').")";
$buttonCaption2 = get_sub_field('label')."&nbsp;<span>(filesize: ".get_sub_field('file_size').")</span>";
$bg0 = get_sub_field('background');
$bg1 = ( !empty($bg0) ) ? wp_get_attachment_image_src($bg0, 'full')[0] : '';
?>
<style>
#<?php echo $moduleIdentifier; ?>{
	background-image:url('<?php echo $bg1; ?>');
}	
</style>
<section class="module download_bar" id="<?php echo $moduleIdentifier; ?>">
	<h2><?php echo get_sub_field('caption'); ?></h2>
	<a class="btn2 download" href="<?php echo get_sub_field('file_url'); ?>" title="<?php echo $buttonCaption1; ?>" ><?php echo get_sub_field('label'); ?> <span>(filesize: <?php echo get_sub_field('file_size'); ?>)</span></a>
</section>