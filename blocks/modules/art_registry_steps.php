<?php
/*
layout: art_registry_steps
source/scss/modules/art_registry_steps.scss

extra_css
left_column
right_column
*/
$moduleIdentifier = $args['module_identifier'];
$extraCSS = get_sub_field('extra_css');
$steps = get_sub_field('steps');
// echo "<pre>".print_r($steps, true)."</pre>";
?>
<style>
#artregistrysteps{
}	
</style>
<section class="module art_registry_steps if ( !empty($extraCSS) ) { echo $extraCSS; } ?>" id="artregistrysteps">
	<div class="container">
		<div class="row">
			<div class="col">
				<ol>
					<?php foreach( $steps AS $step ) :
					?>
					<li>
						<div class="sub accordion">
							<div class="title">
								<h2><?= $step['title'] ?></h2>
							</div>
							<div class="clipper">
								<div class="content">
									<?php foreach( $step['blocks'] AS $blocks ) :
										$layout = $blocks['acf_fc_layout'];
										switch ( $layout ) {
											case 'wsyiwyg_text_block' : {
												echo $blocks['text'];
											} break;
										}
									?>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</li>
					<?php endforeach; ?>

				</ol>
			</div>
		</div>
	</div>
</section>