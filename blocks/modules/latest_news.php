<?php
/*
layout: latest_news
source/scss/modules/latest_news.scss
*/
$moduleIdentifier = $args['module_identifier'];
$extraCSS = get_sub_field('extras_css');
/*
featured
show_filter ('no','yes')
show_paging ('no','yes')
show_items 4 for home page, 8+ for News 
*/

$_posts = getNormalPosts( false, 'all', 4, 1, false ); // no featured news post, all news posts, 4 news posts displayed, page 1, return image IDs not full path

// echo "<pre>".print_r( $_posts, true )."</pre>";
/*
[has_link] => yes
[external_link] => yes
[link_url] => http://www.gooogle.com
*/
$fourLatestPosts = $_posts['posts'];
/*
$_posts['posts'] = [
	[
		[id] => 311
		[title] => ART Welcomes the Launch of the LEAF Coalition and Call for Proposals from REDD+ Countries
		[synopsis] => Lorem Ipsym
		[source] => ARTTrees
		[thumbnail] => 74
		[category] => Press Release
		[date] => November, 2021
		[rawdate] => 15/11/2021
		[permalink] => http://arttree.local/art-welcomes-the-launch-of-the-leaf-coalition-and-call-for-proposals-from-redd-countries/
		[has_link] => no
	]
	...
]
*/

?>
<style>
#<?php echo $moduleIdentifier; ?>{}	
</style>
<section class="module latest_news <?php if( !empty($extraCSS) ) { echo $extraCSS; } ?>" id="<?php echo $moduleIdentifier; ?>">
	<div class="container">
		<div class="row">
			<div class="col header">
				<h2>Latest News</h2>
				<a href="/news/" class="btn2 transparent">More news</a>
			</div>
		</div>
		<div class="row">
			<?php foreach ( $fourLatestPosts AS $post ) :
				$iconClass = '';
				$hasExternalLink = false;
				$postlink = $post['permalink'];
				// attachment icon
				if ( $post['attachments'] !== 'none' ) {
					$iconClass = "attachment";
				}
				// external link ** icon has priority over attachment
				if ( $post['has_link'] === 'yes' && $post['external_link'] === 'yes' ) {
					$postlink = $post['link_url'];
					$iconClass = "externalLink";
					$hasExternalLink = true;
				}
				// thumbnail
				$thumbnailID = ( !empty($post['thumbnail']) ) ? $post['thumbnail'] : false;
				$thumbnail = ( $thumbnailID ) ? wp_get_attachment_image_src($thumbnailID, 'full') : false;
				// 0 URL
				// 1 width
				// 2 height
				$thumbnailPath = ( $thumbnail ) ? $thumbnail[0] : '';
				$tw = $thumbnail[1];
				$th = $thumbnail[2];
				$ta = $th / $tw;
				// $cAspect = 0.61;
				$cAspect = 1.0;
			?>
			<div class="col-lg-3 item">

				<a href="<?= $postlink ?>" class="overlay" <?php if ( $hasExternalLink ) { echo 'target="_blank" title="'.$post['title'].' (external link)"'; } else { echo 'title="'.$post['title'].'"'; } ?>><span><?= $post['title'] ?></span></a>
				<figure <?php if ( $ta > $cAspect ) { echo "class=\"scaleByHeight\""; } ?> >
					<img src="<?= $thumbnailPath ?>" alt="<?= $post['title'] ?>">
				</figure>
				<div class="info <?= $iconClass ?>">
					<p class="category"><?= $post['category'] ?></p>
					<h3><?= $post['title'] ?></h3>
				</div>
				<p class="extra"><?= $post['source'] ?>, <?= $post['rawdate'] ?></p>
			</div>
			<?php endforeach ?>
		</div>
		<div class="row mobile-more-news">
			<div class="col"><a href="/news/" class="btn2 transparent">More news</a></div>
		</div>
	</div>
</section>