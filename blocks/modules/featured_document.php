<?php
/*
flexible content layout: featured_document
source/scss/modules/featured_document.scss
*/

/*
document -> post type 'document'
	URL
	size
	type
title
copy1
copy2

/*
get_sub_field('document') -> post type 'document'
ACF
	title
	cover_image image->ID
	icon image->ID
	translation[
		language
		file[
			filesize
			filename
			url
		]
	]
*/
$moduleIdentifier = $args['module_identifier'];

$extraCSS = get_sub_field('extra_css');
$jumpTarget = ( !empty(get_sub_field('jump_target'))) ? get_sub_field('jump_target') : false;

$bgColour = ( !empty(get_sub_field('bg_colour')) ) ? get_sub_field('bg_colour') : false;
$bgImage = ( !empty(get_sub_field('bg_image')) ) ? get_sub_field('bg_image') : false;

// no-repeat,repeat-x,repeat-y,repeat
$bgRepeat = ( !empty(get_sub_field('bg_repeat')) ) ? get_sub_field('bg_repeat') : false;
// t,tr,r,br,b,bl,l,tl 
$bgPosition = ( !empty(get_sub_field('bg_position')) ) ? get_sub_field('bg_position') : false;
// normal, contain, cover 
$bgSize = ( !empty(get_sub_field('bg_size')) ) ? get_sub_field('bg_size') : 'normal';
$bgOpacity = ( !empty(get_sub_field('bg_opacity')) ) ? get_sub_field('bg_opacity') : 1.0;

$hasBackground = ( $bgColour || $bgImage );

$textColour = ( !empty(get_sub_field('text_colour')) ) ? get_sub_field('text_colour') : 'dark';

$layout = get_sub_field('layout');
$BoxTitle = get_sub_field('title');
$copy1 = get_sub_field('copy1');
$copy2 = get_sub_field('copy2');
$extraHTML =  ( !empty( get_sub_field('extra_html') ) ) ? get_sub_field('extra_html') : false;

$selectedLanguage = get_sub_field('language');
$featuredDocument = get_sub_field('document'); // post type 'document' use ACF field file to get URL,size,type
$documentID = $featuredDocument->ID;

// echo "<pre>".print_r( get_fields(), true )."</pre>";
// echo "<pre>".print_r( get_fields($documentID), true )."</pre>";

// go through translations and find correct one to display
$_translations = get_field('translation', $documentID);
$selectedTranslation = null;
foreach ( $_translations AS $translation ) {
	if ( $translation['language'] === $selectedLanguage ) {
		$selectedTranslation = $translation;
	}
}

$_filename = $selectedTranslation['file']['filename'];
$_filesize = $selectedTranslation['file']['filesize'];
$documentURL = $selectedTranslation['file']['url'];

// get fiel type from filename via regex
$documentType = preg_replace("#\?.*#", "", pathinfo($_filename, PATHINFO_EXTENSION));

if ( $_filesize > 1000000 ) {
	$suffix = 'Mb';
	$fileSizeNice = $_filesize / 1000000;
}
if ( $_filesize > 1000 && $_filesize < 1000000) {
	$suffix = 'Kb';
	$fileSizeNice = $_filesize / 1000;
}
if ( $_filesize < 1000 ) {
	$suffix = 'bytes';
	$fileSizeNice = $_filesize;
}

$documentSize = number_format($fileSizeNice,1).$suffix;

// encode url for share links
$encodedTo = rawurlencode('no-one@gmail.com');
$encodedSubject = rawurlencode($BoxTitle);
$encodedBody = rawurlencode($documentURL);
$uri = "mailto:$encodedTo?subject=$encodedSubject&body=$encodedBody";
$encodedUri = htmlspecialchars($uri);

$_cover = get_field('cover_image', $documentID);
// get cover image if it set
$coverID = ( !empty($_cover) ) ? $_cover : false;
$documentCover = ( $coverID ) ? wp_get_attachment_image_src($coverID, 'full')[0] : '';

if ( $jumpTarget ) {
	$_moduleIdentifier = $jumpTarget;
} else {
	$_moduleIdentifier = $moduleIdentifier;
}

?>
<style>
#<?php echo $_moduleIdentifier; ?>{
	<?php if ($bgColour) : ?>
		background-color: <?= $bgColour ?>;
	<?php endif; ?>
}
#<?php echo $_moduleIdentifier; ?>::before {
	opacity: <?= $bgOpacity ?>;
	<?php if ($bgImage) :
		$bgImageURL = wp_get_attachment_image_src($bgImage, 'full')[0];
	?>
		background-image: url('<?= $bgImageURL ?>');
	<?php
		if ($bgRepeat) :
	?>
		background-repeat: <?= $bgRepeat ?>;
	<?php endif;
		if ( $bgSize !== 'normal') :
	?>
		background-size: <?= $bgSize ?>;
	<?php endif;
		if ($bgPosition) :
			switch($bgPosition) {
				case 't' : { $position = 'top'; } break;
				case 'tr' : { $position = 'right top'; } break;
				case 'r' : { $position = 'right'; } break;
				case 'br' : { $position = 'right bottom'; } break;
				case 'b' : { $position = 'bottom'; } break;
				case 'bl' : { $position = 'left bottom'; } break;
				case 'l' : { $position = 'left'; } break;
				case 'tl' : { $position = 'left top'; } break;
			}
	?>
		background-position: <?= $position ?>;
	<?php
	endif;
	endif;
	?>
}	
</style>


<section class="module featured_document <?php if( !empty($extraCSS) ) { echo $extraCSS; } ?><?= $layout ?> <?= $textColour ?> <?php if( $hasBackground ) { echo 'bgSet'; } ?>" data-module="<?= $moduleIdentifier ?>" id="<?php echo $_moduleIdentifier; ?>">
	<div class="container" <?php if ( $jumpTarget ) { echo "id=\"$jumpTarget\""; } ?>>
		<div class="row">

			<div class="col-lg-6 image">
				<figure>
					<a href="<?= $documentURL; ?>" title="DOWNLOAD <?= "$BoxTitle (file size: $documentSize $documentType)"; ?>"><img src="<?= $documentCover; ?>" alt="<?= $BoxTitle ?>"></a>
					<!-- <figcaption><?= $BoxTitle ?></figcaption>-->
				</figure>
			</div>

			<div class="col-lg-6 description">
				<?= $extraHTML ?>
				<h2><?= $BoxTitle ?></h2>
				<?= $copy1; ?>

				<?php if ( !empty( $copy2 ) ) : ?>
				<div class="accordion">
					<div class="title">
						<h3>Read more</h3>
					</div>
					<div class="clipper">
						<div class="content">
							<?= $copy2; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>

				<div class="buttons">
					<a href="<?= $documentURL; ?>" title="DOWNLOAD <?= "$title (file size: $documentSize $documentType)"; ?>" class="btn2 green" target="_blank">DOWNLOAD <?= "$title"; ?></a>

					<!-- <a href="https://www.linkedin.com/sharing/share-offsite/?url=<?= $encodedBody ?>" class="btn2 linkedin" title="Share this dpcoument on linkedin" target="_blank"><span>linkedin</span></a> -->
					<!-- <a href="<?= $encodedUri ?>" title="Share this document via email" class="btn2 email"><span>email</span></a> -->
				</div>
			</div>
		</div>
	</div>
</section>