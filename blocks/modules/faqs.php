<?php
/*
flexible content layout: faq
source/scss/modules/faq.scss

extra_css
groups[
	title
	slug
	downloads[
		download[
			<custom post type 'document'>
		]
	]
	faqs[
		[
			'question'
			'response'
		]
	]
]
*/

$moduleIdentifier = $args['module_identifier'];
$extraCSS = get_sub_field('extra_css');
$faqGroups = get_sub_field('groups');
// echo "<pre>".print_r( $faqGroups, true )."</pre>";
$faqs = new FAQs();
// generate left hand filter/navigation menu
$menuItems = [];
$anchors = [];
foreach( $faqGroups AS $faqGroup ) {
	$menuItems[] = [
		'title' => $faqGroup['title'],
		'slug' => $faqGroup['slug']
	];
	$anchors[] = $faqGroup['slug'];
}

$currentGroup = 'belhifet';
/*
preg_match('/^.+\/?gr=([a-z\-\_]+)$/', current_location(), $matches);
$currentGroup = ( array_key_exists( 1, $matches ) ) ? $matches[1] : $menuItems[0]['slug'];

$currentFAQ = [];
foreach ( $menuItems AS $menuItem ) {
	if ( $menuItem['slug'] === $currentGroup ) {
		$currentFAQ = $menuItem;
		break;
	}
}
*/
?>
<script>
	const faqMenu = <?= json_encode( $menuItems ) ?>;
	const anchors = <?= json_encode( $anchors ) ?>;
	const siteURL = '<?= site_url() ?>';
</script>
<style>
#<?php echo $moduleIdentifier; ?>{
}	
</style>


<section class="module faqs <?php if ( !empty($extraCSS) ) { echo $extraCSS; } ?>" id="<?php echo $moduleIdentifier; ?>">
	<div class="container">
		<div class="row space-between">

			<div class="faqfilter">
				<div class="stickybox">
					<div class="control">
						<h2>Jump to a FAQ subject:</h2>
					</div>
					<div class="clipper">
						<ul>
							<?php foreach ( $menuItems AS $index => $menuItem ) :
								// $link = "/faqs?gr={$menuItem['slug']}",
								$link = "/faqs/#{$menuItem['slug']}";
							?>
							<li <?php if ( $index === 0 ) { echo "class=\"active\""; } ?>><a href="<?= $link ?>" title="<?= $menuItem['title'] ?>"  data-target="<?= $menuItem['slug'] ?>"><?= $menuItem['title'] ?></a></li>
							<?php endforeach ?>
						</ul>
					</div>
				</div>
			</div>

			<div class="faqlist">

				<?php foreach( $faqGroups AS $faqGroup ) :
					$downloads = ( !empty($faqGroup['downloads']) ) ? $faqGroup['downloads'] : false;
					$questions = ( !empty($faqGroup['faqs']) ) ? $faqGroup['faqs'] : false;
				?>
					
					<div class="faqGroup <?php if ( $currentGroup === $faqGroup['slug'] ) { echo 'active'; } ?>" data-faqgroup="<?= $faqGroup['slug'] ?>" id="<?= $faqGroup['slug'] ?>">
						<h2 class="groupTitle"><?= $faqGroup['title'] ?></h2>
						<?php if ( $downloads ) : ?>
							<ul class="sub downloads">
								<?php foreach( $downloads AS $downloadP ) :
									$download = $downloadP['download'];
									// check for document/file assigned to FAQ gropu but subsequently binned/deleted
									if ( !empty( $download ) ) :
										$documentID = $download->ID;
										$documentTitle = get_field('title', $documentID);
										$translations = get_field('translation', $documentID);
									?>
								<li class="doc">
									
									<div class="item pdf multiLanguage" id="document-<?= $documentID ?>">
										<h3><?= $documentTitle ?></h3>

										<ul>
											<?php foreach( $translations AS $translation ) :
											$documentProperties = $faqs->getTypeAndSize( $translation['file']['filename'], $translation['file']['filesize'] );
											// $caption = ucfirst($translation['language'])." ({$documentProperties['size']} {$documentProperties['type']})";
											$caption = ucfirst($translation['language']);
											?>
											<li class="translation">
												<a href="<?= $translation['file']['url'] ?>" title="<?= $caption ?>" target="_blank"><?= $caption ?></a></li>
											<?php endforeach ?>
										</ul>	
									</div>
								</li>
							<?php
							endif;
							endforeach;
							?>
							</ul>
						<?php endif ?>

						<?php if ( $questions ) :?>
							<ul class="questions">
								<?php foreach ( $questions AS $question ) :
								?>
								<li class="accordion">
									<div class="title">
										<h2><?= $question['question'] ?></h2>
									</div>
									<div class="clipper">
										<div class="content"><?= $question['response'] ?></div>
									</div>
								</li>
								<?php endforeach ?>

							</ul>
						<?php endif ?>
					</div>	
				<?php endforeach ?>
			</div>
		</div>
	</div>
</section>