<?php
/*
layout: governance
source/scss/modules/governance.scss

groups: 'board-of-directors','sectetariat'
extra_css
*/

$moduleIdentifier = $args['module_identifier'];
$extraCSS = get_sub_field('extra_css');

// get column layout
// default to 5/7 column solit if not defined
$columnLayout = ( !empty(get_sub_field('column_layout')) ) ? get_sub_field('column_layout') : 'five' ;
switch ( $columnLayout ) {
	case 'one' : {
		$leftCol = 'col-lg-1';
		$rightCol = 'col-lg-11';
	} break;
	case 'two' : {
		$leftCol = 'col-lg-2';
		$rightCol = 'col-lg-10';
	} break;
	case 'three' : {
		$leftCol = 'col-lg-3';
		$rightCol = 'col-lg-9';
	} break;
	case 'four' : {
		$leftCol = 'col-lg-4';
		$rightCol = 'col-lg-8';
	} break;
	case 'five' : {
		$leftCol = 'col-lg-5';
		$rightCol = 'col-lg-7';
	} break;
	case 'six' : {
		$leftCol = 'col-lg-6';
		$rightCol = 'col-lg-6';
	} break;
	case 'seven' : {
		$leftCol = 'col-lg-7';
		$rightCol = 'col-lg-5';
	} break;
	case 'eight' : {
		$leftCol = 'col-lg-8';
		$rightCol = 'col-lg-4';
	} break;
	case 'nine' : {
		$leftCol = 'col-lg-9';
		$rightCol = 'col-lg-3';
	} break;
	case 'ten' : {
		$leftCol = 'col-lg-10';
		$rightCol = 'col-lg-2';
	} break;
	case 'eleven' : {
		$leftCol = 'col-lg-11';
		$rightCol = 'col-lg-1';
	} break;
}

$board = getGroup( 'board-of-directors' );
$boardMembers = getPeopleInOrder( $board['members'] );
$secretariat = getGroup( 'secretariat' );
$secretariatMembers = getPeopleInOrder( $secretariat['members'] );

$defaultGroup = $args['defaultGroup'];
// echo "<pre>".print_r( $boardMembers, true )."</pre>";
// echo "<pre>".print_r( $secretariatMembers, true )."</pre>";
?>
<style>
#<?php echo $moduleIdentifier; ?>{
}	
</style>
<section class="module governance <?php if ( !empty($extraCSS) ) { echo $extraCSS; } ?>" id="governance">
	<div class='container'>

		<div class="row">
			<div class="<?= $leftCol ?> leftColumn">
				<ul>
					<li><a href="" class="trigger <?php if ( $defaultGroup === 'board-of-directors' ) { echo "active"; } ?>" data-action="board-of-directors">Board of Directors</a></li>
					<li><a href="" class="trigger <?php if ( $defaultGroup === 'secretariat' ) { echo "active"; } ?>" data-action="secretariat">Secretariat</a></li>
					<li><a href="/program-history">PROGRAM HISTORY</a></li>
					<?php if (false) : ?>
					<li>
						<div class="sub accordion">
							<div class="title">
								<h2>EXPERT COMMITTEES</h2>
							</div>
							<div class="clipper">
								<div class="content">
									<ul>
										<li><a href="" class="doc">INTERIM STEERING COMMITTEE</a></li>
										<li><a href="" class="doc">TREES COMMITTEE</a></li>
										<li><a href="" class="doc">ART COMMITTEE</a></li>
									</ul>
								</div>
							</div>
						</div>
					</li>
				<?php endif ?>
				</ul>
			</div>
			<div class="<?= $rightCol ?> rightColumn">
				<div class="switchpanel">
					<div class="panel active" data-subject="board-of-directors">
						<h3><?= $board['name'] ?></h3>
						<p><?= $board['content'] ?></p>
					</div>
					<div class="panel" data-subject="secretariat">
						<h3><?= $secretariat['name'] ?></h3>
						<p><?= $secretariat['content'] ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col peopleGrid">
		<div class="switchpanel">
			<div class="panel <?php if ( $defaultGroup === 'board-of-directors' ) { echo "active"; } ?>" data-subject="board-of-directors">
		<?php
		if ( count( $board ) > 0 ) {
			$args = [
				'group' => 'board-of-directors',
				'members' => $boardMembers,
				'membership' => false,
				'subject' => false
			];
			get_template_part( 'blocks/common/people-grid', '', $args);
		}
		?>
			</div>
			<div class="panel <?php if ( $defaultGroup === 'secretariat' ) { echo "active"; } ?>" data-subject="secretariat">
		<?php
		if ( count( $secretariat ) > 0 ) {
			$args = [
				'group' => 'sectetariat',
				'members' => $secretariatMembers,
				'membership' => false,
				'subject' => false
			];
			get_template_part( 'blocks/common/people-grid', '', $args);
		}
		?>
				
			</div>

		
	</div>		
</section>
