<?php
/*
horizontal_divider
*/
$moduleIdentifier = $args['module_identifier'];
// background-image (image ID)
$backgroundImage = ( !empty( get_sub_field('background-image') ) ) ? get_sub_field('background-image') : false;
?>
<style>
	<?php if ($backgroundImage) :
		$backgroundImagePath = wp_get_attachment_image_src($backgroundImage, 'full')[0];
	?>
	#<?php echo $moduleIdentifier; ?>{
		background-image: url('<?= $backgroundImagePath ?>');
	}		
	<?php endif; ?> 
</style>
<section class="module horizontal_divider <?php if ($backgroundImage) { echo "d-image"; } ?>" id="<?php echo $moduleIdentifier; ?>">
	<?php if ($backgroundImage) :
		wp_get_attachment_image_src($backgroundImage, 'full')[0];
	?>
		<!-- <?= $backgroundImage ?> -->
	<?php else : ?>
	<div class="container">
		<div class="row">
			<div class="col">
				<hr />
			</div>
		</div>
	</div>
	<?php endif; ?> 
</section>
