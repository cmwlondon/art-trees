<?php
// ordered list content block

// numbering_position ('left','above')

$numbering = $args['numbering_position'];
$start = (int) $args['start'];

?>
<!-- <?= $start ?> -->
<div class="sub ordered_list <?= $numbering ?> <?php if ( $start === 0 ) : ?>no-numerals<?php endif; ?>" >
	<ol <?php if ( $start > 1) { echo "start=\"$start\""; } ?> >
		<?php foreach( $args['items'] AS $key1 => $item ) :
			$content = $item['item_content'];
		?>
		<li>
			<?php if ($start > 0) : ?><span class="marker"><?php echo str_pad(($key1 + $start), 2, "0", STR_PAD_LEFT); ?></span><?php endif; ?>
			<div class="li">
				<?php
					foreach( $content AS $key2 => $block ) :

						switch ( $block['acf_fc_layout'] ) :

							case 'copy' :{
								get_template_part( 'blocks/modules/content-blocks/text','',[ 'copy' => $block['copy'] ]);
							} break;

							case 'image' :{
								get_template_part( 'blocks/modules/content-blocks/image','',[ 'image' => $block['image'], 'caption' => $block['caption'] ]);
							} break;

							case 'downloads' :{
								get_template_part( 'blocks/modules/content-blocks/download','',[ 'bottom-border' => $block['bottom_border'], 'downloads' => $block['documents'] ]);
							} break;

							case 'email' :{
								get_template_part( 'blocks/modules/content-blocks/email','',[ 'email' => $block['email_address'] ]);
							} break;

							case 'buttons' :{
								get_template_part( 'blocks/modules/content-blocks/button','',['buttons' => $block['button_group'] ]);
							} break;

							case 'accordion' :{
								get_template_part( 'blocks/modules/content-blocks/accordion','',[ 'heading' => $block['heading'], 'content' => $block['content'] ]);
							} break;

							// not advisable to double nest ordered_list
							case 'ordered_list' :{
								// get_template_part( 'blocks/modules/content-blocks/ordered_list','',[ 'items' => $block['items'] ]);
							} break;

						endswitch;
					endforeach;

				?>

			</div>
		</li>
		<?php endforeach; ?>
	</ol>
</div>