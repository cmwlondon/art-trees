<?php
?>
<div class="sub accordion">
	<div class="title">
		<h2><?php echo $args['heading']; ?></h2>
	</div>
	<div class="clipper">
		<div class="content">
			<?php echo $args['content']; ?>
		</div>
	</div>
</div>
