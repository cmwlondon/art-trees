<?php
// downlod content block
// 'bb' css class add bototm border
/*
[documents] => Array(
    [0] => Array
        (
            [document] => 82
            [caption] => example caption
        )

)
*/

/*
[url] => http://arttree.local/wp-content/uploads/2021/11/TREES-2.0-August-2021-Clean.pdf
[link] => http://arttree.local/document/the-redd-environmental-excellence-standard-trees/trees-2-0-august-2021-clean/
[alt] => */

// get post title, ACF field 'title' and 'caption' - pick one



$documents = $args['downloads'];
$hasBottomBorder = $args['bottom-border'] === 'yes';

$thisDocHandler = new DocumentHandler(false,false);
?>
<div class="sub download <?php if ( $hasBottomBorder ) { echo "bb"; } ?>">
	<ul>
		<?php foreach($documents AS $key => $documentItem) :
			$documentID = $documentItem['document'];
			$_caption = $documentItem['caption'];
			$_documentPost = $thisDocHandler->getDocumentByID( $documentID );
			// webinarcaption
			// webinarlink
			$translations = $_documentPost['translations'];
			$isMultiLanguage = ( count($translations) > 1 );
		?>
		<li data-docid="<?= $documentID ?>">
			
			<?php if( $isMultiLanguage ) : ?>
			<div class="docdl multiLanguage">
				<h3><?= $_caption ?></h3>
				<ul>
				<?php foreach ( $translations AS $translation ) :
					$language = ucfirst($translation['language'])
				?>
					<li><a href="<?= $translation['url'] ?>"><?= $language ?></a></li>
				<?php endforeach ?>
				</ul>
			<?php if( $_documentPost['webinarcaption'] && $_documentPost['webinarlink'] ) : ?><p class="webinar"><a href="<?= $_documentPost['webinarlink'] ?>" target="_blank" title="<?= $_documentPost['webinarcaption'] ?>"><?= $_documentPost['webinarcaption'] ?></a></p><?php endif; ?>
			</div>

			<?php else :
				$translation = $translations[0];
				$language = ucfirst( $translation['language'] );
				$url = $translation['url'];
				$size = $translation['size'];
				$type = $translation['type'];
			?>

			<?php if( $_documentPost['webinarcaption'] && $_documentPost['webinarlink'] ) : ?>
				<div class="docdl <?= $type ?> multiLanguage">
					<h3><?= $_caption ?></h3>
					<p><a href="<?= $url; ?>" title="(<?= $language ?>)" target="_blank"><span><?= $language ?></span></a></p>
					<p class="webinar"><a href="<?= $_documentPost['webinarlink'] ?>" target="_blank" title="<?= $_documentPost['webinarcaption'] ?>"><?= $_documentPost['webinarcaption'] ?></a></p>
				</div>
			<?php else: ?>
				<div class="docdl <?= $type ?>">
					<a href="<?= $url; ?>" class="dl overlay" title="<?= $_caption ?>" target="_blank"><span><?= $_caption ?></span></a>
					<h3><?= $_caption ?></h3>
					<p>(<?= $language ?>)</p>
				</div>
			<?php endif;
		endif; ?>
		</li>
		<?php endforeach; ?>
	</ul>
</div>
