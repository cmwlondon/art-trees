<?php
/*
	[image] => 89
	[caption] => example image caption
*/
$imageID = ( !empty($args['image']) ) ? $args['image'] : false;
$imagePath = ( $imageID ) ? wp_get_attachment_image_src($imageID, 'full')[0] : '';
$caption = ( !empty($args['caption']) ) ? $args['caption'] : false;

?>
<div class="sub image">
	<figure>
		<div><img src="<?php echo $imagePath ; ?>"></div>
		<?php if ( $caption ) : ?><figcaption><?php echo $caption; ?></figcaption><?php endif; ?>
	</figure>
</div>
