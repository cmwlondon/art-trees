<?php
/*
button_group[
    [
        [button_type] => transparent  REQUIRED
            transparent : Transparent
            green : Green
            download : Download
        [caption_main] => Go to a page  REQUIRED
        [caption_secondary] => 
        [url] => / REQUIRED
        [new_window] => no REQUIRED
            no : No
            yes : Yes
    ],
    ...
]
*/
	$buttons = $args['buttons'];
?>
<div class="sub button">
<?php foreach( $buttons AS $key => $button) :
	$type = $button['button_type'];
	$main = $button['caption_main'];
	$secondary = $button['caption_secondary'];
	$url = $button['url'];
	$opneInNewWindoe = ($button['new_window'] === 'yes');

?>
	<a href="<?php echo $url; ?>" class="btn2 <?php echo $type; ?>" title="" <?php if ( $opneInNewWindoe ) : ?>target="_blank"<?php endif; ?>><?php echo $main; ?> <?php if ( !empty($secondary) ) { echo "<span>($secondary)</span>"; } ?></a>
<?php endforeach; ?>
</div>
