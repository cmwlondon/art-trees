<?php
/*
layout: featured_news
source/scss/modules/featured_news.scss
*/
$moduleIdentifier = $args['module_identifier'];

/*
post fields
ID
post_date
post_date_gmt
post_content
post_title
post_name -> permalink
post_type

page ACF fields:
extra_css
$featured_items -> featured

featured post ACf fields:
	[displayed_title] => ART Approves TREES Concepts for Two Jurisdictions
	[synopsis] => 
	[source] =>
	[thumbnail] => 73
	[image] => 62
	has_link ('no','yes')
	external_link ('no','yes')
	link_url
	attachments[
		type ('file', 'document')
		file -> file type
		document -> post type 'document' object
		cover_image (image ID)
	]
*/

/*
$imageID = ( !empty($args['image']) ) ? $args['image'] : false;
$imagePath = ( $imageID ) ? wp_get_attachment_image_src($imageID, 'full')[0] : '';
*/
$featuredPosts = $args['featured'];

$featuredItems = [];
foreach( $featuredPosts AS $key => $item ) {
	$featuredItem =  $item['item'];
	$featuredItemID = $featuredItem->ID;

	$featuredPostACF = get_fields( $featuredItemID );
	$thisPostCategories = wp_get_post_categories( $featuredItemID, [] ); // returns category ID
	$category = get_category( $thisPostCategories[0] ); // get FIRST category
	$permalink =  '/'.$featuredItem->post_name;
	$imageID = ( !empty($featuredPostACF['image']) ) ? $featuredPostACF['image'] : false;
	$imagePath = ( $imageID ) ? wp_get_attachment_image_src($imageID, 'full')[0] : '';
	$has_link = ($featuredPostACF['has_link'] === 'yes');

	// echo "<p class=\"testex\">post ID: $featuredItemID</p>";
	// echo  "<pre>".print_r($featuredPostACF, true)."</pre>";

	$featuredItems[] = [
		'id' => $featuredItemID,
		'title' => $featuredPostACF['displayed_title'],
		'synopsis' => $featuredPostACF['synopsis'],
		'category' => $category->name,
		'permalink' => $permalink,
		'image' => $imagePath,
		'date' => get_the_date("F, Y", $featuredItemID),
		'source'=> $featuredPostACF['source'],
		'has_link' => $has_link,
		'external_link' => ( $has_link ) ? $featuredPostACF['external_link'] : '',
		'link_url' => $has_link ? $featuredPostACF['link_url'] : '',
		'attachments' => $featuredPostACF['attachments']
	];
}


?>
<style>
#<?php echo $moduleIdentifier; ?>{}	
</style>
<section class="module featured_news <?php if( !empty($extraCSS) ) { echo $extraCSS; } ?>" id="<?php echo $moduleIdentifier; ?>">
	<div class="container">
		<div class="row">

			<div class="col">
				<h2>features</h2>
				<div class="track">
					<?php foreach ( $featuredItems AS $key => $item ) {
						if ( $item['has_link'] ) {
							// link spoecified
							$link = $item['link_url'];
							$external = ($item['external_link'] === 'yes');
						} else {
							$link = $item['permalink'];
							$external = false;
						}
					?>
					<div class="item" id="post<?= $item['id'] ?>">
						<div class="bg1"></div>
						<div class="flexy">
							<div class="description">
								<p class="category"><?= $item['category'] ?></p>
								<h3><a href="<?= $link ?>" <?php if ( $external ) echo "target=\"_blank\""; ?>><?= $item['title']?></a></h3>
								<p><?= $item['synopsis']?></p>
								<p class="source"><?= $item['source']?> - <?= $item['date']?></p>
							</div>
							<?php if (!empty($item['image'])) : ?>
							<div class="image">
								<figure>
									<a href="<?= $link ?>" <?php if ( $external ) echo "target=\"_blank\""; ?> title="<?= $item['title']?>"><img src="<?= $item['image'] ?>" alt="<?= $item['title']?>"></a>
									<!-- <figcaption><?= $title ?></figcaption> -->
								</figure>
							</div>
							<?php endif; ?>
						</div>
					</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
</section>