<?php
/*
layout: art_registry_intro
source/scss/modules/art_registry_intro.scss

extra_css
left_column
right_column
*/

$moduleIdentifier = $args['module_identifier'];
$extraCSS = get_sub_field('extra_css');
$leftColumn = get_sub_field('left_column');
$rightColumn = get_sub_field('right_column');

?>
<style>
#<?php echo $moduleIdentifier; ?>{
}	
</style>
<section class="module art_registry_intro <?php if ( !empty($extraCSS) ) { echo $extraCSS; } ?>" id="<?php echo $moduleIdentifier; ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 leftcolumn">
				<?= $leftColumn ?>
			</div>
			<div class="col-lg-8 rightcolumn">
				<?= $rightColumn ?>
			</div>
		</div>

	</div>
</section>