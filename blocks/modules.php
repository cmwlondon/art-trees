<?php
/* 'Modules' modules flexible content element
layout types:
two_column_text+text
two_column_text+image
home_complex_image
latest_news
document_list
download_bar
art_registry_into
art_registry_steps
*/

if ( have_rows('modules') ) :
  while ( have_rows('modules') ) :
    the_row();
    $moduleLayout = get_row_layout();

      /*
      '+' is INVALID in HTML ID attribute so 'two_column_text+text' and 'two_column_text+image' do not work
      substitute '+' character for another
      */
      $moduleIdentifier = "module-".str_replace('+','-',$moduleLayout)."-".get_row_index();

      // echo "<p class=\"testex\">module: $moduleLayout</p>\n";

      $args = [
        'module_identifier' => $moduleIdentifier
      ];

      switch ( $moduleLayout ) :
        
        case 'horizontal_divider' :{
          get_template_part( 'blocks/modules/horizontal_divider', '', $args);
        } break;
        case 'two_column_text+text' :{
          get_template_part( 'blocks/modules/two_column_text+text', '', $args);
        } break;

        case 'two_column_text+image' :{
          get_template_part( 'blocks/modules/two_column_text+image', '', $args);
        } break;

        case 'home_complex_image' :{
          get_template_part( 'blocks/modules/home_complex_image', '', $args);
        } break;

        case 'latest_news' :{
          get_template_part( 'blocks/modules/latest_news', '', $args);
        } break;

        case 'document_list' :{
          get_template_part( 'blocks/modules/document_list', '', $args);
        } break;

        case 'download_bar' :{
          get_template_part( 'blocks/modules/download_bar', '', $args);
        } break;

        case 'art_registry_intro' :{
          get_template_part( 'blocks/modules/art_registry_intro', '', $args);
        } break;

        case 'art_registry_steps' :{
          get_template_part( 'blocks/modules/art_registry_steps', '', $args);
        } break;

        case 'faqs' :{
          get_template_part( 'blocks/modules/faqs', '', $args);
        } break;

        case 'governance' :{
          $args['defaultGroup'] = 'board-of-directors';
          get_template_part( 'blocks/modules/governance', '', $args);
        } break;

        case 'featured_document' :{
          get_template_part( 'blocks/modules/featured_document', '', $args);
        } break;

        case 'featured_news' :{
          // get featured item
          $featured_items = get_field('featured_items'); // array of post objects
          $args['featured'] = $featured_items;
          get_template_part( 'blocks/modules/featured_news', '', $args);
        } break;

        case 'news_filter' :{
          // get featured item
          $featured_items = get_field('featured_items'); // array of post objects

          $featuredItemIDs = [];
          foreach( $featured_items AS $key => $item ) {
            $featuredItemIDs[] =  $item['item']->ID;
          }

          // $featured->ID
          $args['featured'] = $featuredItemIDs;

          get_template_part( 'blocks/modules/news_filter', '', $args);
        } break;

        /*
        // home page + work
        case 'latest_clients' : {

          $clients = [];

          // manually select which clients to display
          // alternate - query all custom post type 'clients', order by post_title
          $queryArguments = [
            'post_type' => 'clients',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order'   => 'ASC'
          ];
          $the_query = new WP_Query( $queryArguments );
          $initalPostCount = $the_query->post_count;

          if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
              $the_query->the_post();

              $post_id = get_the_ID();
              $clients[] = [
                'id' => $post_id,
                'title' => get_field('title', $post_id),
                'logo' => get_field('logo', $post_id)
              ];
            }          
          }
          wp_reset_postdata();

          $arguments = [
            'module_identifier' => $moduleIdentifier,
            'block-title' => get_sub_field('title'),
            'block-title-colour' => get_sub_field('title_colour'),
            'block-subtitle' => get_sub_field('subtitle'),
            'block-subtitle-colour' => get_sub_field('subtitle_colour'),
            'background-colour' => get_sub_field('background_colour'),
            'background-image' => get_sub_field('background_image'),
            'grid-width' => get_sub_field('grid_width'),
            'block-link-present' => get_sub_field('block_link_present'),
            'block-link' => get_sub_field('block_link'),
            'clients' => $clients
          ];

          get_template_part( 'blocks/common/modules/latest-clients','',$arguments);
        } break;
        */


      endswitch;
  endwhile;
endif;
?>

