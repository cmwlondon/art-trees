/* default */

let winheight = 0;
let threequarterheight = 0;
let halfheight = 0;
let quarterheight = 0;

let pageSize = null;
let breakpoint = 992;
let stickybreadcrumbTrigger = 0;

let pSet = [];

/* ---------------------------------------------------------------- */

function windowDimensions() {
	windowSize = {
		"w" : $(window).width(),
		"h" : $(window).height(),
		"a" : $(window).width() / $(window).height()
	};

	if (windowSize.a > 1) {
		landscapeAspect = true;

		if (landscapeAspect && windowSize.h < 500) {
			shortLandscape = true;
		} else {
			shortLandscape = false;
		}
	} else {
		landscapeAspect = false;
	}

	if(landscapeAspect && !shortLandscape) {
		$('html').addClass('landscapeAspectRatio');
	}
	if(landscapeAspect && shortLandscape) {
		$('html').addClass('shortLandscape');
	}
	if(!landscapeAspect) {
		$('html').addClass('portraitAspectRatio');
	}

	winheight = $(window).height();
	threequarterheight = Math.floor( winheight * 0.75);
	halfheight = Math.floor( winheight * 0.5);
	quarterheight = Math.floor( winheight * 0.25);
}

/* ---------------------------------------------------------------- */

$(document).ready(function () {

	// check for iOS devices
	let isIOS = (/iPad|iPhone|iPod/.test(navigator.platform) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1)) && !window.MSStream;
	// console.log(isIOS);
	if (isIOS) {
		$('html').addClass('is-ios');
	}

	windowDimensions();

	pageSize = ( windowSize.w > breakpoint ) ? 'desktop' : 'mobile';

	// set the scroll offset for the breadcrumb to switch from static to fixed (on pagebanner + breadcrumb)
	stickybreadcrumbTrigger = ( windowSize.w > breakpoint ) ? 600 : 700; 

	$(window).on('resize',() => {
		windowDimensions();
		pageSize = ( windowSize.w > breakpoint ) ? 'desktop' : 'mobile';
		stickybreadcrumbTrigger = ( windowSize.w > breakpoint ) ? 600 : 700; 
	});

	/* ---------------------------------------------------------------- */

	// mobile nav show/hide elements
	$('div.menuToggle').on('click',function(e){
		e.preventDefault();
		$('header.navheader').toggleClass('open');
	});


	// mobile navigation - stcks to top pf page
	// scroll down - header slides off of top of browser
	// scroll back up - header slides down from top of browser
	function fixNavBar(){
		let scrollState = $(window).scrollTop();
		let navBar = $('header.navheader');
		let bodyBlock = $('body');

		// page banner AND breadcrumb
		if ( $('main').hasClass('mainAll') ) {
			if ( scrollState > stickybreadcrumbTrigger) {
				bodyBlock.addClass('stickyBreadcrumb');
			} else {
				bodyBlock.removeClass('stickyBreadcrumb');
			}
		}

		// fix navbar once user scrolls down
		let newState = ( scrollState > 0 );
		// detect change of state
		if ( newState !== navTogglerStateFixed ) {
			if( newState ) {
				// apply fix
				bodyBlock.addClass('fixed');
			} else {
				// remove fix
				bodyBlock.removeClass('fixed');				
			}
			navTogglerStateFixed = newState;			
		}

		if ( scrollState > 150) {
			bodyBlock.addClass('headerShadow');	
		} else {
			bodyBlock.removeClass('headerShadow');
		}

		// slide navbar in from top of screen when user scrolls up
		if ( scrollState > 0) {
			bodyBlock.addClass('fixedHide');	
			navBar.removeClass('open');
		} else {
			bodyBlock.removeClass('fixedHide');	
		}
		// check for scroll up
		if ( scrollState < p) {
			bodyBlock.addClass('fixedShow');	
		} else {
			bodyBlock.removeClass('fixedShow');
		}
		p = scrollState;
	}

	let navTogglerStateFixed = false;
	let p = 0;

	let navfix = $(window).scroll(() => {
		fixNavBar();
	});
	fixNavBar();

	/* ---------------------------------------------------------------- */

	// scroll down button
	if ( $('a.scrolldown').length > 0 ) {
		$('a.scrolldown').on('click',function(e){
			e.preventDefault();

			let thisSCL = $(this);
			let thisSCLAction = thisSCL.attr('data-target');
			let thisSCLContainer = thisSCL.attr('data-container');
			let parentModule = thisSCL.parents(thisSCLContainer).eq(0);

			switch( thisSCLAction ){

				// go to next module
				// data-target="nextmodule"
				case 'nextmodule' : {
					let ald = parentModule.nextAll('section.module').eq(0);
					let destTop = ald.position().top;
					window.scrollTo(0, destTop );	
				} break;

				// other actions to go here
			}
		});
	}

	/* ---------------------------------------------------------------- */
	// global animations

	// text/image elements x 3
	let imageTextPairs = $('.two_column_text_image .pair');

	imageTextPairs.each(function(i,pair){
		// let pair = imageTextPairs.eq(0);
		pSet.push( new GenericScrollHandler({
			"trigger" : $(pair),
			"subject" : $(pair),
			"animateOnce" : false, // animation only runs once
			"getDimensions" : (instance) => {
				instance.started = false;
				instance.top = instance.trigger.position().top;
				instance.height = instance.trigger.height();
				instance.bottom = instance.top + instance.height;
				// instance.start = instance.top - (instance.wh * 0.6666); // top of module 1/3 up browser window
				instance.start = instance.top - (instance.wh - 150); // top of subject 150px from bottom of screen
			},
			"monitor" : (instance, scrolltop) => {
				let percent = 0;

				if ( scrolltop > instance.start && !instance.started) {
					instance.update(1.0);	
				}
			},
			"update" : (instance, percent) => {
				instance.started = true;
				instance.subject.addClass('animate');
			}
		}));
	});

	// text/text header block
	if ( $('.two_column_text_text .sub.heading').length > 0 ) {

		$('.two_column_text_text').each(function(i, item){
			pSet.push( new GenericScrollHandler({
				"trigger" : $(this),
				"subject" : $(this).find('.sub.heading'),
				"animateOnce" : false, // animation only runs once
				"getDimensions" : (instance) => {
					instance.started = false;
					instance.top = instance.trigger.position().top;
					instance.height = instance.trigger.height();
					instance.bottom = instance.top + instance.height;

					instance.start = instance.top - (instance.wh  - 150); // top of module 1/3 up browser window
					// $('body').append( $('<div></div>').addClass('mark').css({"top" : instance.start + "px","background-color" : "#0f0",}) );
					// $('body').append( $('<div></div>').addClass('mark').css({ "top" : ( instance.top - instance.wh) + "px", "background-color" : "#0f0", }) );

					// instance.end = instance.top; // bottom of module halfway up browser window
					// $('body').append( $('<div></div>').addClass('mark').css({"top" : instance.end + "px","background-color" : "#f00",}) );

					// $('body').append( $('<div></div>').addClass('mark').css({ "top" : instance.top + "px", "background-color" : "#0f0", }) );
					// $('body').append( $('<div></div>').addClass('mark').css({ "top" : instance.bottom + "px", "background-color" : "#f00", }) );
				},
				"monitor" : (instance, scrolltop) => {
					let percent = 0;

					if ( scrolltop > instance.start && !instance.started) {
						instance.update(1.0);	
					}
				},
				"update" : (instance, percent) => {
					instance.started = true;
					instance.subject.addClass('animate');
				}
			}));
		});
	}

	// text/text ordered list elements, buttons
	let listItems = $('.two_column_text_text .sub.ordered_list li, .two_column_text_text .sub.button, .two_column_text_text .sub.text, .two_column_text_text .sub.download');
	listItems.each(function(i, item){
		// let pair = imageTextPairs.eq(0);
		pSet.push( new GenericScrollHandler({
			"trigger" : $(item),
			"subject" : $(item),
			"animateOnce" : false, // animation only runs once
			"getDimensions" : (instance) => {
				instance.started = false;
				instance.top = instance.trigger.position().top;
				instance.height = instance.trigger.height();
				instance.bottom = instance.top + instance.height;
				// instance.start = instance.top - ((instance.wh * 0.6666) + 500); // top of module 1/3 up browser window
				instance.start = instance.top - ((instance.wh - 150) + 100); // top of subject 150px from botto  of screen (100px to account for initial start position of subject)
			},
			"monitor" : (instance, scrolltop) => {
				let percent = 0;

				if ( scrolltop > instance.start && !instance.started) {
					instance.update(1.0);	
				}
			},
			"update" : (instance, percent) => {
				instance.started = true;
				instance.subject.addClass('animate');
			}
		}));
	});

	/* ---------------------------------------------------------------- */

	if ( $('.accordion').length > 0 ) {
		let accordionBlocks = [];
		$('.accordion').each(function(i){
			let thisAccordion = $(this);
			let trigger = thisAccordion.find('.title');
			trigger.on('click', (e) => {
				let parent = $(e.target).parents('.accordion');

				/*
				parent.children().each((index, element) => {
					$(element).get(0).addEventListener('transitionend', (e) => {
						e.stopPropagation();
					});
				})

				let transitionEnd1 = (e) => {
					console.log('transitionend1');
				}

				// add transition end event on this.itemsContainer
				// parent.get(0).addEventListener('transitionend', transitionEnd1, {"once" : true});

				parent.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", transitionEnd1 );
				console.log('transition start');
				*/
				parent.toggleClass('open');
			});			
		});
	}

	/* ---------------------------------------------------------------- */

	if ( $('body.homepage').length > 0 ) {
		// apply animation on page load as this panel is at top of page
		let homeLeadPanel = $('.homeLeadPanel .heading, .homeLeadPanel .xbox');
		homeLeadPanel.addClass('loadAnimate')
	}

	/* ---------------------------------------------------------------- */

	// ART Registry
	if ( $('.art_registry_intro').length > 0 ) {
		$('.art_registry_intro').each(function(i, module){
			pSet.push( new GenericScrollHandler({
				"trigger" : $(module),
				"subject" : $(module).find('.leftcolumn, .rightcolumn'),
				"animateOnce" : false, // animation only runs once
				"getDimensions" : (instance) => {
					instance.started = false;
					instance.top = instance.trigger.position().top;
					instance.height = instance.trigger.height();
					instance.bottom = instance.top + instance.height;
					// instance.start = instance.top - (instance.wh * 0.6666); // top of module 1/3 up browser window
					instance.start = instance.top - (instance.wh - 150); // top of subject 150px from bottom of screen
				},
				"monitor" : (instance, scrolltop) => {
					let percent = 0;

					if ( scrolltop > instance.start && !instance.started) {
						instance.update(1.0);	
					}
				},
				"update" : (instance, percent) => {
					instance.started = true;
					instance.subject.addClass('animate');
				}
			}));
		});

	}

	/* ---------------------------------------------------------------- */

	// News

	if ( $('.news_filter').length > 0 ) {
		// filter/paging
		// classes.js -> NewsLoader
		let newsPageLoader = new NewsLoader({
			"newsModule" : $('.news_filter')
		});

		// mobile filter dropdown
		$('.news_filter .filter .control').on('click',function(e){
			e.preventDefault();
			let filter = $(this).parents('.filter');

			filter.toggleClass('open');
		});

		$('.news_filter .filter ul li button').on('click',function(e){
			e.preventDefault();
			let filter = $(this).parents('.filter');

			filter.removeClass('open');
		});

		let slider = $('section.featured_news .track');
		slider
		// .on('init', function(event, slick, direction){ })
		/*
		.on('setPosition', function (event, slick) {
   			 slick.$slides.css('height', slick.$slideTrack.height() + 'px')
		})
		*/
		.slick({
			"autoplaySpeed" : 5000,
			"autoplay" : false,
			"dots" : true,
			"arrows" : true
		});
	}

	/* ---------------------------------------------------------------- */

	// document list

	if ( $('.document_list').length > 0 ) {
		// filter/paging
		// classes.js -> DocumentFilter

		let docsFilter = new DocumentFilter({
			"documentListModule" : $('.document_list')
		});

		/*
		// mobile filter dropdown
		$('.docfilter .filter .control').on('click',function(e){
			e.preventDefault();
			let filter = $(this).parents('.filter');

			filter.toggleClass('open');
		});

		$('.docfilter .filter ul li a').on('click',function(e){
			e.preventDefault();
			let filter = $(this).parents('.filter');

			filter.removeClass('open');
		});
		*/
	}

	/* ---------------------------------------------------------------- */

	// about us page group switch areas 'board-of-directors'/'secretariat'
	let switchPanels = $('.switchpanel');
	if ( switchPanels.length > 0 ) {
		// bind behaviouor to links in left hand column
		let triggers = $('.governance .trigger');
		triggers.on('click', (e) => {
			e.preventDefault();
			let thisTrigger = $(e.target);
			let action = thisTrigger.attr('data-action');

			// update left hand column
			triggers.removeClass('active');
			thisTrigger.addClass('active');

			// update switched panels
			switchPanels.each((index, parent) => {
				let panels = $(parent).find('.panel');
				panels.each((index, panel) => {
					if ( $(panel).attr('data-subject') == action ) {
						$(panel).addClass('active');
					} else {
						$(panel).removeClass('active');
					}
				});
				
			})

		});
	}

	/* ---------------------------------------------------------------- */
	
	// FAQ page
	// $('section.faqs')

	if( $('section.faqs').length > 0 ) {
		let faqModule = $('section.faqs');
		let thisFAQList = new FAQs({
			"faqmodule" : faqModule,
			"filter" : faqModule.find('.faqfilter'),
			"groups" : faqModule.find('.faqlist .faqGroup')
		});

		// mobile filter open close
		$('.faqfilter .control').on('click',function(e){
			e.preventDefault();
			let filter = $(this).parents('.faqfilter');

			filter.toggleClass('open');
		});
	}
	/*
	history.pushState('data to be passed', 'Title of the page', '/test');
	*/

	/* ---------------------------------------------------------------- */
	
	// AJAX endppints
	/*
	filterNews
	pagingNews
	documentFilter
	documentPaging

	filterFAQ    
	    category

	filterPeople
		category ('board','secretariat')

	personDetails
		person
	*/

	/* ---------------------------------------------------------------- */

	$("#signup-form").submit( (e) => {
		e.preventDefault();
		console.log('signup form submit intercept');

		// client-side validation
	    // non empty, [a-z][A-Z]- '
	    // email non-empty valid email address
	    // org optional [a-z][A-Z]-.
	    // optin non-empty true/yes

		$.ajax({
			url: localize_vars.ajaxurl,
			type: "POST",
			data: {
				'action': 'mailinglist',
				'nonce' : localize_vars.nonce,
				'name' : $('input#name').val(),
				'email' : $('input#email').val(),
				'org' : $('input#organization').val(),
				'optin' : $('input#optin').prop('checked')
			},
			dataType: 'json',
			success : (data) => {
				console.log("status %s \n errors: ", data.rc);				
				console.log(data.errors);				
				if ( data.rc == 'OK' ) {
					// ok
					// display submission confirmation
				} else {
					// invalid
					// highlight invalid fields

				}
			}
		});
	});

	/* ---------------------------------------------------------------- */

});

/* ---------------------------------------------------------------- */
