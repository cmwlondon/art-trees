(() => {
	// console.log('classes.js autorun function inside lambda function');
}) ();

/* ------------------------------------------------ */
// Template
/* ------------------------------------------------ */

class Template{
	constructor(parameters){
		this.parameters = parameters;

		this.init();
	}
	init(){
	}
}

/*
let templateInstance = new Template({});
*/

/* ------------------------------------------------ */
// GenericScrollHandler
/* ------------------------------------------------ */

class GenericScrollHandler  {
	constructor(parameters) {
		this.parameters = parameters;

		this.trigger = parameters.trigger;
		this.subject = parameters.subject;
		this.animateOnce = parameters.animateOnce;
		this.monitorAction = parameters.monitor;
		this.updateAction = parameters.update;
		this.getDimensionsAction = parameters.getDimensions;

		this.init();
	}

	init(){
		this.animationComplete = false;
		this.state = false;

		this.itemID = this.trigger.attr('id');

		this.getDimensions();

		// set animation state based on current scroll offset
		this.monitor($(window).scrollTop());

		// update dimensions on window resize
		this.resizer = $(window).on('resize.getDimensions',() => { this.getDimensions(); });

		// monitor scrolling and update animation
		this.scroller = $(window).scroll(() => { this.monitor($(window).scrollTop()); });
	}

	getDimensions(){
		if( !this.animateOnce || !this.animationComplete ) {

			this.wh = $(window).height();

			this.getDimensionsAction(this);

			// this.top = this.trigger.position().top;
			// this.height = this.trigger.height();
			// this.bottom = this.top + this.height;
		}
	}

	monitor(scrolltop){
		if( !this.animateOnce || !this.animationComplete ) {
			this.monitorAction(this,scrolltop);			
		}
	}

	update(percent){
		this.updateAction(this,percent);
	}

}

/*
// index people
let pNodes = $('.peopleSlickSlider .item');
let pSet = [];

pNodes.each(function(i){
	pSet.push( new GenericScrollHandler({
		"trigger" : $('.peopleSlickSlider'),
		"subject" : $(this),
		"animateOnce" : false, // animation only runs once
		"getDimenions" : (instance) => {
			
			// get top offets of .item
			// let t1 = $('.peopleSlickSlider').position().top;
			// let t2 = instance.trigger.position().top;
			// instance.top = t1 + t2;

			instance.top = instance.trigger.position().top;
			instance.height = instance.trigger.height();
		},
		"monitor" : (instance,scrolltop) => {

			let percent = 0;

			// ------------------------------------------------
			// single shot animation - add class to each element
			// ------------------------------------------------

			// ------------------------------------------------
			// scrub animation based on scroll position
			// ------------------------------------------------

			// instance.top => top of trigger hits top of viewport
			// instance.top - instance.wh => top of trigger hits bottom of viewport
			// instance.bottom => bottom of trigger hits top of viewport
			// instance.bottom - instance.wh => bottom of trigger hits bottom of viewport

			let start = instance.top - (instance.height * 0.75);
			let end = start + (instance.height * 0.75);

			if ( scrolltop > start && scrolltop < end ) {
				percent =  (scrolltop - start ) / ( end - start );
			}

			if ( scrolltop > end ){
				percent = 1.0;
			}

			$('#dbTop').text(instance.top);
			$('#dbBottom').text(instance.bottom);
			$('#dbHeight').text(instance.height);
			$('#dbScrolltop').text(scrolltop);
			$('#dbStart').text(start);
			$('#dbEnd').text(end);
			$('#dbPercent').text(percent);

			instance.update(percent);
		},
		"update" : (instance,percent) => {

			// ------------------------------------------------
			// scrub animation based on scroll position
			// ------------------------------------------------

			if ( percent > 0.0 && percent < 1.0) {
					instance.trigger.addClass('animating');
			} else {
					instance.trigger.removeClass('animating');
			}

			if ( percent === 1.0) {
				instance.trigger.removeClass('animating');

				// animation only runs once
				if( instance.animateOnce) {
					instance.trigger.addClass('animationComplete');
					instance.animationComplete = true;
				}
			}

			window.requestAnimationFrame(() => {
				instance.subject.css({"opacity" : percent, "top" : Math.floor( (1.0 - percent) * 500 ) + "px"});	
			});

		}
	}));
});
*/


/* ------------------------------------------------ */
// ImageLoader
/* ------------------------------------------------ */

class ImageLoader{
	constructor(parameters) {
		this.parameters = parameters;

		this.class = parameters.class;
		this.parentID = parameters.parentID;
		this.source = parameters.source;
		this.alt = parameters.alt;
		this.loads = parameters.loads;
		this.fails = parameters.fails;

		this.init();
	}

	init(){
		var that = this;

		this.noloadTimeout = window.setTimeout( () => {
			// console.log("following item image failed to load: %s", this.parentID);
			that.fails();
			$('#post-' + this.parentID).addClass('imageFail');
		}, 250);

		this.newImg = $('<img></img>')
		.addClass(this.class)
		.attr({
			"src" : this.source,
			"alt" : this.alt,
			"data-parent" : this.parentID
		})
		.on('load', function(e){
			clearTimeout(that.noloadTimeout);
			// let iparent = $(this).attr('data-parent');
			// $('#post-' + iparent).addClass('ready')
			that.loads();
			// console.log("image %s loaded %s", that.parentID, iparent);
		});
		/*
		arrow function changes value of $(this)
		'this' returns ImageLoader instance
		.on('load',() => {
			clearTimeout(this.noloadTimeout);
			let iparent = $(this).attr('data-parent');
			$('#post-' + iparent).addClass('ready')
			console.log("image %s loaded %s", this.parentID, iparent);
		});
		*/
	}

	serverImage() {
		var that = this;

		return this.newImg
	}
}

/*
let postImage = new ImageLoader({
	"parentID" : parameters.item.id,
	"source" : parameters.item.image,
	"alt" : parameters.item.title,
	"loads" : () => { console.log('loads'); },
	"fails" : () => { console.log('fails'); },
});
let newImage = postImage.serverImage(); // jQuery image object
*/


/* ------------------------------------------------ */
// QueueSingle
/* ------------------------------------------------ */

function QueueSingle(parameters) {
	this.parameters = parameters;

	this.actions = parameters.actions;
	this.settings = parameters.settings;

	this.workers = [];
	this.workerIndex = 0;
	this.workerCount = 0;
	this.running = false;

	this.init();
}

QueueSingle.prototype = {
	"constructor" : QueueSingle,
	"template" : function () {var that = this; },
	
	"init" : function () {
		var that = this;

		// console.log('Queue.init');
		// console.log(this.settings);
	},
	"addItem" : function(item) {
		var that = this;
		this.workers.push(item);
		this.workerCount = this.workers.length;

		return this.workerCount;
	},
	"startQueue" : function() {
		var that = this;

		this.actions.queueStart(this);

		this.startWorker();
		this.running = true;
	},
	"startWorker" : function() {
		var that = this;

		if (this.workerIndex < this.workerCount) {
			this.actions.itemStart(this,this.workers[this.workerIndex]);
		} else {
			this.queueComplete();
		}
		
	},
	"workerComplete" : function() {
		var that = this;

		this.actions.itemComplete(this,this.workers[this.workerIndex]);

		this.workerIndex++;	
		this.startWorker();
	},
	"queueComplete" : function() {
		var that = this;

		this.actions.queueComplete(this);
		this.running = false;
	}
}

/*
// setup
let postQueue = new QueueSingle({
	"settings" : {
		"delay" : 250 // quarter second delay between each person fades in
	},
	"actions" : {
		// run when queue is started
		"queueStart" : function(queue){},

		// run when an item is started
		"itemStart" : function(queue,worker){
			let workerNode = $('article#' + worker.id);

			let portraitImageFull = $('<img></img>').attr({
				"alt" : worker.id,
				"src" : worker.image
			})
			.addClass('full')
			.on('load',function(e){

				let portraitImageThumb = $('<img></img>').attr({
					"alt" : worker.id,
					"src" : worker.thumb
				})
				.addClass('thumb')
				.on('load',function(e){
					// stagger image loading
					worker.delay = window.setTimeout(function(){

						queue.workerComplete();	// signal this item has finished and a new item can be started

					}, queue.settings.delay);

				});
				workerNode.find('.frame a').append( portraitImageThumb )

			});
			workerNode.find('.frame a').append( portraitImageFull )

		},

		// run when an item finishes (image.load)
		"itemComplete" : function(queue,worker){
			let workerNode = $('article#' + worker.id);
			workerNode.removeClass('loading');	
		},

		// run when all items have finished
		"queueComplete" : function(queue){
		}
	}
});

// populate queue
posts.map(function(post){
	postQueue.addItem({
		"id" : post.id,
		"thumb" : post.thumb,
		"image" : post.image
	});
});

// start
posts.startQueue();
*/


/* ------------------------------------------------ */
// QueueItem
/* ------------------------------------------------ */

class QueueItem{
	constructor( parameters ) {
		this.parameters = parameters;

		this.properties = parameters.properties;
		this.parentQueue = parameters.parentQueue;
		this.startAction = parameters.startAction;
		this.completeAction = parameters.completeAction;

		this.init();
	}

	init(){
		this.active = false;
		this.completed = false;
	}

	start(){
		this.active = true;
		this.startAction(this.parentQueue,this);
	}

	complete(){
		this.active = false;
		this.completed = true;
		this.completeAction(this);
		this.parentQueue.workerComplete(this.properties.name);
	}
}

/*
// used within Queue
// this = Queue instance
let newItem = new QueueItem({
	"properties" : thisWorker,
	"parentQueue" : this,
	"startAction" : this.workerStartAction, 
	"completeAction" : this.workerCompleteAction
});
*/

/* ------------------------------------------------ */
// Queue
/* ------------------------------------------------ */

class Queue{
	constructor( parameters ) {
		this.itemDelay = parameters.itemDelay;
		this.workerLimit = parameters.workerLimit;

		// callbacks/actions
		// queue
		this.queueStartAction = parameters.queueStartAction; // queue
		this.queueCompleteAction = parameters.queueCompleteAction; // queue

		// worker
		this.workerStartAction = parameters.workerStartAction; // queue,worker
		this.workerCompleteAction = parameters.workerCompleteAction; // queue,worker

		this.init();
	}

	init() {
		this.items = [];
		this.itemCount = 0;
		this.activeItemsBox = [];
		this.activeItems = 0;
		this.running = false;
	}

	addItems(items) {
		let n = 0;
		while ( n < items.length) {
			this.addItem( items[n] );
			n++;
		}
	}

	addItem(item) {
		item.index = this.items.length;
		this.items.push(item);
		this.itemCount++;
	}

	removeItem(identifier) {}

	initWorker(){
		let thisWorker = this.items.pop(); // get LAST item in items array 

		// -> QueueItem
		let newItem = new QueueItem({
			"properties" : thisWorker,
			"parentQueue" : this,
			"startAction" : this.workerStartAction,
			"completeAction" : this.workerCompleteAction
		});
		this.activeItemsBox.push(newItem);
		this.workerStart(newItem);

		this.activeItems++;
	}

	queueStart() {
		this.queueStartAction(this);

		if ( this.items.length > 0 ) {
			// start initial wave of workers, up to this.workerLimit
			let n = 0;
			while( this.activeItems < this.workerLimit && n < this.itemCount) {
				this.initWorker();
				n++;
			}
			this.running = true;
		}
	}

	queueStop() {}

	queuePause() {}

	queueComplete() {
		this.queueCompleteAction(this);
	}

	workerStart(worker) {
		worker.start();
	}

	workerComplete(name) {
		if( this.running ) {
			let n = 0;
			while( n < this.activeItemsBox.length ) {
				let x = this.activeItemsBox[n];

				if (x.properties.name === name)
					break;
				n++;
			}

			this.activeItemsBox.splice(n, 1);

			// start another worker if there are workers left
			if ( this.items.length > 0 ) {
				this.initWorker();
			} 

			// all queued items completed
			if ( this.items.length === 0 && this.activeItemsBox.length === 0 ) {
				this.running = false;
				this.queueComplete(this);
			}

		}
	}
}

/*
let testQueue = new Queue({
	"itemDelay" : 250,
	"workerLimit" : 8,
	"queueStartAction" : function(queue){
		console.log( "- queueStartAction items: %s", queue.itemCount );
		console.log(" ");
	},
	"queueCompleteAction" : function(queue){
		console.log(" ");
		console.log("**");
		console.log("---- queueCompleteAction START");
		console.log("**");
		console.log(" ");

		let n = 0;
		while(n < queue.activeItemsBox.length) {
			let go = queue.activeItemsBox[n];

			console.log(go.properties.slider.text);
			go.properties.slider.configure({"name" : go.properties.name});
			go.properties.slider.initialised({"index" : go.properties.index});

			n++;
		}

		console.log(" ");
		console.log("**");
		console.log("---- queueCompleteAction END");
		console.log("**");
	},
	"workerStartAction" : function(queue,worker){
		console.log("-- worker start name: %s", worker.properties.name);
		window.setTimeout(function(){
			worker.complete();
		},queue.itemDelay)
	},
	"workerCompleteAction" : function(worker){
		console.log("--- worker complete name: %s", worker.properties.name);
		console.log(" ");
	}
});

let n = 0;
while( n < 6) {
	testQueue.addItem({
		"name" : "worker" + n,
		"slider" : {
			"text" : 'alpha beta',
			"configure" : function(parameters){
				console.log("Alpha %s", parameters.name);
			},
			"initialised" : function(parameters){
				console.log("Beta %s", parameters.index);
			}
		}
	})
	n++;
} 

testQueue.queueStart();
*/
