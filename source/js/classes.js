'use strict';

/*
Classes:
NewsLoader
DocumentFilter
FAQs
*/

/* ------------------------------------------------ */
// NewsLoader
/* ------------------------------------------------ */

let NewsLoader = function(parameters) {
	this.parameters = parameters;

	this.newsModule = parameters.newsModule;

	this.init();
}
NewsLoader.prototype = {
	"constructor" : NewsLoader,
	"template" : function () {var that = this; },
	
	"init" : function () {
		var that = this;

		this.itemsContainer = this.newsModule.find('.newsItems');
		this.wt = this.itemsContainer.find('.wt');
		this.newsItemTarget = this.newsModule.find('.newsItemTarget');
		this.newsPaging = this.newsModule.find('.paging');
		this.newsItemTemplate = $('#itemTemplate');

		this.currentFilter = this.itemsContainer.attr('data-filter');
		this.featured = this.itemsContainer.attr('data-featured');
		this.currentPage = this.itemsContainer.attr('data-page');
		
		// default 4
		// 
		this.itemsPerPage = this.itemsContainer.attr('data-items-per-page');

		this.filterNodes = this.newsModule.find('.filter li');

		// add behaviour ot filter controls
		this.filterButtons = this.newsModule.find('.filter button');
		this.filterButtons.on('click', function(e) {
			e.preventDefault();
			that.doFilter($(this).attr('data-action'));
		});

		// add behaviour to paging controls
		this.pagingButtons = this.newsPaging.find('button');
		this.pagingButtons.on('click', function(e) {
			e.preventDefault();
			that.doPaging($(this).attr('data-action'));
		});
	},

	"doPaging" : function(page) {
		var that = this;

		// don't bother updating
		if ( page == this.currentPage) {
			return false;
		}

		// intercept transition end event on children
		this.itemsContainer.children().each(function(i){
			$(this).get(0).addEventListener('transitionend', (e) => {
				e.stopPropagation();
			});
		})

		let transitionEnd1 = function(e) {

			that.itemsContainer.find('.item').remove();

			// get news items
			$.ajax({
				url: localize_vars.ajaxurl,
				type: "GET",
				data: {
					'action': 'newspaging',
					'nonce' : localize_vars.nonce,
					'filter' : that.currentFilter,
					'featured' : that.featured,
					"pagesize" : that.itemsPerPage,
					"page" : page
				},
				dataType: 'json',
				success : (data) => {
					that.processPagingResponse(data);
				}
			});

		}

		// add transition end event on this.itemsContainer
		this.itemsContainer.get(0).addEventListener('transitionend', transitionEnd1, {"once" : true});

		// do fade out on this.itemsContainer
		this.wt.addClass('fadeOut');
		this.itemsContainer.addClass('waiting');
	},

	"processPagingResponse" : function(data) {
		let newPosts = data.posts;

		newPosts.map((post,i) => {
			this.newsItemTarget.append( this.buildItem({
				"post" : post
			}));
		}, this);
		
		if( data.pages > 1 ) {
			this.updatePaging({
				"pages" : data.pages,
				"page" : data.page
			});
		}

		this.currentPage = data.page;
		this.itemsContainer.attr({"data-page" : this.currentPage});

		this.wt.removeClass('fadeOut');
		this.itemsContainer.removeClass('waiting');

		/*
		// update address bar URL 'gr' parameter, allows sharing of link
		const url = new URL(window.location);
		url.searchParams.set('pn', this.currentPage);
		window.history.pushState({}, '', url);					
		*/
	},

	"doFilter" : function (filter) {
		var that = this;

		// intercept transition end event on children
		this.itemsContainer.children().each(function(i){
			$(this).get(0).addEventListener('transitionend', (e) => {
				e.stopPropagation();
			});
		})

		let transitionEnd1 = function(e) {

			that.itemsContainer.find('.item').remove();
			// that.pagingButtons.remove();
			that.newsPaging.empty();

			// get news items
			$.ajax({
				url: localize_vars.ajaxurl,
				type: "GET",
				data: {
					'action': 'filternews',
					'nonce' : localize_vars.nonce,
					'filter' : filter,
					'featured' : that.featured,
					"pagesize" : that.itemsPerPage,
					"page" : 1 // reset page when filter changes
				},
				dataType: 'json',
				success : (data) => {
					that.processFilterResponse(data);
				}
			});

		}

		// add transition end event on this.itemsContainer
		this.itemsContainer.get(0).addEventListener('transitionend', transitionEnd1, {"once" : true});

		// do fade out on this.itemsContainer
		this.itemsContainer.find('.wt').addClass('fadeOut');
		this.itemsContainer.addClass('waiting');

	},

	"processFilterResponse" : function (data) {
		var that = this;

		// iterate over posts
		let newPosts = data.posts;

		newPosts.map((post,i) => {
			this.newsItemTarget.append( this.buildItem({
				"post" : post
			}));
		}, this);
		
		if( data.pages > 1 ) {
			this.buildPaging({
				"pages" : data.pages,
				"page" : data.page
			});
		}

		this.currentPage = 1;
		this.itemsContainer.attr({"data-page" : this.currentPage});
		this.currentFilter = data.filter
		this.itemsContainer.attr({ "data-filter" : this.currentFilter });

		this.updateFilter( data.filter );

		this.wt.removeClass('fadeOut');
		this.itemsContainer.removeClass('waiting');

		/*
		console.log( this.currentFilter );
		const url = new URL(window.location);
		url.searchParams.set('sub', this.currentFilter);
		window.history.pushState({}, '', url);					
		*/
	},

	"buildItem" : function (parameters) {
		var that = this;

		let newItem = this.newsItemTemplate.clone();
		newItem.attr({"id" : "post" + parameters.post.id});

		// attachments
		// has_link
		// external_link
		// link_url
		// add attachment icon to news item
		let iconClass = ( parameters.post.attachments !== 'none') ? 'attachment' : '';
		// add external link icon to news item OVERRIDES attachemnt icon
		if ( parameters.post.has_link == 'yes' && parameters.post.external_link == 'yes' ) {
			iconClass = 'externalLink';
		}
		newItem.find('.info').addClass(iconClass);

		// update fields in template 
		let linkOptions = null;
		if ( parameters.post.has_link === 'no' ) {
			// link to post
			linkOptions = {
				"href" : parameters.post.permalink 
			};
		} else {
			// link to specified link
			// check for external link
			linkOptions = {
				"href" : parameters.post.link_url
			};
			if ( parameters.post.external_link === 'yes' ) {
				linkOptions.target = "_blank";
			}
		}
		newItem.find('a.overlay').attr(linkOptions)

		newItem.find('p.category').text(parameters.post.category);
		newItem.find('h3').text(parameters.post.title);
		newItem.find('p.extra').text(parameters.post.source + ' - ' + parameters.post.date);

		// check image aspect ratio, handle differently for images with an aspect ratio larger than 1.0
		if ( parseFloat(parameters.post.ta) > 1.0 ) {
			newItem.find('figure').addClass('scaleByHeight');
		}
		
		newItem.find('img').attr({
			"src" : parameters.post.thumbnail,
			"alt" : parameters.post.title
		})

		return newItem;
	},

	"buildPaging" : function (parameters) {
		var that = this;

		// parameters.pages
		// parameters.page

		// this.newsPaging

		let buttonZero = $('<button></button>')
		.addClass('btn')
		.addClass('transparent')
		// .on('click', (e) => { console.log( $(e.target).attr('data-action') ); });

		let buttonFirst = buttonZero.clone()
		.addClass('first current')
		.attr({"data-action" : "1"})
		.text('FIRST')
		.on('click', function(e) {
			e.preventDefault();
			that.doPaging($(this).attr('data-action'));
		});
		let buttonLast = buttonZero.clone()
		.addClass('last')
		.attr({"data-action" : parameters.pages})
		.text('LAST')
		.on('click', function(e) {
			e.preventDefault();
			that.doPaging($(this).attr('data-action'));
		});


		// iterate over pages
		// this.newsPaging

		this.newsPaging.append(buttonFirst);

		let n = 0;
		while (n < parameters.pages) {
			let buttonNumber = buttonZero.clone();

			buttonNumber
			.attr({
				"data-action" : n + 1
			})
			.text(n + 1)
			.on('click', function(e) {
				e.preventDefault();
				that.doPaging($(this).attr('data-action'));
			});

			if ( (n+1) == parameters.page ) {
				buttonNumber.addClass('current');
			}

			this.newsPaging.append(buttonNumber);	

			n++;
		}
		this.newsPaging.append(buttonLast);
	},

	"updatePaging" : function (parameters) {
		// parameters.pages, parameters.page

		// $('.paging button').find('[data-action="2"]').addClass('current');
		// $('.paging button').not('[data-action="2"]').removeClass('current');
		this.pagingButtons = this.newsPaging.find('button');

		this.pagingButtons.each(function(i) {
			let button = $(this);
			let buttonPage = button.attr('data-action')
			if ( buttonPage == parameters.page ) {
				button.addClass('current');
			} else {
				button.removeClass('current');
			}
		});
	},

	"buildItemQueue" : function (items, target, mode) {
		var that = this;

	},

	"updateFilter" : function (filter) {
		var that = this;

		this.filterNodes.each(function(i) {
			let button = $(this).find('button');
			let buttonFilter = button.attr('data-action');

			if ( buttonFilter == filter ) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}
		});

	}
}
/*
let newsPageLoader = new NewsLoader({
	"filter" : ",
	"loadmore" : ",
	
});
*/

// ---------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------


/* ------------------------------------------------ */
// DocumentFilter
/* ------------------------------------------------ */

class DocumentFilter{
	constructor( parameters ) {
		// this.parameters = parameters;

		this.dlm = parameters.documentListModule;

		this.init();
	}

	init() {
		var that = this;

		this.documentContainer = this.dlm.find('.documentContainer');
		this.wt = this.documentContainer.find('.wt');
		this.docBox = this.documentContainer.find('.docBox');
		this.filterControl = this.dlm.find('.docfilter');
		this.filterLevel = this.dlm.attr('data-filter-level');

		// initial state of filter parameters set by server based on URL
		this.ipp = this.documentContainer.attr('data-items-per-page'); // constant

		// change on filter
		this.main = this.documentContainer.attr('data-main');
		this.sub = this.documentContainer.attr('data-sub');
		this.currentFilter = this.main;
		if ( this.sub !== ''  ) {
			this.currentFilter = this.currentFilter + "," + this.sub;
		}
		this.pages = this.documentContainer.attr('data-pages');

		// global masterCategories
		// {
		// "slug" : "name",
		// ...
		// }

		// change npge
		this.page = this.documentContainer.attr('data-page');

		// console.log("this.main: '%s' this.sub: '%s' this.ipp:[%s] this.pages:[%s] this.page:[%s]", this.main, this.sub, this.ipp, this.pages, this.page);

		// bind main/sub category filters
		this.filterButtons = this.dlm.find('.docfilter .accordion a')
		this.filterButtons.on('click',(e) => {
			e.preventDefault();
			this.doFilter( $(e.target) );
		});

		// bind paging
		this.pagingButtons = this.dlm.find('.paging a')
		this.pagingButtons.on('click',(e) => {
			e.preventDefault();
			this.doPaging( $(e.target) );
		});
	}

	doFilter( filterItem ) {
		let that = this;

		let action = filterItem.attr('data-action');

		// don't do anything if the user selects the current filter
		if ( action !== this.currentFilter) {
			let filters = action.split(',');

			// intercept transition end event on children
			this.documentContainer.children().each(function(i){
				$(this).get(0).addEventListener('transitionend', (e) => {
					e.stopPropagation();
				});
			})

			let transitionEnd1 = function(e) {

				// remove items in document list
				that.docBox.empty();

				// remove paging buttons
				that.pagingButtons.remove();

				// filter action, resets pages based on returned docs and ipp, paging to page 1
				// reset main, sub, pages, page
				// AJAX paframeters:
				// main
				// sub
				// ipp = this.ipp
				// page = 1

				// console.log( "classes.js 484 section: %s filter level: %s action: %s", section, that.filterLevel, action );
				$.ajax({
					url: localize_vars.ajaxurl,
					type: "GET",
					data: {
						'section' : section,
						'filter-level' : that.filterLevel,
						'action': 'docfilter',
						'nonce' : localize_vars.nonce,
						'filter' : action,
						"ipp" : that.ipp
					},
					dataType: 'json',
					success : (data) => {
						that.handleFilterResponse(data);
					}
				});

			}

			// add transition end event on this.itemsContainer
			this.documentContainer.get(0).addEventListener('transitionend', transitionEnd1, {"once" : true});

			// do fade out on this.itemsContainer
			this.wt.addClass('fadeOut');
			this.documentContainer.addClass('waiting');

		}
	}

	doPaging( pagingItem ) {
		let that = this;

		let action = pagingItem.attr('data-action');

		// intercept transition end event on children
		this.documentContainer.children().each(function(i){
			$(this).get(0).addEventListener('transitionend', (e) => {
				e.stopPropagation();
			});
		})

		let transitionEnd1 = function(e) {

			// remove items in document list
			that.docBox.empty();

			// jump to requested page with same main,sub,ipp,pages
			// AJAX paframeters:
			// main = this.main
			// sub = this.sub
			// ipp = this.ipp
			// page = action

			let filter = [];
			filter.push( that.documentContainer.attr('data-main') );
			if ( that.documentContainer.attr('data-sub') !== '' ) {
				filter.push( that.documentContainer.attr('data-sub') );
			}

			console.log("classes.js 544 filter level: %s", that.filterLevel);
			$.ajax({
				url: localize_vars.ajaxurl,
				type: "GET",
				data: {
					'section' : section,
					'filter-level' : that.filterLevel,
					'action': 'docpaging',
					'nonce' : localize_vars.nonce,
					'filter' : filter.join(','),
					"ipp" : that.ipp,
					"page" : action
				},
				dataType: 'json',
				success : (data) => {
					that.handlePagingResponse(data);
				}
			});

		}

		// add transition end event on this.itemsContainer
		this.documentContainer.get(0).addEventListener('transitionend', transitionEnd1, {"once" : true});

		// do fade out on this.itemsContainer
		this.wt.addClass('fadeOut');
		this.documentContainer.addClass('waiting');
	}

	handleFilterResponse( data ) {

		if ( data.status == 'OK' ) {
			this.buildDocList( data.documents, (data.sub !== '') );
			this.updateFilter( data.filter );

			if ( data.pagecount > 1 ) {
				this.buildPaging( data.section, data.filter, data.pagecount, data.page );
			}

			this.main = data.main;
			this.sub = data.sub;
			this.pages = data.pagecount;
			this.page = 1;
			this.currentFilter = this.main;
			if ( this.sub !== ''  ) {
				this.currentFilter = this.currentFilter + "," + this.sub;
			}

			this.documentContainer.attr({ "data-main" : this.main });
			this.documentContainer.attr({ "data-sub" : this.sub });
			this.documentContainer.attr({ "data-pages" : this.pages });
			this.documentContainer.attr({ "data-page" : this.page });

			this.wt.removeClass('fadeOut');
			this.documentContainer.removeClass('waiting');

			// possibly update page title H1,
			// addres bar
			// and breadcrumb trail .breadcrumb > li > span

			// masterCategories[this.main]
			// masterCategories[this.sub]
			this.dlm.find('h1').text( masterCategories[this.main] );
			$('.breadcrumb li > span').text( masterCategories[this.main] );

			let filterCaption = ( this.sub !== '' ) ? masterCategories[this.main] + ' > ' + masterCategories[this.sub] : masterCategories[this.main];

			$('p.currentFilter').text( "Current filter: " + filterCaption );

			const url = new URL( siteURL + '/' + data.section + '/' + this.main + '/' );
			url.searchParams.set('sub', this.sub );
			window.history.pushState({}, '', url);					
		} else {

		}
	}

	handlePagingResponse( data ) {

		if ( data.status == 'OK' ) {
			console.log("classes.js 625 data.filterlevel: %s data.sub: %s", data.filterlevel, data.sub);
			this.buildDocList( data.documents, (data.sub !== '') );
			this.updatePaging( data.pagecount, data.page );

			// filter main,sub and pagecount unchanged
			this.page = data.page;
			this.documentContainer.attr({ "data-page" : this.page });

			this.wt.removeClass('fadeOut');
			this.documentContainer.removeClass('waiting');

			const url = new URL( siteURL + '/' + section + '/' + this.main + '/' );
			url.searchParams.set('sub', this.sub );
			url.searchParams.set('pn', this.page);
			window.history.pushState({}, '', url);					
		} else {

		}
	}

	updateFilter( filter ) {

		let _filter = filter.split(',');
		let newMain = _filter[0];
		let newFilter = newMain;
		if ( _filter.length > 1  ) {
			newFilter = newFilter + "," + _filter[1];
		}

		this.filterControl.find('li.accordion').each( (index, element) => {
			// console.log( $(element).attr('data-main'), newMain );
			if ($(element).attr('data-main') == newMain ) {
				$(element).addClass('current');
			} else {
				$(element).removeClass('current');
			}
		} );

		this.filterControl.find('li a').each(( index, element ) => {
			let linkAction = $(element).attr('data-action');
			if ( linkAction === newMain || linkAction === newFilter ) {
				$(element).parents('li').eq(0).addClass('current');
			} else {
				$(element).parents('li').eq(0).removeClass('current');
			}
		});
	}

	updatePaging( pages, page ) {
		this.pagingButtons.each(function(i) {
			let button = $(this);
			let buttonPage = button.attr('data-action')
			if ( buttonPage == page ) {
				button.addClass('current');
			} else {
				button.removeClass('current');
			}
		});

		this.page = page;
	}

	buildPaging( section, filter, pages, page ) {
		let _filter = filter.split(',');
		let main = _filter[0];
		let sub = (_filter.length > 1) ? _filter[1] : '';
		let _href = "/" + section + "/" + main + "/?sub=" + sub + "&pn=";

		let docPaging = this.dlm.find('.paging');

		let buttonZero = $('<a></a>')
		.addClass('btn2')
		.addClass('transparent');

		/*
		let buttonFirst = buttonZero.clone()
		.addClass('first current')
		.attr({
			"href" : _href + "1",
			"data-action" : "1"
		})
		.text('FIRST')
		.on('click', (e) => {
			e.preventDefault();
			this.doPaging( $(e.target) );
		});

		let buttonLast = buttonZero.clone()
		.addClass('last')
		.attr({
			"href" : _href + pages,
			"data-action" : pages
		})
		.text('LAST')
		.on('click', (e) => {
			e.preventDefault();
			this.doPaging( $(e.target) );
		});
		*/

		// docPaging.append(buttonFirst);
		let n = 0;
		while (n < pages) {
			let buttonNumber = buttonZero.clone();

			buttonNumber
			.addClass('page')
			.attr({
				"href" : _href + ( n + 1 ), 
				"data-action" : n + 1
			})
			.text(n + 1)
			.on('click', (e) => {
				e.preventDefault();
				this.doPaging( $(e.target) );
			});

			if ( (n+1) == page ) {
				buttonNumber.addClass('current');
			}

			docPaging.append(buttonNumber);	

			n++;
		}
		// docPaging.append(buttonLast);

		this.pagingButtons = this.dlm.find('.paging a');
	}

	buildDocList( documents, isSubPresent ) {
		let target = this.docBox;
		let ml = $('<ul></ul>');

		if ( isSubPresent ) {

			let axs = documents.map((document) => {
				ml.append( this.buildDocItem( document ) );
			}, this);
			target.append( ml);
		} else {
			// this.buildSubItem( sub );
			for (const [key, value] of Object.entries(documents)) {
				ml.append( this.buildSubItem( value ) );
			}
			target.append( ml);
		}
	}

	buildSubItem( subcategory ) {
		// .documentListTemplates li.sub
		let template = $('.documentListTemplates li.sub').clone();
		// set properties/attributes
		template.find('h2').text( subcategory.name );

		// -> this.buildDocItem( document )
		subcategory.docs.map((document) => {
			template.find('ul').eq(0).append( this.buildDocItem( document ) );
		}, this);
		return template;
	}

	buildDocItem( document ) {
		// .documentListTemplates li.doc
		let template = $('.documentListTemplates li.doc').clone();
		// set properties/attributes

		/*
		<div class="item pdf multiLanguage">
			<h3>TREES 2.0 Webinar Presentation (Sept 2021)</h3>
			<ul>
				<li class="translation"><a href="http://arttree.local/wp-content/uploads/2021/12/TREES-2.0-Public-Consultation-Final.pdf" title="English (2.0Mb pdf)" target="_blank">English (2.0Mb pdf)</a></li>
				<li class="translation"><a href="http://arttree.local/wp-content/uploads/2021/12/TREES-2.0-Webinar-PT-Final.pdf" title="Portuguese (1.9Mb pdf)" target="_blank">Portuguese (1.9Mb pdf)</a></li>
				<li class="translation"><a href="http://arttree.local/wp-content/uploads/2021/12/TREES-2.0-Public-Consultation-Spanish-Final.pdf" title="Spanish (1.9Mb pdf)" target="_blank">Spanish (1.9Mb pdf)</a></li>
			</ul>	
			<a href="https://www.youtube.com/" target="_blank" title="A webinar link caption">A webinar link caption</a>
		</div>
		*/		

		// add document title
		template.find('h3').text( document.title );

		// build translation links
		// -> this.buildDocTranslastionItem( translation )
		if ( document.translations.length > 1 ) {
			// use PDF icon for multilanguage documents
			// to do: maybe use generic icon instead
			template.find('.item').addClass('pdf');
			document.translations.map((translation) => {
				template.find('ul').eq(0).append( this.buildDocTranslastionItem( translation, true ) );
			}, this);
		} else {
			let translation = document.translations[0];
			template.find('.item').addClass(translation.type);
			template.find('ul').eq(0).append( this.buildDocTranslastionItem( translation, false ) );
		}

		// build optional webinar link
		if ( document.webinarcaption && document.webinarlink ) {
			let webinarLink = $('<a></a>')
			.attr({
				"href" : document.webinarlink,
				"target" : "_blank",
				"title" : document.webinarcaption
			})
			.html(document.webinarcaption);
			template.find('.item').append( webinarLink );
		}

		return template;
	}

	buildDocTranslastionItem( translation, isMultipole ) {
		// .documentListTemplates li.translation
		let template = $('.documentListTemplates li.translation').clone();
		// set properties/attributes
		// language, url,size,type

		// let caption = ( isMultipole ) ? translation.language : translation.language + " (" + translation.size + " " + translation.type + ")";
		// let caption = translation.language + " (" + translation.size + " " + translation.type + ")";
		let caption = translation.language;

		template.find('a').attr({
			"href" : translation.url,
			"title"  : caption
		})
		.text( caption );

		return template;
	}
}


/*
	if ( $('.document_list').length > 0 ) {
		let docsFilter = new DocumentFilter({
			"documentFilterModule" : $('.document_list')
		});
	}
*/


/* ------------------------------------------------ */
// FAQs
/* ------------------------------------------------ */

/*
anchors:
art
indigenous-peoples
hlfd
safeguards
removals
nesting
*/

class FAQs{
	constructor( parameters ) {
		// this.parameters = parameters;

		this.faqModule = parameters.faqmodule;
		this.filter = parameters.filter;
		this.groups = parameters.groups;

		this.init();
	}

	init() {
		var that = this;

		this.currentGroup = 0; // inital state of menu, first item highlighted
		this.triggers = this.filter.find('a');
		this.menuaItems = this.filter.find('li');
		this.topIndex = [];
		this.menuOverride = false;

		let faqurl = window.location.href;
		
		let anchorRE = /.*\#([a-z\-]+)$/g; // #art
		// let anchorRE = /.*\/\?gr\=([a-z\-]+)$/g; // ?gr=art	
		let result = anchorRE.exec(faqurl);		
		// result[1] = hash

		// location.hash = '';

		/*
		https://learn.jquery.com/events/introduction-to-custom-events/

		$('section.faqs').on( "myCustomEvent", {
		    foo: "bar"
		}, function( event, arg1, arg2 ) {
		    console.log( event.data.foo ); // "bar"
		    console.log( arg1 );           // "bim"
		    console.log( arg2 );           // "baz"
		});

		$('section.faqs').trigger( "myCustomEvent", [ "bim", "baz" ] );
		*/
		this.initialIndexed = false;

		// run rest of setup AFTER indexing
		this.faqModule.on('indexed',(e) => {

			if (!this.initialIndexed){
				// add click behaviour to menu
				this.setupMenu();

				// add accordion behaviour to faq groups
				this.setupAccordion();

				// reindex on wondow.resize
				$(window).on('resize',() => { this.index(); });

				// add scroll behaviour
				$(window).scroll(() => { this.monitorScroll( $(window).scrollTop() ); });

				/*
				// emulate hashlinks
				// console.log("window.location.hash: %s", window.location.hash);
				this.loadDelay = window.setTimeout( () => {
					if (result !== null && anchors.includes(result[1]) ){
						// window.scrollTo(0, 0 );
						this.jumpto(result[1]);
					}
				}, 500);
				*/

				this.initialIndexed = true;
			}

		});

		// index groups (collect top position and heights of each FAQ group)
		this.index();
	}

	index(){
		// index positions of top of each section
		// .faqgroup data-faqgroup = faq group slug

		// $('div.mark').remove();
		let topOffset = 0;
		let faqClass = $('main').attr('class').trim();

		let faqListTop = parseInt( $('.faqlist').position().top, 10);
		// $('body').append( $('<div></div>').addClass('mark').css({ "top" : faqListTop + "px", "background-color" : "#000", }).html('faqListTop: ' + faqListTop  ) );

		/*
		switch( faqClass ) {
			case 'mainAll' : {
				//  main + banner + breadcrumb 'mainAll' 100 + 600 + 48
				topOffset = ( $(window).width() > 991 ) ? 748 : 0; 
			} break;

			case 'mainBreadcrumb' : {
				// main + breadcrumb 'mainBreadcrumb ' 100 + 48
				topOffset = ( $(window).width() > 991 ) ? 148 : 0; 
			} break;

			default : {
				// main '' 100
				topOffset = ( $(window).width() > 991 ) ? 100 : 0;
			}
		}
		// topOffset = ( $(window).width() > 991 ) ? 164 : 0;
		*/

		topOffset = faqListTop - 178;

		// reset
		this.topIndex = [];

		// add offset to clear menu bar in desktop mode only

		// populate using new values
		this.groups.each( (index, item) => {

			let group = $(item).attr('data-faqgroup');
			let top0 = parseInt( $(item).position().top, 10);
			let top = top0 + topOffset;
			let height = parseInt( $(item).height(), 10);
			let bottom = top + height + topOffset;

			this.topIndex.push({
				"faq" : group,
				"top" : top,
				"height" : height,
				"bottom" : bottom
			});

			// $('body').append( $('<div></div>').addClass('mark').css({ "top" : (top0 + topOffset) + "px", "background-color" : "#000", }).html('top0 ' + index + ' - ' + top0  ) );
			/*
			// $('body').append( $('<div></div>').addClass('mark').css({ "top" : top + "px", "background-color" : "#f00", }).html(index) );
			$('body').append( $('<div></div>').addClass('mark').css({ "top" : top + "px", "background-color" : "#0f0", }) );
			$('body').append( $('<div></div>').addClass('mark').css({ "top" : bottom + "px", "background-color" : "#f00", }) );
			*/

		});
		this.faqModule.trigger( "indexed",[] );
	}

	setupMenu(){

		// scroll viewport to selected FAQ section
		this.triggers.on('click',(e) => {
			this.filter.removeClass('open');
			/*
			e.preventDefault();
			let trigger = $(e.target); 
			// let filter = trigger.parents('.faqfilter');
			let menuParent = trigger.parents('li');
			let target = trigger.attr('data-target'); // faq group slug
			let newIndex = this.menuaItems.index(menuParent);

			// update menu state in this.monitorScroll()
			// this.menuaItems.removeClass('active');
			// menuParent.addClass('active');

			// close mobile menu

			this.menuOverride = true;
			this.targetIndex = newIndex;

			this.currentGroup = newIndex;
			this.scrollTo( newIndex );
			*/
		});
	}

	setupAccordion(){
		// height of FAQ group changes when an accordion opens/closes
		// need to rerun index()
		$('.accordion').each((index,element) => {
			let thisAccordion = $(element);
			let trigger = thisAccordion.find('.title');
			trigger.on('click', (el2) => {
				let parent = $(el2.target).parents('.accordion');

				let transitionEnd1 = (e) => { this.index(); }

				parent.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", transitionEnd1 );
			});
		});
	}

	jumpto(group){
		// find index of required group
		let newIndex = -1;
		let n = 0;
		while( n < this.topIndex.length ) {
			let currGroup = this.topIndex[n];
			if ( currGroup.faq === group ){
				newIndex = n;
				break;
			}
			n++;
		}
		if ( newIndex > -1) {
			this.menuaItems.removeClass('active');
			this.menuaItems.eq(newIndex).addClass('active');

			this.scrollTo( newIndex );
		}
	}

	scrollTo( index ){
		this.scrollTarget = this.topIndex[ index ].top;
		window.scrollTo(0, this.scrollTarget );
	}

	monitorScroll( scrollPosition ){
		
		if ( this.menuOverride && this.scrollTarget === scrollPosition ) {
			this.currentGroup = this.targetIndex;
			this.menuaItems.removeClass('active');
			this.menuaItems.eq(this.currentGroup).addClass('active');
			this.menuOverride = false;
		} else {
			// update left hand menu based on scroll position (visible FAQ group)
			let n = 0;
			while( n < this.topIndex.length ) {
				let currGroup = this.topIndex[n];
		
				if ( currGroup.top <= scrollPosition && (currGroup.bottom + $(window).height()) > scrollPosition) {
					this.menuaItems.removeClass('active');
					this.menuaItems.eq(n).addClass('active');
					this.currentGroup = n;
				}
				n++;
			}
		}

		// update URL when scrolling stops
		this.nows = scrollPosition;
		window.clearTimeout(this.scrollTimer);

		this.scrollTimer = window.setTimeout( () => {
			if ( $(window).scrollTop() === this.nows ) {
				window.clearTimeout(this.scrollTimer);

				let currGroup = this.topIndex[this.currentGroup];
				// let url = new URL( siteURL + '/faqs/?gr=' + currGroup.faq ); // ?gr=art
				let url = new URL( siteURL + '/faqs/#' + currGroup.faq ); // #art
				window.history.pushState({}, '', url);					
			}
		}, 10);
	}
};

/*
let faqModule = $('section.faqs');
let thisFAQList = new FAQs({
	"filter" : faqModule.find('.faqfilter'),
	"groups" : faqModule.find('.faqlist .faqGroup')
});
*/

/* ------------------------------------------------ */
// GenericScrollHandler
/* ------------------------------------------------ */

class GenericScrollHandler  {
	constructor(parameters) {
		this.parameters = parameters;

		this.trigger = parameters.trigger;
		this.subject = parameters.subject;
		this.animateOnce = parameters.animateOnce;
		this.monitorAction = parameters.monitor;
		this.updateAction = parameters.update;
		this.getDimensionsAction = parameters.getDimensions;

		this.init();
	}

	init(){
		this.animationComplete = false;
		this.state = false;

		this.itemID = this.trigger.attr('id');

		this.getDimensions();

		// set animation state based on current scroll offset
		this.monitor($(window).scrollTop());

		// update dimensions on window resize
		this.resizer = $(window).on('resize.getDimensions',() => { this.getDimensions(); });

		// monitor scrolling and update animation
		this.scroller = $(window).scroll(() => { this.monitor($(window).scrollTop()); });
	}

	getDimensions(){
		if( !this.animateOnce || !this.animationComplete ) {

			this.wh = $(window).height();

			this.getDimensionsAction(this);

			// this.top = this.trigger.position().top;
			// this.height = this.trigger.height();
			// this.bottom = this.top + this.height;
		}
	}

	monitor(scrolltop){
		if( !this.animateOnce || !this.animationComplete ) {
			this.monitorAction(this,scrolltop);			
		}
	}

	update(percent){
		this.updateAction(this,percent);
	}

}

/*
// index people
let pNodes = $('.peopleSlickSlider .item');
let pSet = [];

pNodes.each(function(i){
	pSet.push( new GenericScrollHandler({
		"trigger" : $('.peopleSlickSlider'),
		"subject" : $(this),
		"animateOnce" : false, // animation only runs once
		"getDimensions" : (instance) => {
			
			// get top offets of .item
			// let t1 = $('.peopleSlickSlider').position().top;
			// let t2 = instance.trigger.position().top;
			// instance.top = t1 + t2;

			instance.top = instance.trigger.position().top;
			instance.height = instance.trigger.height();
		},
		"monitor" : (instance,scrolltop) => {

			let percent = 0;

			// ------------------------------------------------
			// single shot animation - add class to each element
			// ------------------------------------------------

			// ------------------------------------------------
			// scrub animation based on scroll position
			// ------------------------------------------------

			// instance.top => top of trigger hits top of viewport
			// instance.top - instance.wh => top of trigger hits bottom of viewport
			// instance.bottom => bottom of trigger hits top of viewport
			// instance.bottom - instance.wh => bottom of trigger hits bottom of viewport

			let start = instance.top - (instance.height * 0.75);
			let end = start + (instance.height * 0.75);

			if ( scrolltop > start && scrolltop < end ) {
				percent =  (scrolltop - start ) / ( end - start );
			}

			if ( scrolltop > end ){
				percent = 1.0;
			}

			$('#dbTop').text(instance.top);
			$('#dbBottom').text(instance.bottom);
			$('#dbHeight').text(instance.height);
			$('#dbScrolltop').text(scrolltop);
			$('#dbStart').text(start);
			$('#dbEnd').text(end);
			$('#dbPercent').text(percent);

			instance.update(percent);
		},
		"update" : (instance,percent) => {

			// ------------------------------------------------
			// scrub animation based on scroll position
			// ------------------------------------------------

			if ( percent > 0.0 && percent < 1.0) {
					instance.trigger.addClass('animating');
			} else {
					instance.trigger.removeClass('animating');
			}

			if ( percent === 1.0) {
				instance.trigger.removeClass('animating');

				// animation only runs once
				if( instance.animateOnce) {
					instance.trigger.addClass('animationComplete');
					instance.animationComplete = true;
				}
			}

			window.requestAnimationFrame(() => {
				instance.subject.css({"opacity" : percent, "top" : Math.floor( (1.0 - percent) * 500 ) + "px"});	
			});

		}
	}));
});
*/


/* ------------------------------------------------ */
