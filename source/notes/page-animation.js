/* ------------------------------------------------ */
// PageAnimations
/* ------------------------------------------------ */

class PageAnimations{
	constructor(parameters) {
		this.parameters = parameters;

		this.init();
	}

	init() {
		let that = this;
	}

	apply() {
		let that = this;

		// add case study and news/blog post items
		// case study: case-study-banner, case-study-intro, case-study-single_column_text, case-study-fullwidth_image, case-study-awards, case-study-fullwidth_video, case-study-image_carousel, case-study-two_column_media
		// news post : postTitle, singlePost, otherPosts
		const types = /(twoColumnMixed|sharedCaseStudies|latestClients|latest-news|people|homeValues|singleColumn|fuse|tools|cxmv3|nimble|group|twoColumnParallax|otherPosts|trinity|awards|fullWidthImage|case-study-banner|case-study-intro|case-study-single_column_text|case-study-fullwidth_image|case-study-awards|case-study-fullwidth_video|case-study-image_carousel|case-study-two_column_media|singlePost|valuesBlocks)/;

		const boxes = gsap.utils.toArray('.module');
		boxes.forEach(box => {

			let moduleClass = box.getAttribute('class');

			let matches = moduleClass.match(types);
			if ( matches ) {
				let moduleType = matches[0];

				if ( moduleType === 'homeBanner' || moduleType === 'pageBanner' ) {

				} else {
					console.log( "box: '%s'", moduleType );

					switch ( moduleType ) {

						case 'twoColumnMixed' : {
							let thisBox = $(box);

							// text column
							let textColumn = thisBox.find('.textColumn');
							// image column
							let imageColumn = thisBox.find('.imageColumn');

						  gsap.to(textColumn, { 
						    y: -500,
						    autoAlpha: 1,
						    scrollTrigger: {
						    	"start" : () => 'top-=' + halfheight, // top half way up screen
						    	"end" : 'bottom bottom', // bototm hits bototm of screen
					        markers : false,
						      trigger: box,
						      scrub: false,
						    }
						  })

						  gsap.to(imageColumn, { 
						    y: -500,
						    autoAlpha: 1,
						    scrollTrigger: {
						    	"start" : () => 'top-=' + halfheight, // top half way up screen
						    	"end" : 'bottom bottom', // bototm hits bototm of screen
						      trigger: box,
						      scrub: false,
						    }
						  })
						} break;

						case 'sharedCaseStudies' : {
							let thisBox = $(box);

							// 4th October desing amends
							// home page - carousel .carouselLayout
							// work page - grid .gridLayout
							
							/*
							batchAnimation('.sharedCaseStudies .caseStudy', 6, {
								"e1" : {
									autoAlpha: 1,
									y:-500,
									stagger: 0.15,
									overwrite: true
								}
							});
							*/

						} break;

						case 'latestClients' : {
							// let thisBox = $(box);

							this.batchAnimation('.latestClients .item', 6, {
								"e1" : {
									autoAlpha: 1,
									y:-500,
									stagger: 0.15,
									overwrite: true
								}
							});
						} break;

						case 'latest-news' : 
						case 'otherPosts' : {
							let thisBox = $(box);

							this.batchAnimation('.newsitem', 3, {
								"e1" : {
									autoAlpha: 1,
									y:-500,
									stagger: 0.15,
									overwrite: true
								}
							});
						} break;

						case 'people' : {
							let thisBox = $(box);

							// different behaviour on home page and 'People' page

							if ( thisBox.hasClass('peopleSlickSlider') ) {

								// interferes with slickslider animation
								/*
								batchAnimation('.people .item .outer .frame figure', 3, {
									"e1" : {
										autoAlpha: 1,
										y:-500,
										stagger: 0.15,
										overwrite: true
									}
								});
								*/
							} else {

								// intro header
								let headrSub = thisBox.find('.teamIntro');
							  gsap.to(headrSub, { 
							    y: -500,
							    autoAlpha: 1,
							    scrollTrigger: {
							    	"start" : 'top bottom',
							    	"end" : 'bottom bottom',
						        // "start" : () => 'top-=' + halfheight,
						        // "end" : () => 'bottom-=' + winheight,
							      trigger: box,
							      scrub: false,
							    }
							  })

								this.batchAnimation('.people .item', 3, {
									"e1" : {
										autoAlpha: 1,
										y:-500,
										stagger: 0.15,
										overwrite: true
									}
								});
							}
						} break;
						
						case 'singleColumn' : {
							let thisBox = $(box);

							let textColumn = thisBox.find('.ba');
							let th = textColumn.height();
						  gsap.to(textColumn, { 
						    y: -500,
						    autoAlpha: 1,
						    scrollTrigger: {
						    	"start" : () => 'top-=' + halfheight, // top half way up screen
						    	"end" : () => 'top-=' + (halfheight - th),
					        // "start" : () => 'top-=' + halfheight,
					        // "end" : () => 'bottom-=' + winheight,
						      trigger: box,
						      scrub: false,
						      markers : false
						    }
						  })

						} break;

						case 'fuse' : {
							// console.log('fuse classes.js 2180');
						} break;

						case 'cxm' : {
							let thisBox = $(box);


							// -> BuggerScroller
						} break;

						case 'twoColumnParallax' : {
							let thisBox = $(box);

							this.batchAnimation('.twoColumnParallax .image, .twoColumnParallax .text', 2, {
								"e1" : {
									autoAlpha: 1,
									y:-500,
									stagger: 0.15,
									overwrite: true
								}
							});

						} break;

						case 'valuesBlocks' : {
							let thisBox = $(box);

							this.batchAnimation('.valuesBlocks .image, .valuesBlocks .text', 2, {
								"e1" : {
									autoAlpha: 1,
									y:-500,
									stagger: 0.15,
									overwrite: true
								}
							});

						} break;

						case 'trinity' : {
							console.log('trinity');
							let thisBox = $(box);

						} break;

						case 'awards' : {
							let thisBox = $(box);

							// intro header
							let headrSub = thisBox.find('.introbox');
							let th = headrSub.height();
						  gsap.to(headrSub, { 
						    y: -500,
						    autoAlpha: 1,
						    scrollTrigger: {
						    	"start" : () => 'top-=' + (winheight * 0.75), 
						    	"end" : () => 'top-=' + (halfheight - th),
						      trigger: box,
						      scrub: false,
						      markers : false
						    }
						  })

							this.batchAnimation('.awards .award', 3, {
								"e1" : {
									autoAlpha: 1,
									y:-500,
									stagger: 0.15,
									overwrite: true
								}
							});
						} break;

						case 'fullWidthImage' : {
							let thisBox = $(box);

						} break;

						case 'case-study-banner' : {
							let thisBox = $(box);

						} break;

						case 'case-study-intro' : {
							let thisBox = $(box);

							let blocks = thisBox.find('p');
						  gsap.to(blocks, { 
						    y: -500,
						    autoAlpha: 1,
						    scrollTrigger: {
						    	"start" : () => 'top-=' + (winheight + 250), 
						    	"end" : () => 'top-=' + (winheight),
						      trigger: box,
						      scrub: false,
						      markers : false
						    }
						  })

						} break;

						case 'case-study-fullwidth_image' : {
							let thisBox = $(box);

							let img = thisBox.find('img');
							let imgWidth = img.attr('width'); 
							let imgHeight = img.attr('height'); 
							let tw = thisBox.find('figure').width();
							let scale = tw / imgWidth;
							let nw = imgWidth * scale;
							let nh = imgHeight * scale;

							thisBox.find('figure').attr({
								"width" : nw,
								"height" : nh
							});

							// console.log("iw %s ih %s tw %s scale %s nw %s nh %s", imgWidth, imgHeight, tw, scale, nw, nh);

							$(window).on('resize',() => {
								let img = thisBox.find('img');
								let imgWidth = img.attr('width'); 
								let imgHeight = img.attr('height'); 
								let tw = thisBox.find('figure').width();
								let scale = tw / imgWidth;
								let nw = imgWidth * scale;
								let nh = imgHeight * scale;

								thisBox.find('figure').attr({
									"width" : nw,
									"height" : nh
								});

								// console.log("iw %s ih %s tw %s scale %s nw %s nh %s", imgWidth, imgHeight, tw, scale, nw, nh);
							});

							// let th = thisBox.height();
							let th = nh;

							let blocks = thisBox.find('figure');
						  gsap.to(blocks, { 
						    y: -500,
						    autoAlpha: 1,
						    scrollTrigger: {
						    	"start" : () => 'top-=' + (winheight + (th / 2)), 
						    	"end" : () => 'top-=' + ((winheight + (th / 2)) - 250),
						      trigger: box,
						      scrub: false,
						      markers : false
						    }
						  })
						} break;

						case 'case-study-awards' : {
							let thisBox = $(box);
							// h2,h3
							// batch: .items .point

						  // ---------- ** ----------
						  // GenericScrollHandler -->
						  // ---------- ** ----------

							let peopleAnimation = new GenericScrollHandler({
								"trigger" : $('.case-study-awards'),
								"subject" : $('.case-study-awards'),
								"animateOnce" : true, // animation only runs once
								"getDimensions" : (instance) => {
									instance.top = instance.trigger.position().top;
									instance.height = instance.trigger.height();
									instance.bottom = instance.top + instance.height;

									instance.start = instance.top - (instance.wh * 0.75);
									instance.end = instance.bottom - (instance.wh * 0.5);

								},
								"monitor" : (instance,scrolltop) => {
									if ( scrolltop < instance.start ) {
										instance.update(0.0);	
									}

									if ( scrolltop > instance.start && scrolltop < instance.end ) {
										let percent =  (scrolltop - instance.start ) / ( instance.end - instance.start );
										instance.update(percent);	
									}
									
									if (scrolltop >= instance.end) {
										instance.update(1.0);
									}
								},
								"update" : (instance,percent) => {
									// fade in
									points.css({
										"opacity" : percent,
										"top" : Math.floor((1.0 - percent) * 250) + "px" 
									});

									// animate number
									counters.map(function(counter){
										counter.update(percent);
									}, this);
								}
							});

						} break;

						case 'case-study-fullwidth_video' : {
							let thisBox = $(box);
							let th = thisBox.height();

							let blocks = thisBox.find('.videoBox');
						  gsap.to(blocks, { 
						    y: -500,
						    autoAlpha: 1,
						    scrollTrigger: {
						    	"start" : () => 'top-=' + (winheight + (th / 2)), 
						    	"end" : () => 'top-=' + ((winheight + (th / 2)) - 250),
						      trigger: box,
						      scrub: false
						    }
						  })
						} break;

						case 'case-study-two_column_media' : {
							let thisBox = $(box);
							let th = thisBox.height();
							let mediaAnimations = [];
							let blocks = thisBox.find('.media');

							blocks.each(function(i){
								let thisItem = $(this);
								let th = thisItem.height();

								mediaAnimations.push(gsap.to(thisItem, { 
							    y: -500,
							    autoAlpha: 1,
							    scrollTrigger: {
							    	"start" : () => 'top-=' + (winheight + (th / 2)), 
							    	"end" : () => 'top-=' + ((winheight + (th / 2)) - 250),
							      trigger: box,
							      scrub: false
							    }
							  }));
							});

						} break;

						case 'singlePost' : {
							let thisBox = $(box);

							let copyAnimations = [];
							let blocks = thisBox.find('.content p, .content figure');

							blocks.each(function(i){
								let thisItem = $(this);
								let th = thisItem.height();

								copyAnimations.push(gsap.to(thisItem, { 
							    y: -500,
							    autoAlpha: 1,
							    scrollTrigger: {
							    	"start" : () => 'top-=' + (winheight+250), 
							    	"end" : () => 'top-=' + ((winheight+250) - 250),
							      trigger: thisItem,
							      scrub: false,
							      markers : false
							    }
							  }));
							  
							});

						} break;

					}
				};
			}
		});

		this.lottieScroller();
	}

	batchAnimation( items, batchSize, animation ){

		// https://codepen.io/GreenSock/pen/823312ec3785be7b25315ec2efd517d8

		// usage:
		batch(items, {
		  interval: 0.1, 
		  batchMax: batchSize,   

		  onEnter: batch => gsap.to(batch, animation.e1),
			start : () => 'top-=' + (winheight + 480),
			end : () => 'top-=' + winheight,
			markers : false
		});

		// the magical helper function (no longer necessary in GSAP 3.3.1 because it was added as ScrollTrigger.batch())...
		function batch(targets, vars) {

		  let varsCopy = {},
		      interval = vars.interval || 0.1,
		      proxyCallback = (type, callback) => {
		        let batch = [],
		            delay = gsap.delayedCall(interval, () => {callback(batch); batch.length = 0;}).pause();
		        return self => {
		          batch.length || delay.restart(true);
		          batch.push(self.trigger);
		          vars.batchMax && vars.batchMax <= batch.length && delay.progress(1);
		        };
		      },
		      p;

		  for (p in vars) {
		    varsCopy[p] = (~p.indexOf("Enter") || ~p.indexOf("Leave")) ? proxyCallback(p, vars[p]) : vars[p];
		  }
		  
		  gsap.utils.toArray(targets).forEach(target => {
		    let config = {};
		    for (p in varsCopy) {
		      config[p] = varsCopy[p];
		    }
		    config.trigger = target;

		    ScrollTrigger.create(config);
		  });
		}
	}

}

/*
let thisPageAnimation = new PageAnimations({});
thisPageAnimation.apply();
*/
