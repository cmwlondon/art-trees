<?php
/*
layout: document_list
source/scss/modules/document_list.scss
*/
$moduleIdentifier = $args['module_identifier'];
$items_per_page = 4;
$title = get_the_title($post->ID);
$allCategories = getDocumentCategories();
$mainList = $allCategories['main'];
$subList = $allCategories['sub'];
$heirarchy  = $allCategories['heirarchy'];

$showNocat = (get_field('show_nocat','option') === 'yes' );
// sort categories into arbitrary order

$sortOrder = get_field('category_order','option');
echo "<pre>".print_r( $mainList, true )."</pre>";
echo "<pre>".print_r( $subList, true )."</pre>";
echo "<pre>".print_r( $sortOrder, true )."</pre>";
// expand to include sub categories and documents

// has an order for the main catgeories been set?
if ( !empty($sortOrder) ) {
	// yes
	// apply order to hierarchy
	$orderedCategories = [];
	$unorderedCategories = [];

	$catorder = [];
	foreach ( $sortOrder AS $csel ) {
		$catorder[] = $csel['category-select'];
	}

	// $catorder = ['documents-and-templates','summaries-guidance','external-resources'];
	// iterate over selected main category order
	foreach( $catorder AS $tc) {
		if( array_key_exists($tc, $heirarchy )) {
			$orderedCategories[] = $heirarchy[$tc];
		}
	}

	// at this point the main categories included in $sortOrder are selected 
	// find main categories NOT listed in sortOrder
	foreach( $heirarchy AS $key => $_main ) {
		if ( !in_array($key, $catorder) ) {
			$unorderedCategories[] = $_main;	
		}
	}

	// append unordered categories to end
	$allCategories = array_merge( $orderedCategories, $unorderedCategories );

} else {
	// no
	// return main catgeories in the order they are returned from the database
	$allCategories = $heirarchy;
}

/* ----------------------------------------------------------------- */
// FIKLTER OUT MAIN NONCAT CATEGORY
if ( !$showNocat ) {
$_allCategories = [];
foreach ( $allCategories AS $cat ) {
	if ( $cat ['main']['slug'] !== 'nocat' ) {
		$_allCategories[] = $cat;
	} 
}
$allCategories = $_allCategories;
}
/* ----------------------------------------------------------------- */

/*
echo "<p>sort order</p>";
echo "<pre>".print_r( $sortOrder, true )."</pre>";
*/

// get main/sub document categpry from URL
preg_match('/^.+\/([a-z\-\_]+)\/(?:\?sub=([a-z\-\_]*))*(?:\&pn=([0-9]+))*$/', current_location(), $matches);
$main = ( array_key_exists( 1, $matches ) ) ? $matches[1] : '';
$sub = ( array_key_exists( 2, $matches ) ) ? $matches[2] : '';
$currentPage = ( array_key_exists( 3, $matches ) ) ? (int) $matches[3] : 1;

// check to see if main/sub in URL exists in the document categories
// $allCategories[n]['main']['slug']
// $allCategories[n]['sub'][<key>['slug']
$mainFlag = false;
// check to see if main exists if not (sub is irrelevant)
foreach ( $allCategories AS $key => $mainCat ) {
	if ( $mainCat['main']['slug'] === $main ) {
		$mainFlag = true;
		break;
	}
}
// default to $allCategories[0]['sub'][0]
if ( !$mainFlag ) {
	$main = $allCategories[0]['main']['slug'];
	$sub = '';
}

$filteredDocs = getDocumentsByFilterAndPage([ 'main' => $main, 'sub' => $sub ], $currentPage, $items_per_page);
$_allFilteredDocuments = $filteredDocs['documents'];
$pages = $filteredDocs['pages'];
$_docs = $_allFilteredDocuments;

// $pages = 1;
// $_docs = [];

// get inital main and sub name properties
$mainName = '';
$subName = '';
$vx = [];
foreach( $heirarchy AS $key1 => $nm ) {
	$vx[ $key1 ] = htmlspecialchars_decode($nm['main']['name']);

	if ( array_key_exists( 'sub', $nm ) ) {
		foreach( $nm['sub'] AS $key2 => $sm ) {
			$vx[ $key2 ] = htmlspecialchars_decode($sm['name']);
		}
	}
}

$fmt = $vx[$main];
if ( !empty( $sub) ) {
	$fmt =  "$fmt &gt; ". $vx[$sub];
}

// ------------------------------------------------------------------------------------------

?>
<script>
const masterCategories = <?= json_encode( $vx ) ?>;
const siteURL = '<?= get_site_url() ?>';
</script>
<style>
#<?= $moduleIdentifier; ?>{}	
</style>
<section class="module document_list" id="<?= $moduleIdentifier; ?>">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1><?= $title ?></h1>
			</div>
		</div>
		<div class="row">

			<!-- category list START -->
			
			<div class="col-xl-3 col-lg-4 docfilter">
		
				<div class="filter">
					<div class="control">
						<h2>Filter by document type:</h2>
						<p class="currentFilter">Current filter: <?= $fmt ?></p>
					</div>
					<div class="clipper">
						<ul class="main">
							<?php foreach ( $allCategories AS $catmain) :
								$categoryMain = $catmain['main'];
								$categorySub = [];
								if ( array_key_exists( 'sub', $catmain  ) ) :
									$categorySub = $catmain['sub'];
								endif;
								
								$isCurrentParent = false;
								foreach ( $categorySub AS $cs ) {
									if( $cs['slug'] === $sub && $categoryMain['slug'] === $main ) {
										$isCurrentParent = true;
										break;
									}
								}

							?>

								<li class="accordion <?php if( $categoryMain['slug'] === $main ) : ?>current<?php endif; ?> <?php if ( $isCurrentParent ) { echo "open"; } ?>" data-main="<?= $categoryMain['slug'] ?>">
									<div class="title">
										<h2><?= $categoryMain['name'] ?></h2>
									</div>
									<div class="clipper">
										<div class="content">
										<?php if ( count( $categorySub ) ) : ?>
											<ul class="sub">
										<?php foreach ( $categorySub AS $catsub ) :?>
											<li <?php if( $catsub['slug'] === $sub && $categoryMain['slug'] === $main ) : ?>class="current"<?php endif; ?>>
												<a href="/trees/<?= $catmain['main']['slug'] ?>/?sub=<?= $catsub['slug'] ?>" data-action="<?= $catmain['main']['slug'].','.$catsub['slug'] ?>"><?= $catsub['name'] ?></a>
											</li>
										<?php endforeach; ?>
											</ul>
										<?php endif; ?>
										</div>
									</div>
								</li>

								<?php if (false) : ?>
								<li <?php if( $categoryMain['slug'] === $main ) : ?>class="current"<?php endif; ?>>

									<a href="/trees/<?= $categoryMain['slug'] ?>/" data-action="<?= $categoryMain['slug'] ?>"><?= $categoryMain['name'] ?></a>
									<?php if ( count( $categorySub ) ) : ?>
										<ul class="sub">
									<?php foreach ( $categorySub AS $catsub ) :?>
										<li <?php if( $catsub['slug'] === $sub && $categoryMain['slug'] === $main ) : ?>class="current"<?php endif; ?>>
											<a href="/trees/<?= $catmain['main']['slug'] ?>/?sub=<?= $catsub['slug'] ?>" data-action="<?= $catmain['main']['slug'].','.$catsub['slug'] ?>"><?= $catsub['name'] ?></a>
										</li>
									<?php endforeach; ?>
										</ul>
									<?php endif; ?>
								</li>
								<?php endif; ?>

							<?php endforeach; ?>
						</ul>
					</div>
				</div>

			</div>

			<!-- category list END -->

			<!-- document list START -->
			
			<div class="col-xl-9 col-lg-8 docList">
				<div class="row documentContainer" data-main="<?= $main ?>" data-sub="<?= $sub ?>" data-items-per-page="<?= $items_per_page ?>" data-pages="<?= $pages ?>" data-page="<?= $currentPage ?>">
					<div class="wt">
						<div class="docBox">
							<?php if ( !empty($sub) ) : ?>

							<!-- subcategpory defined START -->
							<ul class="doctop">
								<?php foreach( $_docs AS $doc ) :
									$sts = $doc['translations'];
									$typeicon = ( count($sts) > 1 ) ? 'pdf' : $sts[0]['type'];
								?>
								<li class="doc">
									<div class="item <?= $typeicon ?> multiLanguage">
										<h3><?= $doc['title'] ?></h3>
										<ul>
											<?php if ( count($sts) > 1 ) : ?>
												<?php foreach ( $sts AS $st ) :
													// $caption = $st['language'];
													$caption = ucfirst($st['language'])." ({$st['size']} {$st['type']})";
												?>
													<li class="translation"><a href="<?= $st['url'] ?>" title="<?= "{$st['language']} ({$st['size']} {$st['type']})" ?>" target="_blank"><?= $caption ?></a></li>
												<?php endforeach; ?>
											<?php else :
												$st = $sts[0];
												$caption = ucfirst($st['language'])." ({$st['size']} {$st['type']})";
											?>
												<li class="translation"><a href="<?= $st['url'] ?>" title="<?= "{$st['language']} ({$st['size']} {$st['type']})" ?>" target="_blank"><?= $caption ?></a></li>
											<?php endif; ?>

										</ul>	
									</div>
								</li>
								<?php endforeach; ?>
							</ul>
							<!-- END -->

							<?php else : ?>

							<!-- subcategpory not defined, show all under main START -->
							<ul class="doctop">
								<?php foreach( $_docs AS $subcategory ) : ?>
								<li class="sub">
									<h2><?= $subcategory['name'] ?></h2>
									<ul>
										<?php foreach( $subcategory['docs'] AS $subdoc ) :
											$sts = $subdoc['translations'];
											$typeicon = ( count($sts) > 1 ) ? 'pdf' : $sts[0]['type'];
											// $typeicon = 'pdf';
										?>
										<li class="doc">
											<div class="item <?= $typeicon ?> multiLanguage">
												<h3><?= $subdoc['title'] ?></h3>
												<?php if ( count($sts) > 0 ) : ?>
												<ul>
												<?php if ( count($sts) > 1 ) : ?>
													<?php foreach ( $sts AS $st ) :
													// $caption = $st['language'];
													$caption = ucfirst($st['language'])." ({$st['size']} {$st['type']})";
													?>
														<li class="translation"><a href="<?= $st['url'] ?>" title="<?= "{$st['language']} ({$st['size']} {$st['type']})" ?>" target="_blank"><?= $caption ?></a></li>
													<?php endforeach; ?>
												<?php else :
													$st = $sts[0];
													$caption = ucfirst($st['language'])." ({$st['size']} {$st['type']})";
												?>
													<li class="translation"><a href="<?= $st['url'] ?>" title="<?= "{$st['language']} ({$st['size']} {$st['type']})" ?>" target="_blank"><?= $caption ?></a></li>
												<?php
												endif;
												?>
												</ul>	
												<?php
												endif;
												?>
											</div>
											
										</li>
										<?php
										endforeach;
										?>
									</ul>
								</li>
							<?php endforeach; ?>
							</ul>
							<!-- END -->
							<?php endif; ?> 
						</div>

						<div class="paging" data-min="1" data-max="<?= $pages ?>">
						<?php if ( $pages > 1 ) : ?>
							<!-- <a href="/trees/<?= $main ?>/?sub=<?= $sub ?>&amp;pn=1" class="btn2 transparent <?php if ( $currentPage === 1 ) : ?>current<?php endif; ?>" data-action="1">FIRST</a> -->
							<?php
							$n = 0;
							while ($n < $pages ) {
								$page = $n + 1;
							?>
								
								<a href="/trees/<?= $main ?>/?sub=<?= $sub ?>&amp;pn=<?= $page ?>" class="btn2 transparent page <?php if ( $currentPage === $page ) : ?>current<?php endif; ?>" data-action="<?= $page ?>"><?= $page ?></a>
							<?php
								$n++;
							}
							?>
							<!-- <a href="/trees/<?= $main ?>/?sub=<?= $sub ?>&amp;pn=<?= $pages ?>" class="btn2 transparent <?php if ( $currentPage === $pages ) : ?>current<?php endif; ?>" data-action="<?= $pages ?>">LAST</a> -->
						<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			
			<!-- document list END -->

		</div>
	</div>

	<ul class="documentListTemplates">
		<li class="sub">
			<h2>SUBCATEGORY</h2>
			<ul>
				<!-- docs -->
			</ul>
		</li>
		<li class="doc">
			<div class="item multiLanguage">
				<h3></h3>
				<ul>
					<!-- docitems -->
				</ul>	
			</div>
		</li>
		<li class="translation">
			<a href="URL" title="LANGUAGE (SIZE TYPE)" target="_blank">LANGUAGE</a>
		</li>
	</ul>
</section>