	lottieScroller() {
		// who we are 'CXM' mdule map animations
		if ( $('section.cxmv3').length > 0 ){
			let target = $('.lst-cxm');
			let source = target.attr('data-source');

			let cxmAnimationFrames = lottie.loadAnimation({
				container: target.get(0), 
				renderer: 'svg',
				loop: false,
				autoplay: false,
				path: source 
			});

			let cxmGraphAnimation = new GenericScrollHandler({
				"trigger" : $('.cxmv3'),
				"subject" : $('.cxmv3 figure'),
				"animateOnce" : false, // animation only runs once
				"getDimensions" : (instance) => {
					instance.top = instance.trigger.position().top;
					instance.height = instance.trigger.height();
					instance.bottom = instance.top + instance.height;

					instance.start = instance.top - (instance.wh / 2); // top of module halfway up browser window
					instance.end = instance.bottom - (instance.wh / 2); // bottom of module halfway up browser window
				},
				"monitor" : (instance,scrolltop) => {
					let percent = 0;

					instance.state = false;

					// not reach CXM block yet
					if ( scrolltop < instance.start ) {
						// first frame
						instance.update(0.0);
					}

					// CXM block visible
					if ( scrolltop > instance.start && scrolltop < instance.end ) {

						let percent =  (scrolltop - instance.start ) / ( instance.end - instance.start );
						instance.state = true;
						instance.update(percent);	
					}

					// scrolled past CXM block
					if ( scrolltop > instance.end ) {
						// last frame
						instance.update(1.0);
					}
				},
				"update" : (instance,percent) => {

					/* this is the important part */
					cxmAnimationFrames.goToAndStop(percent * (cxmAnimationFrames.totalFrames - 1), true);			
				}
			});
		}

		// if ( $('.lst-fuse').length > 0 ) {}

		// 'Who we are page' tools section
		if ( $('section.tools').length > 0 ) {
			let toolsAnimation = new GenericScrollHandler({
				"trigger" : $('.tools'),
				"subject" : $('.tools .items'),
				"animateOnce" : false, // animation only runs once
				"getDimensions" : (instance) => {
					instance.top = instance.trigger.position().top;

					instance.height = instance.trigger.height();
					instance.bottom = instance.top + instance.height;

					instance.start = instance.top; // top of module halfway up browser window
					instance.end = instance.bottom - (instance.wh); // bottom of module halfway up browser window

					instance.current = 0;

					// $('body').append($('<div></div>').addClass('mark').css({"top" : instance.top + "px", "background-color" : "green"}) );
					// $('body').append($('<div></div>').addClass('mark').css({"top" : instance.bottom + "px", "background-color" : "red"}) );

					// $('body').append($('<div></div>').addClass('mark').css({"top" : instance.start + "px", "background-color" : "green", "height" : "1px"}) );
					// $('body').append($('<div></div>').addClass('mark').css({"top" : instance.end + "px", "background-color" : "red", "height" : "1px"}) );
				},
				"monitor" : (instance,scrolltop) => {
					let percent = 0;

					if ( scrolltop < instance.top ) {
						// first frame
						instance.update(0.0);
					}

					if ( scrolltop >= instance.top && scrolltop <= (instance.top + toolTrack) ) {
						// last frame
						let percent = (scrolltop - instance.top) / toolTrack;
						instance.update(percent);
					}

					/*
					if ( scrolltop > instance.start && scrolltop < instance.end ) {

						let percent =  (scrolltop - instance.start ) / ( instance.end - instance.start );
						instance.state = true;
						instance.update(percent);	
					}
					*/

				},
				"update" : (instance,percent) => {

					if ( percent > 0 ) {
						let topmargin = (percent * toolTrack);
						$('.tools').css({"margin-top" : topmargin + "px"});
					}

					let progress = $('.toolsProgress li');
					let slider = $('.toolsWrapper2');

					if (percent > 0.0 && percent < 0.2) {
						// 1
						instance.current = 0;
						slider.css({"left" : "0%"});
						progress.eq(0).addClass('active');
						progress.slice(1,5).removeClass('active');
					}

					if (percent > 0.2 && percent < 0.4) {
						// 2
						instance.current = 1;
						slider.css({"left" : "-100%"});
						progress.slice(0,2).addClass('active');
						progress.slice(2,5).removeClass('active');
					}

					if (percent > 0.4 && percent < 0.6) {
						// 3
						instance.current = 2;
						slider.css({"left" : "-200%"});
						progress.slice(0,3).addClass('active');
						progress.slice(3,5).removeClass('active');
					}

					if (percent > 0.6 && percent < 0.8) {
						// 4
						instance.current = 3;
						slider.css({"left" : "-300%"});
						progress.slice(0,4).addClass('active');
						progress.slice(4,5).removeClass('active');
					}

					if (percent > 0.8 && percent < 1.0) {
						// 5
						instance.current = 4;
						slider.css({"left" : "-400%"});
						progress.addClass('active');
					}

					if (percent > 0.8) {
						$('.toolsProgress').addClass('end');
					} else {
						$('.toolsProgress').removeClass('end');
					}

				}
			});

			/*
			$('.nextItem').on('click',function(e){
				toolsAnimation.current++;

				// slight offset 0.21 instead of 0.2 (20%) to move past scroll point
				// 0 0.0
				// 1 0.21
				// 2 0.42
				// 3 0.63
				// 4 0.84

				let of = ( (toolsAnimation.end - toolsAnimation.start) * (toolsAnimation.current * 0.21) ) + toolsAnimation.start;
				window.scrollTo(0, of );
			});

			$('.toolsProgress li').on('click',function(e){
				let panel = $('.toolsProgress li').index($(this));

				if (panel !== toolsAnimation.current) {
					toolsAnimation.current = panel;
					let of = ( (toolsAnimation.end - toolsAnimation.start) * (toolsAnimation.current * 0.21) ) + toolsAnimation.start;
					window.scrollTo(0, of );
				}
			});
			*/
		}

		// 'Who we are' page, home page 'Group' module - bind scroll to map animation
		if ( $('.lst-group').length > 0 ) {
		// set up mobile map
			let mtarget = $('.lst-group-m');
			let msource = mtarget.attr('data-source');
			let gmmAnimationFrames = lottie.loadAnimation({
				container: mtarget.get(0), 
				renderer: 'svg',
				loop: false,
				autoplay: false,
				path: msource 
			});

			// set up desktop map
			let dtarget = $('.lst-group-d');
			let dsource = dtarget.attr('data-source');
			let gmdAnimationFrames = lottie.loadAnimation({
				container: dtarget.get(0), 
				renderer: 'svg',
				loop: false,
				autoplay: false,
				path: dsource 
			});

			let gmdGraphAnimation = new GenericScrollHandler({
				"trigger" : $('.group'),
				"subject" : $('.group figure.mapgraph'),
				"animateOnce" : false, // animation only runs once
				"getDimensions" : (instance) => {

					instance.top = instance.trigger.position().top;

					if ($('.toolsWrapper1').length > 0) {
						instance.top = instance.top + toolTrack;
					}

					let headingHeight = instance.trigger.find('.topclip').height();
					let introHeight = instance.trigger.find('.msqinfo').height();
					instance.top = instance.top + ( headingHeight + introHeight);

					instance.height = instance.trigger.height();
					instance.bottom = instance.top + instance.height;

					instance.start = instance.top - (instance.wh / 2); // top of module halfway up browser window
					instance.end = instance.top;
					// instance.end = instance.bottom - (instance.wh / 2); // bottom of module halfway up browser window

					/*
					console.log("group start: %s end: %s", instance.start, instance.end);
					$('body').append($('<div></div>').addClass('mark').css({"top" : instance.top + "px", "background-color" : "black", "height" : "1px"}) );
					$('body').append($('<div></div>').addClass('mark').css({"top" : instance.bottom + "px", "background-color" : "black", "height" : "1px"}) );
					$('body').append($('<div></div>').addClass('mark').css({"top" : instance.start + "px", "background-color" : "green", "height" : "1px"}) );
					$('body').append($('<div></div>').addClass('mark').css({"top" : instance.end + "px", "background-color" : "red", "height" : "1px"}) );
					*/
				},
				"monitor" : (instance,scrolltop) => {
					let percent = 0;

					instance.state = false;

					// not reach CXM block yet
					if ( scrolltop < instance.start ) {
						// first frame
						instance.update(0.0);
					}

					// CXM block visible
					if ( scrolltop > instance.start && scrolltop < instance.end ) {

						let percent =  (scrolltop - instance.start ) / ( instance.end - instance.start );
						instance.state = true;
						instance.update(percent);	
					}

					// scrolled past CXM block
					if ( scrolltop > instance.end ) {
						// last frame
						instance.update(1.0);
					}
				},
				"update" : (instance,percent) => {

					/* this is the important part */
					gmdAnimationFrames.goToAndStop(percent * (gmdAnimationFrames.totalFrames - 1), true);			
					gmmAnimationFrames.goToAndStop(percent * (gmmAnimationFrames.totalFrames - 1), true);			
				}
			});
		}

	}
