

http://arttree.local/trees/documents-and-templates/
http://arttree.local/trees/summaries-guidance/
http://arttree.local/trees/external-resources/

rewrite
	document subtree:
	http://arttree.local/trees/documents-and-templates/?sub=calculations-worksheet
	to
	http://arttree.local/trees/documents-and-templates/calculations-worksheet

	document page
	http://arttree.local/trees/documents-and-templates/?sub=calculations-worksheet&pn=2
	to
	http://arttree.local/trees/documents-and-templates/calculations-worksheet/2

/trees/documents-and-templates/([a-z\=]+)
-> 


https://wpmudev.com/blog/building-customized-urls-wordpress/?utm_expid=3606929-90.6a_uo883STWy99lnGf8x1g.0

https://developer.wordpress.org/reference/functions/add_rewrite_rule/
	A quick example: processing rules to make a subscribers page, hoping it helps.

	add_action('init', function() {
	 
	    $page_id = 2; // update 2 (sample page) to your custom page ID where you can get the subscriber(s) data later
	    $page_data = get_post( $page_id );
	 
	    if( ! is_object($page_data) ) { // post not there
	        return;
	    }
	 
	    add_rewrite_rule(
	        $page_data->post_name . '/subscriber/([^/]+)/?$',
	        'index.php?pagename=' . $page_data->post_name . '&my_subscribers=1&my_subscriber=$matches[1]',
	        'top'
	    );
	 
	});

	Now in your page template ( if you have one, or while filtering this custom page’s content ), you can get the displayed subscriber slug by calling get_query_var('my_subscriber'), but first, pass 'my_subscriber' to the query variables:

	add_filter('query_vars', function($vars) {
	    $vars[] = "my_subscriber";
	    return $vars;
	});

<?php
/**
 * Add rewrite rules
 *
 * @link https://codex.wordpress.org/Rewrite_API/add_rewrite_rule
 */
function myplugin_rewrite_rule() {
	add_rewrite_rule( '^book/book-author/([^/]*)/?', 'index.php?post_type=book&book-author=$matches[1]','top' );
}
add_action('init', 'myplugin_rewrite_rule', 10, 0);

cange URL format (page parameter) for wordpress pagination
https://wordpress.stackexchange.com/questions/212507/how-to-change-url-structure-for-pagination-pages