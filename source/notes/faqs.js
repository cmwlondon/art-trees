	// FAQ page
	// $('section.faqs')

	if( $('section.faqs').length > 0 ) {
		let faqModule = $('section.faqs');
		let filterItems = faqModule.find('.faqfilter li > a');
		let faqGroupItems = faqModule.find('.faqlist .faqGroup');
		let _addIndex = -1;
		let _removeIndex = -1;

		console.log(faqMenu);

		// <p class="currentFilter">Current filter: <?= $currentFAQ['title'] ?></p>

		// filter
		$('.faqfilter .control').on('click',function(e){
			e.preventDefault();
			let filter = $(this).parents('.faqfilter');

			filter.toggleClass('open');
		});

		$('.faqfilter ul li a').on('click',function(e){
			e.preventDefault();
			let filter = $(this).parents('.faqfilter');

			filter.removeClass('open');

			if ( !$(e.target).hasClass('active') ) {
				let targetGroup = $(e.target).attr('data-target'); 

				// update filter
				filterItems.each((index,element) => {
					if ( $(element).attr('data-target') == targetGroup) {
						$(element).addClass('active');
					} else {
						$(element).removeClass('active');
					}
				});

				faqGroupItems.children().each(function(i){
					$(this).get(0).addEventListener('transitionend', (e) => {
						e.stopPropagation();
					});
				})

				let transitionEnd1 = function(e) {
					// show relevabnt group
					faqGroupItems.eq(_addIndex).addClass('active');

					// update 'current filter' caption (mobile)
					$('p.currentFilter').text("Current filter:" + faqMenu[_addIndex].title );

					// update address bar URL 'gr' parameter, allows sharing of link
					const url = new URL(window.location);
					url.searchParams.set('gr', faqMenu[_addIndex].slug);
					window.history.pushState({}, '', url);					

					e.target.removeEventListener('transitionend',transitionEnd1);
				}

				faqGroupItems.each((index,element) => {
					if ( $(element).hasClass('active')) {
						_removeIndex = index;
					}
				});

				faqGroupItems.each((index,element) => {
					if ( $(element).attr('data-faqgroup') == targetGroup) {
						//$(element).addClass('active');
						_addIndex = index;
					}
				});

				faqGroupItems.eq(_removeIndex).get(0).addEventListener('transitionend', transitionEnd1, {"once" : true});
				faqGroupItems.eq(_removeIndex).removeClass('active');
			}
		});
	}
