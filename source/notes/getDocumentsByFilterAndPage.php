<?php
// filter[ 'main' => '', 'sub' => '' ] main and sub category filter
// page = 1 to N cureent page
// $ipp 1 to N items per page
function getDocumentsByFilterAndPage($filter, $page = 1, $ipp = 4){
	$docPool = getAllDocuments();

	$mainCategoryFilter = $filter['main'];
	$subCategoryFilter = $filter['sub'];

	// build flat array of documents which match the supplied Filter(s)
	$filteredDocs = [];
	foreach ( $docPool AS $doc) {
		$documentMainCategory = $doc['main'];
		$documentSubCategory = $doc['sub'];

		// match main category
		// does the document have a amain category
		if( array_key_exists('slug', $documentMainCategory) ) {
			// yes
			if( $documentMainCategory['slug'] === $mainCategoryFilter ) {

				// does the filter include a subcategory filter 
				if ( !empty( $subCategoryFilter ) ){
					
					// does the document have a subcategory?
					if ( array_key_exists('slug', $documentSubCategory ) ) {

						// yes, does the doc match it
						if( $documentSubCategory['slug'] === $subCategoryFilter ) {
							// yes, return doc

							$filteredDocs[] = $doc;
						}
					} else {
					}
				} else {
					// no subcategory filter, return doc

					// does the document have a subcategory?
					if ( !array_key_exists('slug', $documentSubCategory ) ) {
						$doc['sub'] = [
							'slug' => 'nocat',
							'name' => 'NO CATEGORY'
						];
					}
					$filteredDocs[] = $doc;
				}
			} else {
				// NO, document is not listed 
			}

		}
	}

	// handle paging
	$docCount = count($filteredDocs);
	$pages = ( $docCount % $ipp !== 0) ? floor($docCount / $ipp) + 1 : floor($docCount / $ipp);
	$page = ( $page > $pages || $page < 1 ) ? 1 : $page;
	$offset = ($page - 1) * $ipp;

	// process documents into flat array (main + sub) or hierarchical array (main only)
	$_allFilteredDocuments = $filteredDocs;
	// check to see if subcategory filter is present
	if ( empty( $subCategoryFilter ) ) {
		// no

		// rearrange docs into hierarchy
		$docHierarchy = [];
		foreach( $_allFilteredDocuments AS $doc ) {
			if ( array_key_exists( 'sub', $doc ) && array_key_exists( 'slug', $doc['sub'] ) ) {
				$_sub = $doc['sub']['slug'];

				if ( !array_key_exists($_sub, $docHierarchy) ) {
					$docHierarchy[$_sub]['name'] = $doc['sub']['name'];
				}
				$docHierarchy[$_sub]['docs'][] = $doc;
			}
		}
		// at this point $docHierarchy contains all documents under $main category in hierarchical structure

		// find 'nocat' category and move to end of list
		$isCategorised = array_filter($docHierarchy, function($key) { return $key !== 'nocat'; }, ARRAY_FILTER_USE_KEY);
		$notCategorised = array_filter($docHierarchy, function($key) { return $key === 'nocat'; }, ARRAY_FILTER_USE_KEY);
		$docHierarchy = array_merge( $isCategorised, $notCategorised );

		// paging needs to be handled differently as the heirarchical structure changes the order of items
		// index docs in hierarchical array
		$index = [];
		foreach( $docHierarchy AS $major => $iSub1 ) {
			foreach( $iSub1['docs'] AS $minor => $iSub2 ) {
				$index[] = [ $major, $minor ];
			}
		}

		// clip index based on items-per-page, page
		$index = array_slice( $index, $offset, $ipp);

		// build new versions of $docs based on clipper $index
		$nd = [];
		$thisPageDocCount = 0;
		foreach ( $index AS $indexItem ) {
			$nd[ $indexItem[0] ]['name'] = $docHierarchy[ $indexItem[0] ]['name']; 
			$nd[ $indexItem[0] ]['docs'][] = $docHierarchy[ $indexItem[0] ]['docs'][ $indexItem[1] ];
			$thisPageDocCount++;
		}
		$_docs = $nd;

	} else {
		// yes

		// flat doc list
		$_docs = array_slice( $_allFilteredDocuments, $offset, $ipp);
		$thisPageDocCount = count($_docs);

	}

	// return filtered + paged document array
	return [
		'documents'	=> $_docs,
		'allcount' => count($_allFilteredDocuments),
		'count' => ( $thisPageDocCount < $ipp ) ? $thisPageDocCount : $ipp,
		'pages' => $pages
	];
}
?>