<?php
// AJAX endpoints for work and news/blog posts

function checkNonce( $action ) {
    $nonce = (array_key_exists('nonce', $_GET)) ? $_GET['nonce'] : false;
    if ( !($nonce && wp_verify_nonce( $_GET['nonce'], 'ajax-nonce' )) ) {
        $response = [
            'action' => $action,
            'status' => 'error'
        ];
        echo json_encode( $response );
        exit();
    }
}

// document list page filter,paging

// switch document filter -> return updates list of documents
add_action('wp_ajax_nopriv_docfilter', 'documentFilter');
add_action('wp_ajax_docfilter', 'documentFilter');

function documentFilter(){
    /*
    ** CHANGING THE FILTER RESETS PAGING TO PAGE 1
    */

    // NONCE check
    // checkNonce('document list filter');

    /*
    paraemeters:
    main
    sub
    items-per-page

    field validation:
    filter STRING,STRING / STRING
    items-per-page INT n > 0, n < totla number of documents
    */

    $section = $_GET['section'];
    $filterLevel = $_GET['filter-level'];
    $filter = $_GET['filter'];
    $ipp = $_GET['ipp'];

    $_filter = explode(',', $filter);
    $main = $_filter[0];
    $sub = ( array_key_exists(1, $_filter) ) ? $_filter[1] : '';

    $mappings = [
        'trees' => [
            'field' => 'category_order',
            'category' => 'category-select',
            'subcategory' => 'subcategory-select',
            'documents' => 'documents',
            'document' => 'document-select'
        ],
        'verification' => [
            'field' => 'category_order2',
            'category' => 'category-select2',
            'subcategory' => false,
            'documents' => 'documents2',
            'document' => 'document-select2'
        ],
    ];
    $thisPageDocumentsHandler = new DocumentHandler( $section, $mappings );

    $_docs = $thisPageDocumentsHandler->getOrderedDocumentsByFilterAndPage([ 'main' => $main, 'sub' => $sub ], $filterLevel, 1, $ipp);

    // query 'document' post type matching against main and sub categories
    $response = [
        'action' => 'document list filter',
        'status' => 'OK',
        'section' => $section,
        'filterlevel' => $filterLevel,
        'filter' => $filter,
        'ipp' => $ipp,
        'main' => $main,
        'sub' => $sub,
        'page' => 1,
        'allcount' => $_docs['allcount'],
        'doccount' => $_docs['count'],
        'pagecount' => $_docs['pages'],
        'documents' => $_docs['documents']
    ];

    echo json_encode( $response );

    exit();
}

// page through document in current main/sub category
add_action('wp_ajax_nopriv_docpaging', 'documentPaging');
add_action('wp_ajax_docpaging', 'documentPaging');

function documentPaging(){
    /*
    paraemeters:
    filter
    items-per-page
    page

    field validation:
    filter STRING,STRING / STRING
    items-per-page INT n > 0, n < totla number of documents
    page INT 1 < N < total number of docs / items per page
    */

    $section = $_GET['section'];
    $filterLevel = $_GET['filter-level'];
    $filter = $_GET['filter'];
    $ipp = $_GET['ipp'];
    $page = $_GET['page'];

    $_filter = explode(',', $filter);
    $main = $_filter[0];
    $sub = ( array_key_exists(1, $_filter) ) ? $_filter[1] : '';

    /*
    $_docs = [
        'allcount' => 0,
        'count' => 4,
        'pages' => 34535,
        'documents' => []
    ];
    */
    // query 'document' post type matching against main and sub categories
    $mappings = [
        'trees' => [
            'field' => 'category_order',
            'category' => 'category-select',
            'subcategory' => 'subcategory-select',
            'documents' => 'documents',
            'document' => 'document-select'
        ],
        'verification' => [
            'field' => 'category_order2',
            'category' => 'category-select2',
            'subcategory' => false,
            'documents' => 'documents2',
            'document' => 'document-select2'
        ],
    ];
    $thisPageDocumentsHandler = new DocumentHandler( $section, $mappings );
    $_docs = $thisPageDocumentsHandler->getOrderedDocumentsByFilterAndPage([ 'main' => $main, 'sub' => $sub ], $filterLevel, $page, $ipp);

    $response = [
        'action' => 'document list paging',
        'status' => 'OK',
        'section' => $section,
        'filterlevel' => $filterLevel,
        'filterlevel2' => $_docs['fl'],
        'filter' => $filter,
        'ipp' => $ipp,
        'main' => $main,
        'sub' => $sub,
        'page' => $page,
        'allcount' => $_docs['allcount'],
        'doccount' => $_docs['count'],
        'pagecount' => $_docs['pages'],
        'documents' => $_docs['documents']
    ];

    echo json_encode( $response );

    exit();
}

// FAQs

add_action('wp_ajax_nopriv_faqfilter', 'filterFAQ');
add_action('wp_ajax_faqfilter', 'filterFAQ');

function filterFAQ(){
    // switch document filter -> return updates list of documents
    /*
    ** CHANGING THE FILTER RESETS PAGING TO PAGE 1
    */

    /*
    paraemeters:
    category
    */

    // query 'document' post type matching against main and sub categories
    $response = [
        'action' => 'filter FAQs§',
        'category' => 'category',
        'faqs' => []
    ];

    echo json_encode( $response );

    exit();
}

// news/blog posts
add_action('wp_ajax_nopriv_filternews', 'filterNews');
add_action('wp_ajax_filternews', 'filterNews');

function filterNews(){
    // * NOTE switching filters resets back to first page

    // featured
    // filter
    // pagesize
    // page

    $filter = $_GET['filter'];
    $featured = $_GET['featured'];
    $pageSize = $_GET['pagesize'];
    $page = $_GET['page'];

    // get post data with image/thumbnail URLs
    $data = getNormalPosts( explode(',',$featured), $filter, $pageSize, $page, true);

    $response = [
        'action' => 'Filter news',
        'status' => 'OK',
        'featured-item' => $featured,
        'filter' => $filter,
        'items-per-page' => $pageSize,
        'page' => $page,
        'pages' => $data['pages'],
        'postcount' => count($data['posts']),
        'ids' => $data['ids'],
        'posts' => $data['posts']
    ];
    echo json_encode( $response );

    exit();
}


add_action('wp_ajax_nopriv_newspaging', 'pagingNews');
add_action('wp_ajax_newspaging', 'pagingNews');

function pagingNews(){
    // page between news items in current category

    // parameters:
    // featured
    // filter
    // pagesize
    // page

    $featured = $_GET['featured'];
    $filter = $_GET['filter'];
    $pageSize = $_GET['pagesize'];
    $page = $_GET['page'];

    // get post data with image/thumbnail URLs
    $data = getNormalPosts( explode(',',$featured), $filter, $pageSize, $page, true);

    $response = [
        'action' => 'Filter news',
        'status' => 'OK',
        'featured-item' => $featured,
        'filter' => $filter,
        'items-per-page' => $pageSize,
        'page' => $page,
        'pages' => $data['pages'],
        'postcount' => count($data['posts']),
        'ids' => $data['ids'],
        'posts' => $data['posts']
    ];

    echo json_encode( $response );

    exit();
}

// footer mailing list signup form 
add_action('wp_ajax_nopriv_mailinglist', 'mailingList');
add_action('wp_ajax_mailinglist', 'mailingList');

function mailingList(){

    $action = $_POST['action'];
    $nonce = $_POST['nonce'];
    $name =     $_POST['name'];
    $email =     $_POST['email'];
    $org =     $_POST['org'];
    $optin =     $_POST['optin'];

    // validate fields
    // non empty, [a-z][A-Z]- '
    // email non-empty valid email address
    // org optional [a-z][A-Z]-.
    // optin non-empty true/yes

    $response = [
        'action' => 'Sign up to mailing list',
        'nonce' => $nonce,
        'name' => $name,
        'email' => $email,
        'org' => $org,
        'optin' => $optin,
        'errors' => ['name','email','optin'],
        'rc' => 'OK'
    ];

    echo json_encode( $response );

    exit();
}

?>