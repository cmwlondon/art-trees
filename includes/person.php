<?php
/*
People
custom posy type: person

permalink

ACF fields:
title
forename
surname
organization (role)
portrait
bio_copy
linkedin
email

person_type 'board-of-directors','sectetariat'
*/

/*
$args = array (
    'posts_per_page' => -1,
    'post__in' => $ids,
    'orderby' => 'post__in' */

// $idorder ARRAY of ids
class PeopleHandler{
    public $people;
    public $group;

    public function __construct() {
    }

    public function getPeopleInOrder( $idOrderedList ){
    }

    public function getPerson( $id ){
    }

    public function getAllPeople( $omit = false){

        $queryArguments = [
            'post_type' => 'person',
            'posts_per_page' => -1
        ];

        if ( $omit ) {
            $queryArguments['post__not_in'] = [$omit];
        }

        $the_query = new WP_Query( $queryArguments );
        $count = $the_query->post_count; 

        $people = [];
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();

                $post_id = get_the_ID();

                $ACFfields = get_fields($post_id);

                $taxonomy = get_the_terms( $post_id, 'person_type' );

                $people[] = [
                    // 'acf' => $ACFfields,
                    'id' => $post_id,
                    'permalink' => get_permalink(),
                    'title' => get_the_title(),
                    'prefix' => ( !empty( $ACFfields['title'] ) ) ? $ACFfields['title'] : '',
                    'forename' => $ACFfields['forename'],
                    'surname' => $ACFfields['surname'],
                    'org' => $ACFfields['organization'],
                    'portrait' => $ACFfields['portrait'], // image ID
                    'bio' => $ACFfields['bio_copy'],
                    'linkedin' => $ACFfields['linkedin'],
                    'email' => $ACFfields['email'],
                    'membership' => [
                        'name' => $taxonomy[0]->name,
                        'slug' => $taxonomy[0]->slug
                    ]
                ];
            }
        }
        wp_reset_postdata();

        $this->people = $people;
        return $people;
    }

    public function getMembersOf( $group, $omit = false ){
    }

}

function getPeopleInOrder( $idorder ){
    $queryArguments = [
		'post_type' => 'person',
	    'post__in' => $idorder,
	    'orderby' => 'post__in'
    ];
    $the_query = new WP_Query( $queryArguments );
    $count = $the_query->post_count; 

    $people = [];
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();

            $post_id = get_the_ID();

            $ACFfields = get_fields($post_id);

            $taxonomy = get_the_terms( $post_id, 'person_type' );

            $people[] = [
            	// 'acf' => $ACFfields,
            	'id' => $post_id,
                'permalink' => get_permalink(),
                'title' => get_the_title(),
                
            	'prefix' => ( !empty( $ACFfields['title'] ) ) ? $ACFfields['title'] : '',
            	'forename' => $ACFfields['forename'],
            	'surname' => $ACFfields['surname'],
            	'org' => $ACFfields['organization'],
            	'portrait' => $ACFfields['portrait'], // image ID
            	'bio' => $ACFfields['bio_copy'],
            	'linkedin' => $ACFfields['linkedin'],
            	'email' => $ACFfields['email'],
            	'membership' => [
            		'name' => $taxonomy[0]->name,
            		'slug' => $taxonomy[0]->slug
            	]
            ];
        }
    }
    wp_reset_postdata();
	return $people;
}

function getPerson( $id ){

    $queryArguments = [
		'post_type' => 'person',
		'p' => $id
    ];
    $the_query = new WP_Query( $queryArguments );
    $count = $the_query->post_count; 

    $person = [];
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();

            $post_id = get_the_ID();

            $ACFfields = get_fields($post_id);

            $taxonomy = get_the_terms( $post_id, 'person_type' );

            $person = [
            	// 'acf' => $ACFfields,
            	'id' => $post_id,
                'permalink' => get_permalink(),
                'title' => get_the_title(),
            	'prefix' => ( !empty( $ACFfields['title'] ) ) ? $ACFfields['title'] : '',
            	'forename' => $ACFfields['forename'],
            	'surname' => $ACFfields['surname'],
            	'org' => $ACFfields['organization'],
            	'portrait' => $ACFfields['portrait'], // image ID
            	'bio' => $ACFfields['bio_copy'],
            	'linkedin' => $ACFfields['linkedin'],
            	'email' => $ACFfields['email'],
            	'membership' => [
            		'name' => $taxonomy[0]->name,
            		'slug' => $taxonomy[0]->slug
            	]
            ];
        }
    }
    wp_reset_postdata();
	return $person;
}

function getAllPeople( $omit = false){

    $queryArguments = [
		'post_type' => 'person',
		'posts_per_page' => -1
    ];

    if ( $omit ) {
    	$queryArguments['post__not_in'] = [$omit];
    }

    $the_query = new WP_Query( $queryArguments );
    $count = $the_query->post_count; 

    $people = [];
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();

            $post_id = get_the_ID();

            $ACFfields = get_fields($post_id);

            $taxonomy = get_the_terms( $post_id, 'person_type' );

            $people[] = [
            	// 'acf' => $ACFfields,
            	'id' => $post_id,
                'permalink' => get_permalink(),
                'title' => get_the_title(),
            	'prefix' => ( !empty( $ACFfields['title'] ) ) ? $ACFfields['title'] : '',
            	'forename' => $ACFfields['forename'],
            	'surname' => $ACFfields['surname'],
            	'org' => $ACFfields['organization'],
            	'portrait' => $ACFfields['portrait'], // image ID
            	'bio' => $ACFfields['bio_copy'],
            	'linkedin' => $ACFfields['linkedin'],
            	'email' => $ACFfields['email'],
            	'membership' => [
            		'name' => $taxonomy[0]->name,
            		'slug' => $taxonomy[0]->slug
            	]
            ];
        }
    }
    wp_reset_postdata();
    return $people;
}

function groupByMembership( $omit = false ) {
	$allPeople = getAllPeople( $omit );

	$groups = [];

	foreach( $allPeople AS $person ) {
		$memberSlug = $person['membership']['slug'];

		if ( !array_key_exists( $memberSlug, $groups ) ) {
			$groups[ $memberSlug ]['name'] = $person['membership']['name'];
			$groups[ $memberSlug ]['members'][] = $person;
		} else {
			$groups[ $memberSlug ]['members'][] = $person;
		}
	}

	return $groups;
}

function getMembersOf( $group, $omit = false ){
	$groups = groupByMembership( $omit );
	return $groups[ $group ];
}

?>
