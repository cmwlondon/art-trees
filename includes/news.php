<?php
/*
-------------------------------------------------------------------------------------
NewsHandler
-------------------------------------------------------------------------------------
*/

class NewsHandler{

    public $posts;
    public $postCount;
    public $catgeories;

    // -----------------------------------

    public function __construct() {
        $this->getAllPosts();
        $this->getAllPostCategories();
    }

    public function getAllPosts(){
        $posts = [];

        $queryArguments = [
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby'   => array(
                'date' => 'DESC'
            )
        ];

        $the_query = new WP_Query( $queryArguments );
        $this->postCount = $the_query->post_count; 

        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();

                $post_id = get_the_ID();

                $thisPostCategories = wp_get_post_categories( $post_id, [] ); // returns category ID
                $cat = get_category( $thisPostCategories[0] ); // get FIRST category
                // $cat->slug 'document-update'
                // $cat->name 'document update'
                $acf = get_fields($post_id);

                $thumbnailID = ( !empty($acf['thumbnail']) ) ? $acf['thumbnail'] : false;
                $thumbnail = ( $thumbnailID ) ? wp_get_attachment_image_src($thumbnailID, 'full') : false;

                $imageID = ( !empty($acf['image']) ) ? $acf['image'] : false;
                $image = ( $imageID ) ? wp_get_attachment_image_src($imageID, 'full') : false;

                // $acf['has_link'] 'no', 'yes'
                // $acf['link_url'] text REQUIRED if $acf['has_link'] === 'yes'
                // $acf['external_link'] 'no', 'yes'
                $hasExternalLink = ($acf['has_link'] === 'yes' ); 
                $externalLink = ($hasExternalLink) ? $acf['external_link'] : 'no';
                $linkURL = ($hasExternalLink) ? $acf['link_url'] : '';
                // 'external_link' => $acf['external_link'],
                // 'link_url' => $acf['link_url'],

                $posts[] = [
                    'id' => $post_id,
                    // 'acf' => $acf,
                    'title' => $acf['displayed_title'],
                    'synopsis' => $acf['synopsis'],
                    'source' => $acf['source'],
                    'thumbnail' => $thumbnail,
                    'image' => $image,
                    'caption' => ( array_key_exists( 'caption', $acf) ) ? $acf['caption'] : '',
                    'catslug' => $cat->slug,
                    'category' => $cat->name,
                    'date' => get_the_date("F, Y", $post_id), // month name, 4 digit year
                    'rawdate' => get_the_date("d/m/Y", $post_id), // dd/dd/yyyy
                    'permalink' => get_permalink($post_id),
                    'has_link' => $acf['has_link'],
                    'external_link' => $externalLink,
                    'link_url' => $linkURL,
                    'attachments' => ( !empty( $acf['attachments'] ) ) ? $acf['attachments'] : 'none' 
                ];
            }
        }
        wp_reset_postdata();
        $this->posts = $posts;
        return $posts;
    }

    // -----------------------------------

    public function getNLatestPosts( $omitIDList = false, $count = 4 ){
        $pool = $this->posts;
        $n = 0;
        $postCount = 0;
        $selectedPosts = [];
        while ( $n < count($pool) && $postCount < $count ) {
            $_post = $pool[$n];
            if ( !in_array( $_post['id'], $omitIDList ) ){
                $selectedPosts[] = $_post;
                $postCount++;
            }
            $n++;
        }
        return $selectedPosts;
    }

    // -----------------------------------

    public function getPosts($omitIDList = false, $filter, $pageSize, $page = 1, $getImagePaths = false) {
    }

    // -----------------------------------

    public function getPostByID( $id ) {

        foreach ( $this->posts AS $post ) {
            if ( (int) $post['id'] === (int) $id) {
                return $post;
            }
        }
        return false;
    }
    // -----------------------------------

    public function getPostsByCategory( $category, $omitIDList) {
        // category
        // $omitIDList
    }

    public function getNextAndPreviousPermalinks( $id ) {
        $firstPost = false;
        $lastPost = false;
        $nextPost = false;
        $prevPost = false;

        foreach ( $this->posts AS $index => $post) {
            if( (int) $id === (int) $post['id']) {
                // index 0 Latest post
                $firstPost = ( $index === 0 );
                $nextPost = ( !$firstPost ) ? $this->posts[ $index - 1]['permalink'] : false;

                // index N oldest post
                $lastPost = ( $index === ($this->postCount - 1) );
                $prevPost = ( !$lastPost ) ? $this->posts[ $index + 1]['permalink'] : false;
            }
        }
        return [
            'firstPost' => $firstPost,
            'lastPost' => $lastPost,
            'nextPost' => $nextPost,
            'prevPost' => $prevPost
        ];
    }

    // -----------------------------------

    public function getAllPostCategories() {
        $categories = [];
        $categorySlugs = [];

        foreach ( $this->posts AS $post) {
            if( !in_array( $post['catslug'], $categorySlugs) ) {
                $categorySlugs[] = $post['catslug'];
                $categories[ $post['catslug'] ] = $post['category'];
            }
        }

        $this->categories = $categories;
    }

    // -----------------------------------

}

function getAllPostsForCategory($category, $idList = null) {
    // get number of posts for specified category

    /*
    parameters:
    category
    page
    featured - get featured post id omit this id from queries
    */

    $count = 0;
    $queryArguments = [
      'post_type' => 'post'
    ];

    if ( !is_null($idList) ) {
        $queryArguments['post__not_in'] = $idList;
    }

    if ( $category !== 'all' ) {
        $queryArguments['tax_query'] = array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => array( $category ),
            )
        );
    }

    $the_query = new WP_Query( $queryArguments );
    $count = $the_query->post_count; 
    wp_reset_postdata();

    return $count;
}

function getAllPostCategories() {

    $queryArguments = [
        'post_type' => 'post',
        'posts_per_page' => -1,
        'post_status'=> 'publish',
        'ignore_sticky_posts' => true  //**
    ];

    $the_query = new WP_Query( $queryArguments );
    $count = $the_query->post_count; 

    $categories1 = [];
    $categories2 = [];
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();

            $post_id = get_the_ID();

            $thisPostCategories = wp_get_post_categories( $post_id, [] ); // returns category ID
            $cat = get_category( $thisPostCategories[0] ); // get FIRST category
            // $cat->slug 'document-update'
            // $cat->name 'document update'
            if ( !in_array($cat->name, $categories1) ) {
                $categories1[] = $cat->name;
                $categories2[] = [
                    'name' => $cat->name,
                    'slug' => $cat->slug
                ];
            }
        }
    }
    wp_reset_postdata();

    return [
        'posts' => $count,
        'categories' => $categories2
    ];
}

function getNormalPosts($featured = false, $filter, $pageSize, $page = 1, $getImagePaths = false) {

    $posts = [];
    $ids = [];
    $pageCount = 0;

    $queryArguments = [
        'post_type' => 'post',
        'posts_per_page' => $pageSize,
        'paged' => $page,
        'post_status'=> 'publish',
        'ignore_sticky_posts' => true  //**
    ];

    if ( $featured ) {
       $queryArguments['post__not_in'] = $featured;
    } 

    if ( $filter !== 'all' ) {
        $queryArguments['tax_query'] = array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => array( $filter ),
            )
        );
    }

    $the_query = new WP_Query( $queryArguments );
    // $count = $the_query->post_count; 
    $pageCount = $the_query->max_num_pages;

    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();

            $post_id = get_the_ID();
            $thisPostCategories = wp_get_post_categories( $post_id, [] ); // returns category ID
            $cat = get_category( $thisPostCategories[0] ); // get FIRST category

            $post_id = get_the_ID();
            $ids[] = $post_id;

            $acf = get_fields($post_id);
            // [displayed_title] => TREES 2.0 Public Consultation Begins
            // [synopsis] => Lorem Ipsum
            // [source] => ARTTrees
            // [thumbnail] => 75
            // [image] => 292
            // [has_link] => no
            // [external_link] => yes
            // [link_url] => https://www.icao.int/environmental-protection/CORSIA/Pages/TAB.aspx
            // [attachments] => 

            $newPost = [
                'id' => $post_id,
                // 'title' => get_the_title($post_id),
                'title' => $acf['displayed_title'],
                'synopsis' => $acf['synopsis'],
                'source' => $acf['source'],
                'source' => $acf['source'],
                'category' => $cat->name,
                'date' => get_the_date("F, Y", $post_id), //
                'rawdate' => get_the_date("d/m/Y", $post_id), // dd/dd/yyyy
                'permalink' => get_permalink($post_id),
                'has_link' => $acf['has_link'],
                'attachments' => ( !empty( $acf['attachments'] ) ) ? $acf['attachments'] : 'none' 
            ];

            if ( $getImagePaths ) {
                // get image URLs
                // $acf['thumbnail']
                // $acf['image']

                $thumbnailPath = '';
                $tw = 0;
                $th = 0;
                $ta = 0;

                $thumbnailID = ( !empty($acf['thumbnail']) ) ? $acf['thumbnail'] : false;
                if ( $thumbnailID ) {
                    $thumbnail = wp_get_attachment_image_src($thumbnailID, 'full');
                    // 0 URL
                    // 1 width
                    // 2 height
                    $thumbnailPath = $thumbnail[0];
                    $tw = $thumbnail[1];
                    $th = $thumbnail[2];

                    // check to see width is not zero, WP returns zero for width and height for SVG files, leading to a divide by zero error
                    $ta = ( $tw > 0) ? $th / $tw : 9999.0; // SVG image, force 'scale by height'
                }

                $newPost['thumbnail'] = $thumbnailPath;
                $newPost['tw'] = $tw;
                $newPost['th'] = $th;
                $newPost['ta'] = $ta;
            } else {
                // get image IDs
                // $image = $acf['image'];
                $newPost['thumbnail'] = $acf['thumbnail'];
            }


            if ( $acf['has_link'] === 'yes' ) {
                $newPost['external_link'] = $acf['external_link'];
                $newPost['link_url'] = $acf['link_url'];
            }

            $posts[] = $newPost;
        }
    }
    wp_reset_postdata();

    return [
        '$featured' => $featured,
        'filter' => $filter,
        'pages' => $pageCount,
        'page' => $page,
        'ids' => $ids,
        'posts' => $posts
    ];
}

?>