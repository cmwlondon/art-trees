<?php
// add HTML title tag to page header via common header header.php
function theme_slug_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );

// add non breaking space to last two words in block of text
function word_wrapper($text,$minWords = 3) {
   $return = trim($text);
   $arr = explode(' ',$text);
   if(count($arr) >= $minWords) {
           $arr[count($arr) - 2].= '&nbsp;'.$arr[count($arr) - 1];
           array_pop($arr);
           $return = implode(' ',$arr);
   }
   return $return;
}
?>