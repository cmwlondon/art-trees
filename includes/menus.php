<?php
// set up header and footer menus in admin
function arttree2021_menus() {

   $locations = array(
      'primary'  => __( 'Main navigation', 'ARTTree-2021' ),
      'footer' => __( 'Footer links', 'ARTTree-2021' ),
   );

   register_nav_menus( $locations );
}

add_action( 'init', 'arttree2021_menus' );
?>