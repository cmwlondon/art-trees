<?php
// add scripts to pages
add_action( 'wp_enqueue_scripts', 'mbastack_enqueue_scripts' );
function mbastack_enqueue_scripts()
{
    // https://developer.wordpress.org/reference/functions/wp_enqueue_script/#usage
    // wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
    
    /*
    wp_register_script(
        'classes', // Handle
        get_stylesheet_directory_uri() . '/assets/js/classes.min.js' // File url
    );
    wp_register_script(
        'default', // Handle
        get_stylesheet_directory_uri() . '/assets/js/default.min.js' // File url
    );
    */

    wp_enqueue_script( 'classes', get_stylesheet_directory_uri() . '/assets/js/classes-dist.js' ,[], '', false );
    wp_enqueue_script( 'default', get_stylesheet_directory_uri() . '/assets/js/default-dist.js' ,[], '', false );

    wp_localize_script( 
        'default', 
        'localize_vars', 
        array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' ), // add AJAX endpoints
            'url' => get_stylesheet_directory_uri(),
            'nonce' => wp_create_nonce('ajax-nonce')
            // 'path' => get_stylesheet_directory()
        ) 
    );

    /*
    // find out how to attach script based on template/page
    wp_register_script(
        'home', // Handle
        get_stylesheet_directory_uri() . '/assets/js/pages/home.min.js' // File url
    );
    wp_enqueue_script( 'home' );
    */
}
?>