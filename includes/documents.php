<?php
/*
-------------------------------------------------------------------------------------
Documents
-------------------------------------------------------------------------------------

new DocumentHandler( false )
false -> get all documents
'trees' -> get documents for trees section
'verification' -> get documents for verification section
*/

// webinar_caption
// webinar_link

class DocumentHandler{

	public $mainList;
	public $subList;

	public $heirarchy;

	public $documents;

	// -----------------------------------

    public function __construct( $section = false, $mappings ) {
 
    	$this->section = $section;
    	$this->mappings = $mappings;

    	// get all documents
    	$this->allDocuments();

    	// get category structure
    	$this->documentCategories();
    }

	// -----------------------------------

	public function getTypeAndSize( $filename, $size ) {
		$documentType = preg_replace("#\?.*#", "", pathinfo($filename, PATHINFO_EXTENSION));

		if ( $size > 1000000 ) {
			$suffix = 'Mb';
			$fileSizeNice = $size / 1000000;
		}
		if ( $size > 1000 && $size < 1000000) {
			$suffix = 'Kb';
			$fileSizeNice = $size / 1000;
		}
		if ( $size < 1000 ) {
			$suffix = 'bytes';
			$fileSizeNice = $size;
		}

		$documentSize = number_format($fileSizeNice,1).$suffix;

		return [
			'type' => $documentType,
			'size' => $documentSize
		];
	}

	// -----------------------------------

	public function normalizeSortOrder(  ) {

		$mapping = $this->mappings[$this->section];

		$this->sortOrder = ( !empty(get_field( $mapping['field'],'option')) ) ? get_field($mapping['field'],'option') : false;

		switch( $this->section ) {
			case 'trees' : {
				
			} break;

			case 'verification' : {
				// map theme settings verification field names to tree field names

				$tempOrder = [];
				foreach ( $this->sortOrder AS $category ) {
					$_doucuments = [];

					if ( count($category[$mapping['documents']]) > 0 ) {
						foreach ( $category[$mapping['documents']] AS $document ) {
							$_doucuments[] = [
								'document-select' => $document[$mapping['document']]
							];
						}
					}

					$tempOrder[] = [
						'category-select' => $category[$mapping['category']],
						'documents' => $_doucuments
					];
				}
				
				$this->sortOrder = $tempOrder;
			} break;
		}

		return $this->sortOrder;
	}

	// -----------------------------------

	public function documentTaxonomy( $taxonomy ){
	    $main = [];
	    $sub = [];
	    if ( !empty( $taxonomy ) ) {
			foreach($taxonomy AS $term){
				if ($term->parent === 0){
					$main = [
						'name' => $term->name,
						'slug' => $term->slug
					];
				} else {
					$sub = [
						'name' => $term->name,
						'slug' => $term->slug
					];
				}
			}
	    }

	    return [
	    	'main' => $main,
	    	'sub' => $sub,
	    ];
	}

	// -----------------------------------

	public function getMain( $slug ) {
		$category = [];
		$main = $this->mainList;

		/*
		returns:
	    [
	        [name] => 'Summaries & Guidance'
	        [slug] => 'summaries-guidance'
	    ]
		*/
		return $main[ $slug ];
	}

	// slug is composite of parent (main) and subcatgeory slugs seperated by comma
	// MAIN-SLUG,SUB-SLUG
	// summaries-guidance,papers-visual-guides
	public function getSub( $slug ) {
		$category = [];
		
		$sub = $this->subList;
		/*
		returns:
	    [
	        [slug] => 'papers-visual-guides'
	        [name] => 'Papers and Visual Guides'
	        [parent] => [
	            [name] => 'Summaries & Guidance'
	            [slug] => s'ummaries-guidance'
	        ]
	    ]
	  	*/
		return $sub[ $slug ];
	}

	// -----------------------------------

	public function allDocuments() {

	    $queryArguments = [
			'post_type' => 'document',
			'posts_per_page' => -1
	    ];

		// check $this->section
		// if not false query section against tags 'trees','verification'
	    // $this->section = 'verification';
	    if ( $this->section ) {
	    	$queryArguments['tax_query'] = array(
				array(
					'taxonomy' => 'document_attribute',
					'field' => 'slug',
					'terms' => $this->section
				)
			);
	    }

	    $the_query = new WP_Query( $queryArguments );
	    $count = $the_query->post_count; 

	    $documents = [];
	    if ( $the_query->have_posts() ) {
	        while ( $the_query->have_posts() ) {
	            $the_query->the_post();

	            $post_id = get_the_ID();

	            $documents[] = $this->documentProperties( $post_id );

	        }
	    }

	    wp_reset_postdata();

		$this->documents = $documents;
	}	

	// -----------------------------------

	public function getDocumentByID( $id ) {
		foreach ( $this->documents AS $document ) {
			if ( (int) $document['id'] === (int) $id ){
				return $document;
			}
		}
		return false;
	}

	// -----------------------------------

	public function documentProperties( $documentID ) {
	    // $thisPostCategories = wp_get_post_categories( $post_id, [] ); // returns category ID
	    // $cat = get_category( $thisPostCategories[0] ); // get FIRST category
	    // $cat->slug 'document-update'
	    // $cat->name 'document update'

	    $ACFfields = get_fields($documentID);
	    /*
	    title
	    cover_image
	    icon
	    translation[]
	    */
	    $availableTranslations = [];
	    foreach( $ACFfields['translation'] AS $translation ) {

			$_filename = $translation['file']['filename'];
			$_filesize = $translation['file']['filesize'];
			$documentURL = $translation['file']['url'];

			$documentProperties = $this->getTypeAndSize( $_filename, $_filesize );

	    	$availableTranslations[] = [
	    		'language' => ucfirst($translation['language']),
	    		'url' => $documentURL,
	    		'size' => $documentProperties['size'],
	    		'type' => $documentProperties['type']
	    	];
	    }
	    $taxonomy = get_the_terms( $documentID, 'document_type' );

	    $documentcategories = $this->documentTaxonomy( $taxonomy );

	    $_maincat = ( array_key_exists('slug', $documentcategories['main']) ) ?$documentcategories['main'] : ['slug' => 'nocat', 'name' => 'NO CATEGORY'];
	    $_subcat = ( array_key_exists('slug', $documentcategories['sub']) ) ?$documentcategories['sub'] : ['slug' => 'nocat', 'name' => 'NO CATEGORY'];

		$webinarCaption = ( !empty($ACFfields['webinar_caption']) ) ? $ACFfields['webinar_caption'] : false;
		$webinarLink = ( !empty($ACFfields['webinar_link']) ) ? $ACFfields['webinar_link'] : false;

	    return [
	    	'title' => $ACFfields['title'], // string
	    	'filter' => $_maincat['slug'].','.$_subcat['slug'],
	    	'main' => $_maincat, // string
	    	'sub' => $_subcat, // string
	    	'cover' => $ACFfields['cover_image'], // image ID integer
	    	'icon' => $ACFfields['icon'], // image ID integer
	    	'webinarcaption' => $webinarCaption,
	    	'webinarlink' => $webinarLink,
	    	'translations' => $availableTranslations, // array
	    	'id' => $documentID // post ID integer
	    ];
	}

	// -----------------------------------

	public function documentCategories() {

		// iterate over documents and get all main and subcategories
		// stored in ACF fields for each item of post type 'document'
		$allDocs = $this->documents;

		$docCat = [];
		$mainCategories = [];
		$allSub = [];

		foreach( $allDocs AS $doc ) {
			$_main = $doc['main'];
			$_sub = $doc['sub'];


			if ( array_key_exists( 'slug', $_main ) ) {
				// documents has main category

				// build array of available main categories
				if ( !array_key_exists($_main['slug'], $mainCategories ) ) {
					// add new main category
					$mainCategories[$_main['slug']] = $_main;
				}

				// build 
				if ( !array_key_exists($_main['slug'], $docCat ) ) {
					// main does not exist
					// add new main
					$docCat[ $_main['slug'] ]['main'] = $_main;		

					// main exists now, check for existing sub
					if( array_key_exists( 'slug', $_sub ) ) {
						$docCat[ $_main['slug'] ]['sub'][ $_sub['slug'] ] = $_sub;	

						// collect list of all sub categpories
						$mainsubKey = $_main['slug'].','.$_sub['slug'];
						if ( !array_key_exists($mainsubKey, $allSub ) ) {
							// add new main category
							$allSub[ $mainsubKey ] = [
								'slug' => $_sub['slug'],
								'name' => $_sub['name'],
								'parent' => $_main
							];
						}
					}
				} else {
					// main exists, check for existing sub
					if( !array_key_exists( $_sub['slug'], $docCat[ $_main['slug'] ]['sub'] ) ) {
						$docCat[ $_main['slug'] ]['sub'][ $_sub['slug'] ] = $_sub; 
					}

					// collect list of all sub categpories
					$mainsubKey = $_main['slug'].','.$_sub['slug'];
					if ( !array_key_exists($mainsubKey, $allSub ) ) {
						// add new main category
						$allSub[ $mainsubKey ] = [
							'slug' => $_sub['slug'],
							'name' => $_sub['name'],
							'parent' => $_main
						];
					}
				}
			} else {
				// no main category
				$mainCategories[$_main['slug']] = [
					'slug' => 'nocat',
					'name' => 'NO CATEGORY'
				];

				$docCat['nocat'] = [
					'slug' => 'nocat',
					'name' => 'NO CATEGORY'
				];
			}
		}

		// move nocat main category ot end of maincat and heierarchy arrays $mainCategories
		// main category list
		$isCategorised = array_filter($mainCategories, function($key) { return $key !== 'nocat'; }, ARRAY_FILTER_USE_KEY);
		$notCategorised = array_filter($mainCategories, function($key) { return $key === 'nocat'; }, ARRAY_FILTER_USE_KEY);
		$mainCategories = array_merge( $isCategorised, $notCategorised );

		// subcategory list
		$isCategorised = array_filter($allSub, function($key) { return $key !== 'nocat'; }, ARRAY_FILTER_USE_KEY);
		$notCategorised = array_filter($allSub, function($key) { return $key === 'nocat'; }, ARRAY_FILTER_USE_KEY);
		$allSub = array_merge( $isCategorised, $notCategorised );

		// hierarchy $docCat
		$isCategorised = array_filter($docCat, function($key) { return $key !== 'nocat'; }, ARRAY_FILTER_USE_KEY);
		$notCategorised = array_filter($docCat, function($key) { return $key === 'nocat'; }, ARRAY_FILTER_USE_KEY);
		$docCat = array_merge( $isCategorised, $notCategorised );

		// move nocat to end of sub blocks
		foreach( $docCat AS $mainSlug => $mainCat ) {
			$_sub = $mainCat['sub'];
			$isCategorised = array_filter($_sub, function($key) { return $key !== 'nocat'; }, ARRAY_FILTER_USE_KEY);
			$notCategorised = array_filter($_sub, function($key) { return $key === 'nocat'; }, ARRAY_FILTER_USE_KEY);
			$_sub = array_merge( $isCategorised, $notCategorised );

			$docCat[$mainSlug]['sub'] = $_sub;
		}

		$this->heirarchy = $docCat;
		$this->mainList = $mainCategories;
		$this->subList = $allSub;

	}

	// -----------------------------------

	public function getDocumentsByFilter($filter, $omitList = [] ) {
		$main = $filter['main'];
		$sub = $filter['sub'];

		$docs = [];
		foreach ( $this->documents AS $document ) {
			if ( $document['main']['slug'] === $main && $document['sub']['slug'] === $sub ) {
				if ( count($omitList) > 0 ) {
					// check to see ifdocument is in omitlist
					if ( !in_array( $document['id'], $omitList ) ) {
						$docs[] = $document;
					}
				} else {
					$docs[] = $document;
				}
			}
		}
		return $docs;
	}

	// -----------------------------------

	public function getDocumentsBySubcategory( $subCategory ) {
		$subCategorySelect = explode(',', $subCategory['subcategory-select'] );
		$main = $subCategorySelect[0];
		$sub = $subCategorySelect[1];
		$docs = [];
		$omitList = [];

		// check for specified documents
		if (  !empty( $subCategory['documents'] ) ) {
			$thisSubDocs = $subCategory['documents'];
			if ( count( $thisSubDocs ) > 0 ){

				foreach ( $thisSubDocs AS $document ) {
					$id = $document['document-select'];
					$omitList[] = $id;
					$docs[] = $this->documentProperties( $id );
				}
			}
		}

		// are there any more documents in this subcategory not listed undeer specified?
		$docs = array_merge( $docs, $this->getDocumentsByFilter( ['main' => $main, 'sub' => $sub ], $omitList ) );

		return $docs;
	}

	// -----------------------------------

	public function getOrderedDocumentsByFilterAndPage($filter, $filterLevel, $page = 1, $ipp = 4) {
		$_subs = [];
		$_docs = [];
		$_allFilteredDocuments = [];
		$thisPageDocCount = 0;
		$main = $filter['main'];
		$sub = $filter['sub'];

		$this->$filterLevel = $filterLevel;

		$hierarchicalMode = empty( $sub );

		// 'trees' section sort order specified  in Theme Settings page in admin
		// $sortOrder = ( $this->section && $this->section === 'trees' ) ? get_field('category_order','option') : [];
		$sortOrder = $this->normalizeSortOrder( $this->section );
		
		// $sortOrder = ( !empty( $sortOrder ) ) ? $sortOrder : [];
		// default to first
		$currentBlockSlug = $main;
		
		foreach ( $sortOrder AS $mainBlock ) {
			if ( $mainBlock['category-select'] === $main ) {
				$currentBlock = $mainBlock;
				// $currentBlockSlug = $main;
				break;
			}
		}

		if ( $filterLevel === 'mainsub' ) {
			$isMainOnly = ( empty( $sub ) );
			// if ( $isMainOnly && $filterLevel !== 'mainonly' ) {
			if ( $isMainOnly ) {

				// ----------------------------------------------------------------------------------			

				// only main category 
				// hierarchical mode
				$mode = 'hierarchy';
				$docs = [];

				// list subcategpories based on specified subcats, followed by unspecified
				$subcategories = ( !empty( $currentBlock['subcategories'] ) ) ? $currentBlock['subcategories'] : [];
				$specifiedSubCatCount = count( $subcategories );
				$unSpecifiedSubCat = $this->heirarchy[$currentBlockSlug]['sub'];

				if ( $specifiedSubCatCount === 0 ) {
					// no subcategpories specified, so no documents specified either

					foreach ( $unSpecifiedSubCat AS $subSlug => $sub ) {
						$catDocs = $this->getDocumentsByFilter(['main' => $currentBlockSlug, 'sub' => $subSlug ]);
						$docs = array_merge( $docs, $catDocs);
					}
				}

				// some specified aubcategories
				if ( $specifiedSubCatCount > 0 && $specifiedSubCatCount < count($unSpecifiedSubCat) ) {

					$specSlugs = [];
					// specified categories
					foreach ( $subcategories AS $ssc ) {
						$sscFilters = explode( ',', $ssc['subcategory-select'] );
						$sscMain = $sscFilters[0];
						$sscSub = $sscFilters[1];

						$specSlugs[] = $sscSub;
						$specDocs = [];
						$unspecDocs = [];
						$specDocIDs = [];
						$thisSubCategoryDocuments = ( !empty( $ssc['documents'] ) ) ? $ssc['documents'] : [];
						if ( count($thisSubCategoryDocuments) > 0 ) {
							// documents specified
							// get specified documents
							foreach( $thisSubCategoryDocuments AS $document ) {
								$documentID = $document['document-select'];
								$specDocs[] = $this->getDocumentByID( $documentID );
								$specDocIDs[] = $documentID;
							}
					
							$unspecDocs = $this->getDocumentsByFilter(['main' => $sscMain, 'sub' => $sscSub ], $specDocIDs);

							$docs = array_merge( $specDocs, $unspecDocs);
						} else {
							// documents not specified
							$docs = $this->getDocumentsByFilter(['main' => $sscMain, 'sub' => $sscSub ]);
						}
						
					}

					$unSpecDocs = [];
					foreach ( $this->heirarchy[$main]['sub'] AS $unSpecSubSlug => $unSpecSub ) {
						if ( !in_array( $unSpecSubSlug, $specSlugs)) {
							$vx = $this->getDocumentsByFilter(['main' => $sscMain, 'sub' => $unSpecSubSlug ]);
							$unSpecDocs = array_merge( $unSpecDocs, $vx);
						}
					}
					$docs = array_merge( $docs, $unSpecDocs );
				}

				/*
				$yexon = $this->getDocumentsByFilter(['main' => $sscMain, 'sub' => $sscSub ]);
				echo "<p>yexon ".count( $yexon )."</p>";
				echo "<pre>".print_r( $yexon, true )."</pre>";
				*/

				// all specified subcatgeories
				if ( $specifiedSubCatCount === count($unSpecifiedSubCat) ) {
					echo "<p>550</p>";

					// order subcategories by $specifiedSubCat
					$specSlugs = [];
					// specified categories
					foreach ( $subcategories AS $ssc ) {
						
						$sscFilters = explode( ',', $ssc['subcategory-select'] );
						$sscMain = $sscFilters[0];
						$sscSub = $sscFilters[1];
						$specSlugs[] = $sscSub;
						$thisSubCategoryDocuments = ( !empty( $ssc['documents'] ) ) ? $ssc['documents'] : [];
						if ( count($thisSubCategoryDocuments) > 0 ) {
							// documents specified

							// get specified documents
							$specDocs = [];
							$unspecDocs = [];
							$specDocIDs = [];

							foreach( $thisSubCategoryDocuments AS $document ) {
								$documentID = $document['document-select'];
								$specDocs[] = $this->getDocumentByID( $documentID );
								$specDocIDs[] = $documentID;
							}
							$unspecDocs = $this->getDocumentsByFilter(['main' => $sscMain, 'sub' => $sscSub ], $specDocIDs);
							$docs = array_merge( $specDocs, $unspecDocs );
						} else {
							// documents not specified
							$unspecDocs = $this->getDocumentsByFilter(['main' => $sscMain, 'sub' => $sscSub ]);
							$docs = array_merge( $docs, $unspecDocs );
						}
					}
				}

				$_allFilteredDocuments = $docs;
				$docCount = count( $_allFilteredDocuments );
				$pages = ( $docCount % $ipp !== 0) ? floor($docCount / $ipp) + 1 : floor($docCount / $ipp);
				$page = ( $page > $pages || $page < 1 ) ? 1 : $page;
				$offset = ($page - 1) * $ipp;

				// rearrange docs into hierarchy
				$docHierarchy = [];
				foreach( $_allFilteredDocuments AS $doc ) {
					if ( array_key_exists( 'sub', $doc ) && array_key_exists( 'slug', $doc['sub'] ) ) {
						$_sub = $doc['sub']['slug'];

						if ( !array_key_exists($_sub, $docHierarchy) ) {
							$docHierarchy[$_sub]['name'] = $doc['sub']['name'];
						}
						$docHierarchy[$_sub]['docs'][] = $doc;
					}
				}

				// at this point $docHierarchy contains all documents under $main category in hierarchical structure

				// find 'nocat' category and move to end of list
				$isCategorised = array_filter($docHierarchy, function($key) { return $key !== 'nocat'; }, ARRAY_FILTER_USE_KEY);
				$notCategorised = array_filter($docHierarchy, function($key) { return $key === 'nocat'; }, ARRAY_FILTER_USE_KEY);
				$docHierarchy = array_merge( $isCategorised, $notCategorised );

				// paging needs to be handled differently as the heirarchical structure changes the order of items
				// index docs in hierarchical array
				$index = [];
				foreach( $docHierarchy AS $major => $iSub1 ) {
					foreach( $iSub1['docs'] AS $minor => $iSub2 ) {
						$index[] = [ $major, $minor ];
					}
				}

				// clip index based on items-per-page, page
				$index = array_slice( $index, $offset, $ipp);
				$thisPageDocCount = count($index);

				// build new versions of $docs based on clipper $index
				$nd = [];
				$thisPageDocCount = 0;
				foreach ( $index AS $indexItem ) {
					$nd[ $indexItem[0] ]['name'] = $docHierarchy[ $indexItem[0] ]['name']; 
					$nd[ $indexItem[0] ]['docs'][] = $docHierarchy[ $indexItem[0] ]['docs'][ $indexItem[1] ];
					$thisPageDocCount++;
				}
				$_docs = $nd;

				$thisPageDocCount = count($_docs);

				// ----------------------------------------------------------------------------------			

			} else {

				// ----------------------------------------------------------------------------------			

				// main and sub
				// flat mode
				$mode = 'flat';
				$docs = [];

				// filter for unspecified documents
				$unspecifiedSubCat = $this->heirarchy[$currentBlockSlug]['sub'][$sub];

				$allDocs = $_docs = $this->getDocumentsByFilter( $filter );

				$specifiedSubCats = (!empty($currentBlock['subcategories'])) ? $mainBlock['subcategories'] : [];

				if ( count( $specifiedSubCats ) > 0 ) {
					// find correct subcategory
					foreach ( $specifiedSubCats AS $subcat ) {
						$subcatSlug = explode( ',', $subcat['subcategory-select'] )[1];
						if ( $subcatSlug === $sub ) {
							$currentSubCat = $subcat;
						}
					}

					$subCategoryDocuments = ( !empty($currentSubCat['documents']) ) ? $currentSubCat['documents'] : [];
					$subCategoryDocumentCount = count( $subCategoryDocuments );

					if ( $subCategoryDocumentCount === 0 ) {
						// no specified documents
						$docs = $allDocs;
					}

					if ( $subCategoryDocumentCount > 0 && $subCategoryDocumentCount < count($allDocs) ) {
						// ordered docs, followed by unordered
						$inDocs = [];
						$inDocIDs = [];
						$outDocs = [];

						foreach ( $subCategoryDocuments AS $index => $doc ) {
							$docID = $doc['document-select'];
							$inDocs[] = $this->getDocumentByID( $docID );
							$inDocIDs[] = $docID;
						}

						foreach ( $allDocs AS $doc ) {
							if ( !in_array( $doc['id'], $inDocIDs ) ) {
								$outDocs[] = $doc;
							}
						}

						$docs = array_merge( $inDocs, $outDocs );
					}

					if ( $subCategoryDocumentCount === count($allDocs) ) {
						// all documents in subcategory have been listed in theme settings page

						foreach ( $subCategoryDocuments AS $index => $doc ) {
							$docID = $doc['document-select'];
							$docs[] = $this->getDocumentByID( $docID );
						}

					}
				} else {
					// sub category specified
					$docs = $allDocs;

				}

				$_allFilteredDocuments = $docs;
				$docCount = count( $_allFilteredDocuments );
				$pages = ( $docCount % $ipp !== 0) ? floor($docCount / $ipp) + 1 : floor($docCount / $ipp);
				$page = ( $page > $pages || $page < 1 ) ? 1 : $page;
				$offset = ($page - 1) * $ipp;

				$_docs = array_slice( $_allFilteredDocuments, $offset, $ipp);
				$thisPageDocCount = count($_docs);

				// ----------------------------------------------------------------------------------			

			}

			return [
				// 'currentBlock' => $currentBlock,
				// 'subs' => $_subs,
				'fl' => $filterLevel,
				'mode' => $mode,
				'documents'	=> $_docs,
				'allcount' => count($_allFilteredDocuments),
				'count' => ( $thisPageDocCount < $ipp ) ? $thisPageDocCount : $ipp,
				'pages' => $pages,
				'page' => $page
			];

		} else {

			$allDocs = $this->getDocumentsByFilter( [ 'main' => $main, 'sub' => 'nocat' ] );

			$docs = [];
			// documents specified for current main category
			if ( count($currentBlock['documents']) > 0 ) {
				// build ordered documents
				$specID = [];
				foreach( $currentBlock['documents'] AS $docItem){
					$specID[] = $docItem['document-select'];
				}

				// attach remainder to end
				$unspecID = [];
				foreach( $allDocs AS $docItem){
					if ( !in_array( $docItem['id'], $specID)) {
						$unspecID[] = $docItem['id'];	
					}
					
				}
			} else {
				// return all docs with no ordering 
				$docs = $allDocs;
			}

			/*
			$allDocs = $this->documents;			

			// specified documents
			$unspecified = [];

			// index available IDS
			$allDocIDs = [];
			foreach ( $allDocs AS $doc) {
				$allDocIDs[] = $doc['id'];
			}
			echo "<p>$allDocIDs:</p>";
			echo "<pre>".print_r($allDocIDs, true)."</pre>";

			// specified dpcs
			$specified = [];
			foreach ( $currentBlock['documents'] AS $doc) {
				$specified[] = 
			}
			*/
			/*
			[
				[
					[title] => TREES Validation and Verification Conflict of Interest Form
		            [filter] => standard-and-templates,nocat
		            [main] => Array
		                (
		                    [name] => Standard & Templates
		                    [slug] => standard-and-templates
		                )

		            [sub] => Array
		                (
		                    [slug] => nocat
		                    [name] => NO CATEGORY
		                )

		            [cover] => 
		            [icon] => 
		            [translations] => Array
		                (
		                    [0] => Array
		                        (
		                            [language] => English
		                            [url] => http://arttree.local/wp-content/uploads/2021/12/TREES-Conflict-of-Interest-Form-FINAL.docx
		                            [size] => 197.6Kb
		                            [type] => docx
		                        )

		                )

		            [id] => 1464
				]
				...
			]
			*/



			// documents in this category
			// sport order

			$mode = 'flat';
			// $_allFilteredDocuments = [];
			// $thisPageDocCount = 1;
			// $pages = 1;
			// $page = 1;

			$_allFilteredDocuments = $docs;
			$docCount = count( $_allFilteredDocuments );
			$pages = ( $docCount % $ipp !== 0) ? floor($docCount / $ipp) + 1 : floor($docCount / $ipp);
			$page = ( $page > $pages || $page < 1 ) ? 1 : $page;
			$offset = ($page - 1) * $ipp;

			$_docs = array_slice( $_allFilteredDocuments, $offset, $ipp);
			$thisPageDocCount = count($_docs);

			return [
				'fl' => $filterLevel,
				'mode' => $mode,
				'documents'	=> $_docs,
				'allcount' => count($_allFilteredDocuments),
				'count' => ( $thisPageDocCount < $ipp ) ? $thisPageDocCount : $ipp,
				'pages' => $pages,
				'page' => $page
			];
		}


	}

	// -----------------------------------

	public function documentListMenu( $main, $sub, $sortOrder = [] ){
		$menu = [];

		// build main category list
		$mainIn = [];
		$mainSlugIn = [];
		$mainOut = [];

		// check to see if any main category order has been specified
		if ( !empty( $sortOrder ) ) {
			foreach ( $sortOrder AS $mb) {
				$mainSlugIn[] = $mb['category-select'];

				$mainInTemp = [
					'slug' => $mb['category-select']
				];

				// has a subcategory been selected 
				if ( array_key_exists('subcategories', $mb) ) {
					$mainInTemp['subs'] = $mb['subcategories'];
				}

				$mainIn[$mb['category-select']] = $mainInTemp;
			}
		}

		// collect unspecified main catgeories
		foreach ( $this->mainList AS $slug => $mb ) {
			if ( !in_array( $slug, $mainSlugIn) && $slug !== 'nocat' ) {
				$mainOut[$slug] = [
					'slug' => $slug,
					'subs' => []
				];
			}
		}

		$mainCategories = array_merge( $mainIn, $mainOut );

		// subcategories
		foreach ( $mainCategories AS $slug => $mainBlock ) {
			$mainCat = $slug;
			$specifiedSubCat = (!empty($mainBlock['subs'])) ? $mainBlock['subs'] : [];
			$unSpecifiedSubCat = $this->heirarchy[$mainCat]['sub'];
			$subCat = [];

			$isParent = false;

			// no specified subcategories
			if ( count($specifiedSubCat) === 0 ) {
				$subCat = $this->heirarchy[$mainCat]['sub'];

				// iterate over subcatgeories to find which one is 'current'
				$_subCats = [];
				foreach ( $subCat as $subslug => $subItem ) {
					if( $sub === $subItem['slug'] && $mainCat === $main ) {
						$isParent = true;
					}
					$subItem['current'] = ( $sub === $subItem['slug'] );
					$_subCats[] = $subItem;
				}
				$subCat = $_subCats;
			}

			// some specified aubcategories
			if ( count($specifiedSubCat) > 0 && count($specifiedSubCat) < count($unSpecifiedSubCat) ) {

				$in = [];
				$inSlug = [];
				$out = [];

				// order subcategories by $specifiedSubCat
				foreach ( $specifiedSubCat AS $ssc1 ) {
					$ssc2 = explode(',', $ssc1['subcategory-select'] );
					$sscSlug = $ssc2[1];

					foreach ( $unSpecifiedSubCat AS $slug => $source ) {
						if ( $slug === $sscSlug ) {
							if ( $source['slug'] === $sub && $mainCat === $main ) {
								$isParent = true;
							}
							$source['current'] = ( $source['slug'] === $sub && $mainCat === $main );
							$in[] = $source;
							$inSlug[] = $sscSlug;
						}
					}
				}

				foreach ( $unSpecifiedSubCat AS $slug => $source ) {
					if ( !in_array($slug, $inSlug ) ) {
						if ( $source['slug'] === $sub && $mainCat === $main ) {
							$isParent = true;
						}
						$source['current'] = ( $source['slug'] === $sub && $mainCat === $main );
						$out[] = $source;
					}
				}

				// append remaining subcats to end
				$subCat = array_merge($in, $out);
			}

			// all specified subcatgeories
			if ( count($specifiedSubCat) === count($unSpecifiedSubCat) ) {
				// order subcategories by $specifiedSubCat

				// order subcategories by $specifiedSubCat
				foreach ( $specifiedSubCat AS $ssc1 ) {
					$ssc2 = explode(',', $ssc1['subcategory-select'] );
					$sscSlug = $ssc2[1];

					foreach ( $unSpecifiedSubCat AS $slug => $source ) {
						if ( $slug === $sscSlug ) {
							if ( $source['slug'] === $sub && $mainCat === $main ) {
								$isParent = true;
							}
							$source['current'] = ( $source['slug'] === $sub && $mainCat === $main );
							$subCat[] = $source;
						}
					}
				}
			}
			
			$menu[] = [
				'slug' => $mainCat,
				'current' => ( $mainCat === $main),
				'parent' => $isParent,
				'main' => $this->getMain($mainCat),
				'sub' => $subCat
			];
		}

		return $menu;
	}
}

/* ------------------------------------------------------------------------------------- */

?>