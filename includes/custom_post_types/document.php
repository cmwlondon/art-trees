<?php
// https://www.smashingmagazine.com/2015/04/extending-wordpress-custom-content-types/

$labels = array(
'name'               => 'Documents',
'singular_name'      => 'Document',
'menu_name'          => 'Documents',
'name_admin_bar'     => 'Document',
'add_new'            => 'Add New',
'add_new_item'       => 'Add New Document',
'new_item'           => 'New Document',
'edit_item'          => 'Edit Document',
'view_item'          => 'View Document',
'all_items'          => 'All Documents',
'search_items'       => 'Search Documents',
'parent_item_colon'  => 'Parent Document',
'not_found'          => 'No Documents Found',
'not_found_in_trash' => 'No Documents Found in Trash'
);

$args = array(
'labels'              => $labels,
'public'              => true,
'exclude_from_search' => false,
'publicly_queryable'  => true,
'show_ui'             => true,
'show_in_nav_menus'   => true,
'show_in_menu'        => true,
'show_in_admin_bar'   => true,
'menu_position'       => 5,
'menu_icon'           => 'dashicons-admin-appearance',
'capability_type'     => 'post',
'hierarchical'        => false,
// 'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
'supports'            => array( 'title' ),
'has_archive'         => true,
'rewrite'             => array( 'slug' => 'documents' ),
'query_var'           => true
);

register_post_type( 'document', $args );

// Add a taxonomy like categories
$labels = array(
    'name'              => 'Types',
    'singular_name'     => 'Type',
    'search_items'      => 'Search Types',
    'all_items'         => 'All Types',
    'parent_item'       => 'Parent Type',
    'parent_item_colon' => 'Parent Type:',
    'edit_item'         => 'Edit Type',
    'update_item'       => 'Update Type',
    'add_new_item'      => 'Add New Type',
    'new_item_name'     => 'New Type Name',
    'menu_name'         => 'Types',
);

$args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'type' ),
);

register_taxonomy('document_type',array('document'),$args);

    // Add a taxonomy like tags
    $labels = array(
        'name'                       => 'Attributes',
        'singular_name'              => 'Attribute',
        'search_items'               => 'Attributes',
        'popular_items'              => 'Popular Attributes',
        'all_items'                  => 'All Attributes',
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => 'Edit Attribute',
        'update_item'                => 'Update Attribute',
        'add_new_item'               => 'Add New Attribute',
        'new_item_name'              => 'New Attribute Name',
        'separate_items_with_commas' => 'Separate Attributes with commas',
        'add_or_remove_items'        => 'Add or remove Attributes',
        'choose_from_most_used'      => 'Choose from most used Attributes',
        'not_found'                  => 'No Attributes found',
        'menu_name'                  => 'Attributes',
    );

    $args = array(
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'attribute' ),
    );

    register_taxonomy('document_attribute','document',$args);

    /*
    wp_query format to query against this attribute
    
    $queryArguments['tax_query'] = array(
        array(
            'taxonomy' => 'document_attribute',
            'field' => 'slug',
            'terms' => [tag-slug]
        )
    );
    */


/*
specify custom URL rwewrite rule for custom post type
https://wordpress.stackexchange.com/questions/53298/custom-post-type-url-rewriting

    // sets custom post type
    function my_custom_post_type() {
        register_post_type('Projects', array(   
           'label' => 'Projects','description' => '',
           'public' => true,
           'show_ui' => true,
           'show_in_menu' => true,
           'capability_type' => 'post',
           'hierarchical' => false,
           'publicly_queryable' => true,
           'rewrite' => false,
           'query_var' => true,
           'has_archive' => true,
           'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes'),
           'taxonomies' => array('category','post_tag'),
           // there are a lot more available arguments, but the above is plenty for now
        ));
    }

    add_action('init', 'my_custom_post_type');

    global $wp_rewrite;
    $projects_structure = '/projects/%year%/%monthnum%/%day%/%projects%/';
    $wp_rewrite->add_rewrite_tag("%projects%", '([^/]+)', "project=");
    $wp_rewrite->add_permastruct('projects', $projects_structure, false);
*/
?>
