<?php
// faq

$labels = array(
'name'               => 'FAQs',
'singular_name'      => 'FAQ',
'menu_name'          => 'FAQs',
'name_admin_bar'     => 'FAQ',
'add_new'            => 'Add New',
'add_new_item'       => 'Add New FAQ',
'new_item'           => 'New FAQ',
'edit_item'          => 'Edit FAQ',
'view_item'          => 'View FAQ',
'all_items'          => 'All FAQs',
'search_items'       => 'Search FAQs',
'parent_item_colon'  => 'Parent FAQ',
'not_found'          => 'No FAQs Found',
'not_found_in_trash' => 'No FAQs Found in Trash'
);

$args = array(
'labels'              => $labels,
'public'              => true,
'exclude_from_search' => false,
'publicly_queryable'  => true,
'show_ui'             => true,
'show_in_nav_menus'   => true,
'show_in_menu'        => true,
'show_in_admin_bar'   => true,
'menu_position'       => 5,
'menu_icon'           => 'dashicons-admin-appearance',
'capability_type'     => 'post',
'hierarchical'        => false,
// 'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
'supports'            => array( 'title', 'editor' ),
'has_archive'         => true,
'rewrite'             => array( 'slug' => 'faq' ),
'query_var'           => true
);

register_post_type( 'faq', $args );

// Add a taxonomy like categories
$labels = array(
    'name'              => 'Types',
    'singular_name'     => 'Type',
    'search_items'      => 'Search Types',
    'all_items'         => 'All Types',
    'parent_item'       => 'Parent Type',
    'parent_item_colon' => 'Parent Type:',
    'edit_item'         => 'Edit Type',
    'update_item'       => 'Update Type',
    'add_new_item'      => 'Add New Type',
    'new_item_name'     => 'New Type Name',
    'menu_name'         => 'Types',
);

$args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'type' ),
);

register_taxonomy('faq_type',array('faq'),$args);
?>