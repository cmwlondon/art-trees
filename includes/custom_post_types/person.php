<?php
// person

$labels = array(
'name'               => 'People',
'singular_name'      => 'Person',
'menu_name'          => 'People',
'name_admin_bar'     => 'Person',
'add_new'            => 'Add New',
'add_new_item'       => 'Add New Person',
'new_item'           => 'New Person',
'edit_item'          => 'Edit Person',
'view_item'          => 'View Person',
'all_items'          => 'All People',
'search_items'       => 'Search People',
'parent_item_colon'  => 'Parent Person',
'not_found'          => 'No People Found',
'not_found_in_trash' => 'No People Found in Trash'
);

$args = array(
'labels'              => $labels,
'public'              => true,
'exclude_from_search' => false,
'publicly_queryable'  => true,
'show_ui'             => true,
'show_in_nav_menus'   => true,
'show_in_menu'        => true,
'show_in_admin_bar'   => true,
'menu_position'       => 5,
'menu_icon'           => 'dashicons-admin-appearance',
'capability_type'     => 'post',
'hierarchical'        => false,
// 'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
'supports'            => array( 'title' ),
'has_archive'         => true,
'rewrite'             => array( 'slug' => 'governance' ),
'query_var'           => true
);

register_post_type( 'person', $args );

// Add a taxonomy like categories
$labels = array(
    'name'              => 'Types',
    'singular_name'     => 'Type',
    'search_items'      => 'Search Types',
    'all_items'         => 'All Types',
    'parent_item'       => 'Parent Type',
    'parent_item_colon' => 'Parent Type:',
    'edit_item'         => 'Edit Type',
    'update_item'       => 'Update Type',
    'add_new_item'      => 'Add New Type',
    'new_item_name'     => 'New Type Name',
    'menu_name'         => 'Types',
);

$args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'type' ),
);

register_taxonomy('person_type',array('person'),$args);
?>