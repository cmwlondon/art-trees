<?php
// group

// used to provide descriptive text for the Board of Directors and Secretariat across multiple pages in the site

$labels = array(
	'name'               => 'Groups',
	'singular_name'      => 'Group',
	'menu_name'          => 'Groups',
	'name_admin_bar'     => 'Group',
	'add_new'            => 'Add New',
	'add_new_item'       => 'Add New Group',
	'new_item'           => 'New Group',
	'edit_item'          => 'Edit Group',
	'view_item'          => 'View Group',
	'all_items'          => 'All Groups',
	'search_items'       => 'Search Groups',
	'parent_item_colon'  => 'Parent Group',
	'not_found'          => 'No Groups Found',
	'not_found_in_trash' => 'No Groups Found in Trash'
);

$args = array(
	'labels'              => $labels,
	'public'              => true,
	'exclude_from_search' => false,
	'publicly_queryable'  => true,
	'show_ui'             => true,
	'show_in_nav_menus'   => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => 6,
	'menu_icon'           => 'dashicons-admin-appearance',
	'capability_type'     => 'post',
	'hierarchical'        => false,
	// 'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
	'supports'            => array( 'title', 'editor' ),
	'has_archive'         => true,
	'rewrite'             => array( 'slug' => 'group' ),
	'query_var'           => true
);

register_post_type( 'group', $args );

?>