<?php
/*
-------------------------------------------------------------------------------------
Files
-------------------------------------------------------------------------------------
*/

class FileHandler{

	public $mainList;
	public $subList;

	public $heirarchy;

	public $documents;

	// -----------------------------------

    public function __construct() {
    }

	// -----------------------------------

	public function getTypeAndSize( $filename, $size ) {
		$fileType = preg_replace("#\?.*#", "", pathinfo($filename, PATHINFO_EXTENSION));

		if ( $size > 1000000 ) {
			$suffix = 'Mb';
			$fileSizeNice = $size / 1000000;
		}
		if ( $size > 1000 && $size < 1000000) {
			$suffix = 'Kb';
			$fileSizeNice = $size / 1000;
		}
		if ( $size < 1000 ) {
			$suffix = 'bytes';
			$fileSizeNice = $size;
		}

		$fileSize = number_format($fileSizeNice,1).$suffix;

		return [
			'type' => $fileType,
			'size' => $fileSize
		];
	}

	// -----------------------------------
}
?>