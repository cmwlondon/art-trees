<?php
// change login page logo image

function my_login_logo() {
?>
<style>
#login h1 a, .login h1 a {
    background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/svg/LOGO/MBAstack logo_MBAstack-logo-master-colour.svg');
    height: 85px;
    width:auto;
    background-size: auto 125px;
    background-repeat: no-repeat;
    padding-bottom: 30px;
}
</style>
<?php
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// change login page logo to point to home page of site instead of wqordpress.org

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

// change title attribute of login page logo

function my_login_logo_url_title() {
    return 'MBAstack';
}
add_filter( 'login_headertext', 'my_login_logo_url_title' );
?>