<?php
// enable SVG upload via media library

// https://wordpress.stackexchange.com/questions/313951/how-to-upload-svg-in-wordpress-4-9-8
add_filter('wp_check_filetype_and_ext', function ($data, $file, $filename, $mimes) {

    if (!$data['type']) {
        $wp_filetype = wp_check_filetype($filename, $mimes);
        $ext = $wp_filetype['ext'];
        $type = $wp_filetype['type'];
        $proper_filename = $filename;
        if ($type && 0 === strpos($type, 'image/') && $ext !== 'svg') {
            $ext = $type = false;
        }
        $data['ext'] = $ext;
        $data['type'] = $type;
        $data['proper_filename'] = $proper_filename;
    }
    return $data;
}, 10, 4);

add_filter('upload_mimes', function ($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
});

// this adds a style block to the <head> tag of admin pages 
// used to to style the mdia library image view for SVG images
// this function works but the media library is not handling SVG images correctly, it shows a placeholder PNG image instead when you go to the image detail view
// most likely wordpress usually processes uploaded images into three scaled copies for display in the mdeia library, but this process fails with SVG files
add_action('admin_head', function () {
    echo '<style type="text/css">
         .media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail {
      width: 100% !important;
      height: auto !important;
    }</style>';
});



/*
// https://css-tricks.com/snippets/wordpress/allow-svg-through-wordpress-media-uploader/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function fix_svg_thumb_display() {
  echo '
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
      width: 100% !important; 
      height: auto !important; 
    }
  ';
}
add_action('admin_head', 'fix_svg_thumb_display');
*/

/*
related links
https://wpengine.co.uk/resources/enable-svg-wordpress/
https://devmaverick.com/how-to-add-svg-in-wordpress-media-library/
*/

/*
Plugins
https://wordpress.org/plugins/safe-svg/
https://wordpress.org/plugins/svg-support/
*/
?>