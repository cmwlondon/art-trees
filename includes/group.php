<?php
// custom post type 'group'
function getGroup( $slug ) {
	
    $queryArguments = [
		'post_type' => 'group',
		'posts_per_page' => -1,
		'name' => $slug
    ];

    $the_query = new WP_Query( $queryArguments );
    $count = $the_query->post_count; 

    $group = [];
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();

            $post_id = get_the_ID();

            $ACFfields = get_fields($post_id);

            $_members = [];
			foreach ( $ACFfields['members'] AS $item ){
				$_members[] = $item['member'];
			}
            $group = [
            	'id' => $post_id,
            	'members' => $_members,
            	'slug' => $ACFfields['group_slug'],
            	'name' => get_the_title(),
            	// 'content' => get_the_content()
                'content' => $ACFfields['description']
				//the_title();
            ];
        }
    }
    wp_reset_postdata();

    return $group;
}
?>