<?php
/*
footer email signup form
*/

// action = 'signup'

// client side
// validate fields
// submit to server via POST
// server side
// 		submission nonce
// 		go back to invalid from and populate fields with submitted values, indicate nvalid fields
// 		find out how to integrate mail send to given email address

// via AJAX
// 		send email via AJAX call

// ----------------------------------------------------
// SERVER-SIDE
// ----------------------------------------------------
function signup(){
	echo "signup\n";
	print_r($_POST);
	die();

	//wp_redirect( home_url() ); exit;
};

add_action( 'admin_post_nopriv_signup', 'signup' );
add_action( 'admin_post_signup', 'signup' );

// ----------------------------------------------------
// AJAX
// ----------------------------------------------------
function ajax_signup(){

	$name = $_POST['name']; // required, letters,spaces,some punctuation only
	$email = $_POST['email']; // required, vald email address
	$organization = $_POST['organization']; // optional, letters,spaces,some punctuation only
	$optin = $_POST['optin']; // required, value = 'optin'

    $response = [
        'action' => 'signup',
        'name' => $name,
        'email' => $email,
        'organization' => $organization,
        'optin' => $optin,
    ];

	$errors = [];
	$valid = false; // start under the basis the form is always invalid
	// do validation on server side

	if ( $valid ) { 
		// form is valid

		// send signup email to given address

		// build response JSON
		$response['status'] = 'valid';
		$response['errors'] = [];
	} else {
		// build response JSON
		$response['status'] = 'invalid';
		$response['errors'] = ['name','email','organization','optin']; // list names ov invalid fields
	}

    echo json_encode( $response );
    exit();
}

add_action('wp_ajax_nopriv_ajax_signup', 'ajax_signup');
add_action('wp_ajax_ajax_signup', 'ajax_signup');

?>