<?php
// register custom post
function create_posttype() {
    include 'custom_post_types/person.php';
    include 'custom_post_types/document.php';
    include 'custom_post_types/faq.php';
    include 'custom_post_types/group.php';
}

add_action( 'init', 'create_posttype' );
?>