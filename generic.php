<?php
/*
Template Name: generic
*/
if ( have_posts() ) {
	the_post();
	// populates variable $post
	// post id = $post->ID
	$pageCSS = ( !empty(get_field('page_css')) ) ? get_field('page_css') : '';

	$hasGenericHeader = get_field('has_generic_header');
	$hasBreadcumbBar = get_field('has_breadcrumb');

	get_header( null, [ 'page-css' => $pageCSS, 'hasgenericheader' => $hasGenericHeader, 'hasbreadcumbbar' => $hasBreadcumbBar] ); 
	/* PAGE STRUCTURE START */

	// has_generic_header get_field('has_generic_header')
	// has_breadcrumb get_field('has_breadcrumb')

	// get_template_part( 'blocks/common/page-banner','',['large' => get_field('large'), 'small' => get_field('small'), 'desktop' => get_field('page_image_desktop'), 'mobile' => get_field('page_image_mobile'), 'has-video' => get_field('has_video'), 'format' => get_field('video_format'), 'source' => get_field('video_source'), 'gradient_start' => get_field('gradient_start'), 'gradient_end' => get_field('gradient_end') ] );
	get_template_part( 'blocks/common/page-banner','',[]);
	get_template_part( 'blocks/modules','',[]);

	/* PAGE STRUCTURE END */

	get_footer( null, [] );
}
?>