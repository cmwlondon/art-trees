<?php
/*
ARTTree-2021 theme
functions 
*/
ini_set('display_errors','On');
ini_set('error_reporting', E_ALL );
ini_set('mysql.trace_mode', 0 );

/* --------------------------------------------- */

// import SVG files into media library
include 'includes/svg.php';

/* --------------------------------------------- */

// set page title tag
include 'includes/page-title.php';

/* --------------------------------------------- */

// add scripts links to header andfooter
include 'includes/scripts.php';

/* --------------------------------------------- */

// set up custom menus
// include 'includes/menus.php';

/* --------------------------------------------- */

// set up custom posts
include 'includes/custom-posts.php';

/* --------------------------------------------- */

// set up header and footer menus in admin
function arttree_menus() {

   $locations = array(
      'primary'  => __( 'Main navigation', 'ARTTree-2021' ),
      'footer' => __( 'Footer links', 'ARTTree-2021' )
   );

   register_nav_menus( $locations );
}

add_action( 'init', 'arttree_menus' );

/* --------------------------------------------- */

// file handling functions 
include 'includes/files.php';

/* --------------------------------------------- */

// document handling functions 
include 'includes/documents.php';

/* --------------------------------------------- */

// people handling functions 
include 'includes/person.php';

/* --------------------------------------------- */

// group handling functions 
// group -> group of people i.e.: 'board-of-directors','secretriat'
include 'includes/group.php';

/* --------------------------------------------- */

// FAQ functions 
include 'includes/faqs.php';

/* --------------------------------------------- */

// news post handling functions 
include 'includes/news.php';

/* --------------------------------------------- */

// set up AJAX endpoints
include 'includes/ajax-endpoints.php';

/* --------------------------------------------- */

// set up footer signup form
include 'includes/form-endpoints.php';

/* --------------------------------------------- */

/*
// assign parent page to post

//Add the meta box callback function
function admin_init(){
    add_meta_box("case_study_parent_id", "Case Study Parent ID", "set_case_study_parent_id", "casestudy", "normal", "low");
}
add_action("admin_init", "admin_init");

//Meta box for setting the parent ID
function set_case_study_parent_id() {
    global $post;
    $custom = get_post_custom($post->ID);
    $parent_id = $custom['parent_id'][0];
    ?>
    <p>Please specify the ID of the page or post to be a parent to this Case Study.</p>
    <p>Leave blank for no heirarchy.  Case studies will appear from the server root with no assocaited parent page or post.</p>
    <input type="text" id="parent_id" name="parent_id" value="<?php echo $post->post_parent; ?>" />
    <?php
    // create a custom nonce for submit verification later
    echo '<input type="hidden" name="parent_id_noncename" value="' . wp_create_nonce(__FILE__) . '" />';
}

// Save the meta data
function save_case_study_parent_id($post_id) {
    global $post;

    // make sure data came from our meta box
    if (!wp_verify_nonce($_POST['parent_id_noncename'],__FILE__))
        return $post_id;

    if(isset($_POST['parent_id']) && ($_POST['post_type'] == "casestudy")) {
        $data = $_POST['parent_id'];
        update_post_meta($post_id, 'parent_id', $data);
    }
}
add_action("save_post", "save_case_study_parent_id");
*/

/* --------------------------------------------- */

function current_location()
{
    if (isset($_SERVER['HTTPS']) &&
        ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
        $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
        $protocol = 'https://';
    } else {
        $protocol = 'http://';
    }
    return $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

/* --------------------------------------------- */

/*
add_action('init', function() {
    // http://arttree.local/trees/documents-and-templates/
    // http://arttree.local/trees/summaries-guidance/
    // http://arttree.local/trees/external-resources/

    // rewrite rule for /trees/documents-and-templates/
    add_rewrite_rule(
        $page_data->post_name = '/trees/documents-and-templates/([a-x\-]+)$',
        'index.php?pagename=' . $page_data->post_name . '&sub=$matches[1]',
        'top'
    );

    add_rewrite_rule(
        $page_data->post_name = '/trees/documents-and-templates/([a-x\-]+)/([0-9]{1,3})',
        'index.php?pagename=' . $page_data->post_name . '&sub=$matches[1]&pn=$matches[2]',
        'top'
    );
});
*/

/* --------------------------------------------- */

// add theme settings page -> global ACF fields available on all pages

// -> get_field('category_order','option')

if( function_exists('acf_add_options_page') ) {
        acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}

/* --------------------------------------------- */

// populate select field dynamically from wp_query

// Theme Settings -> TREEs Document List
// ------------------------------------------------
// populate main category select
function acf_load_doclist_main( $field ) {
    $field['choices'] = array();
    $mappings = [
        'trees' => [
            'field' => 'category_order',
            'category' => 'category-select',
            'subcategory' => 'subcategory-select',
            'documents' => 'documents',
            'document' => 'document-select'
        ],
        'verification' => [
            'field' => 'category_order2',
            'category' => 'category-select2',
            'subcategory' => false,
            'documents' => 'documents2',
            'document' => 'document-select2'
        ],
    ];

    $thisPageDocumentsHandler = new DocumentHandler( 'trees', $mappings );
    // main $thisPageDocumentsHandler->main
    // sub $thisPageDocumentsHandler->sub
    // heirarchy $thisPageDocumentsHandler->heirarchy

    foreach ( $thisPageDocumentsHandler->mainList AS $category ) {
        if ( $category['slug'] !== 'nocat' ) {
            $field['choices'][ $category['slug'] ] = $category['name'];    
        } 
    }

    return $field;
}
add_filter('acf/load_field/name=category-select', 'acf_load_doclist_main');

// populate subcategory select
function acf_load_doclist_sub( $field ) {
    $field['choices'] = array();
    $choices = [];
    $mappings = [
        'trees' => [
            'field' => 'category_order',
            'category' => 'category-select',
            'subcategory' => 'subcategory-select',
            'documents' => 'documents',
            'document' => 'document-select'
        ],
        'verification' => [
            'field' => 'category_order2',
            'category' => 'category-select2',
            'subcategory' => false,
            'documents' => 'documents2',
            'document' => 'document-select2'
        ],
    ];

    $thisPageDocumentsHandler = new DocumentHandler( 'trees', $mappings );
    foreach ( $thisPageDocumentsHandler->subList AS $subcategory ) {
        if ( $subcategory['slug'] !== 'nocat' ) {
            $field['choices'][ $subcategory['parent']['slug'].','.$subcategory['slug'] ] = $subcategory['name']. " (child of ".$subcategory['parent']['name'].")";    
        } 
    }

    return $field;
}
add_filter('acf/load_field/name=subcategory-select', 'acf_load_doclist_sub');

// populate document select
function acf_load_doclist_docs( $field ) {
    // get main and sub values and use these to filter documents to populate document dropdowns
    $mappings = [
        'trees' => [
            'field' => 'category_order',
            'category' => 'category-select',
            'subcategory' => 'subcategory-select',
            'documents' => 'documents',
            'document' => 'document-select'
        ],
        'verification' => [
            'field' => 'category_order2',
            'category' => 'category-select2',
            'subcategory' => false,
            'documents' => 'documents2',
            'document' => 'document-select2'
        ],
    ];
    
    $field['choices'] = array();
    $choices = [];
    $thisPageDocumentsHandler = new DocumentHandler( 'trees', $mappings );
    //id,title    
    foreach ( $thisPageDocumentsHandler->documents AS $document ) {
        $field['choices'][ $document['id'] ] = "{$document['title']}({$document['main']['name']},{$document['sub']['name']})";
    }
    return $field;
}
add_filter('acf/load_field/name=document-select', 'acf_load_doclist_docs');

// ------------------------------------------------

// Theme Settings -> TREEs Document List
// ------------------------------------------------
function acf_load_doclist_main2( $field ) {
    $field['choices'] = array();
    $mappings = [
        'trees' => [
            'field' => 'category_order',
            'category' => 'category-select',
            'subcategory' => 'subcategory-select',
            'documents' => 'documents',
            'document' => 'document-select'
        ],
        'verification' => [
            'field' => 'category_order2',
            'category' => 'category-select2',
            'subcategory' => false,
            'documents' => 'documents2',
            'document' => 'document-select2'
        ],
    ];
    $thisPageDocumentsHandler = new DocumentHandler( 'verification', $mappings );
    // main $thisPageDocumentsHandler->main
    // sub $thisPageDocumentsHandler->sub
    // heirarchy $thisPageDocumentsHandler->heirarchy

    foreach ( $thisPageDocumentsHandler->mainList AS $category ) {
        if ( $category['slug'] !== 'nocat' ) {
            $field['choices'][ $category['slug'] ] = $category['name'];    
        } 
    }

    return $field;
}
add_filter('acf/load_field/name=category-select2', 'acf_load_doclist_main2');

function acf_load_doclist_docs2( $field ) {
    // get main and sub values and use these to filter documents to populate document dropdowns
    
    $field['choices'] = array();
    $choices = [];
    $mappings = [
        'trees' => [
            'field' => 'category_order',
            'category' => 'category-select',
            'subcategory' => 'subcategory-select',
            'documents' => 'documents',
            'document' => 'document-select'
        ],
        'verification' => [
            'field' => 'category_order2',
            'category' => 'category-select2',
            'subcategory' => false,
            'documents' => 'documents2',
            'document' => 'document-select2'
        ],
    ];
    $thisPageDocumentsHandler = new DocumentHandler( 'verification', $mappings );
    //id,title    
    foreach ( $thisPageDocumentsHandler->documents AS $document ) {
        $field['choices'][ $document['id'] ] = "{$document['title']}({$document['main']['name']})";
    }
    return $field;
}
add_filter('acf/load_field/name=document-select2', 'acf_load_doclist_docs2');

/* --------------------------------------------- */

/*
// customize logo on admin login page
// https://codex.wordpress.org/Customizing_the_Login_Form

/*
*****************************
Place at END of functions.php
*****************************
*/

// change logo on wp admin login page
// include 'includes/custom-logo.php';

/* --------------------------------------------- */
?>
