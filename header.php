<!doctype html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png"><link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png"><link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png"><link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png"><link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png"><link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png"><link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png"><link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png"><link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png"><link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png"><link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"><link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png"><link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"><link rel="manifest" href="/manifest.json"><meta name="msapplication-TileColor" content="#ffffff"><meta name="msapplication-TileImage" content="/ms-icon-144x144.png"><meta name="theme-color" content="#ffffff">
<!-- 
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@mbastack" />
<meta name="twitter:title" content="MBAstack" />
<meta name="twitter:description" content="MBAstack" />
<meta name="twitter:image" content="" />
-->

<!-- facebook opengraph metatags -->

    <!--
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Mono:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/styles.css?t=<?php echo time(); ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> -->
    <!-- <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script> -->
<?php
wp_head();

// 'hasgenericheader' => $hasGenericHeader
// 'hasbreadcumbbar'

$pageCSS = ( !empty(get_field('page_css')) ) ? get_field('page_css') : '';

$hasGenericHeader = ($args['hasgenericheader'] === 'yes' );
$hasBreadcumbBar = ($args['hasbreadcumbbar'] === 'yes' );

// maindefault
// no header, no breadcrumb -> main padding: 100px (menu only)
$mainClass = '';
$headerClass = '';

// mainbreadcrumb
// no header, breadcrumb -> main padding: 208px (menu + breadcrumb bar + breadcrumb bar bottom margin)
if ( !$hasGenericHeader && $hasBreadcumbBar) {
  $mainClass = 'mainBreadcrumb';  
  $headerClass = 'breadcrumbShadow';
} 

// mainall
// header and breadcrumb bar -> initial main padding: 100px (menu only), scroll down until breadcrumg is 100px from top of window: main padding = menu + beradrumb + breadcrumb margin + header height
if ( $hasGenericHeader && $hasBreadcumbBar) {
  $mainClass = 'mainAll';
  $headerClass = '';
} 

$pageClass = ( $pageCSS === 'homepage' ) ? 'homepage' : 'generic';
?>

</head>
<body class="<?= $pageClass ?>">

<!-- Google Tag Manager --> 
<!-- 
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PKQMRP" 
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> 
<script>(function (w, d, s, l, i) {
w[l] = w[l] || []; w[l].push({
'gtm.start':
new Date().getTime(), event: 'gtm.js'
}); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-PKQMRP');</script> 
-->
<!-- End Google Tag Manager -->

<header class="navheader <?= $headerClass ?>">
  <div class="container menubits">

    <div class="logo">
      <a href="<?= site_url() ?>" title="<?= get_bloginfo( 'name' ) ?>"><img alt="<?= get_bloginfo( 'name' ) ?>" src="<?php echo get_template_directory_uri(); ?>/assets/svg/art-logo.svg"></a>
    </div>

    <nav class="barnav">
        <div class="clipper">
        <?php
          wp_nav_menu( array(
            'theme_location' => 'primary',
            'menu_class' => 'navbar-nav',
            'container' => false,
          ) );
        ?>
        </div>
    </nav>

    <div class="menuToggle">
      <div class="toggler"><span class="label">Open navigation</span><span class="icon"></span></div>
    </div>
  </div>
</header>

<main class="<?= $mainClass ?> <?php if ( !empty($args['page-css'] ) ) { echo $args['page-css'];} ?>">
