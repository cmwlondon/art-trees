<?php
/*
Template Name: home
*/
if ( have_posts() ) {
	the_post();
	// populates variable $post
	// post id = $post->ID
	// $pageCSS = ( !empty(get_field('page_css')) ) ? get_field('page_css') : '';

	// $pageCSS = ( !empty(get_field('page_css')) ) ? get_field('page_css') : '';
	get_header( null, [  'page-css' => 'homepage', 'hasgenericheader' => 'no', 'hasbreadcumbbar' => 'no' ] ); 
	/* PAGE STRUCTURE START */

	get_template_part( 'blocks/modules','',[]);
	
	/* PAGE STRUCTURE END */

	get_footer( null, [] );
}
?>